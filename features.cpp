#include "features.h"
#include "utils.h"

SurfFeatureDetector detector(400,4,2,false,false);
SurfDescriptorExtractor extractor(400,4,2,false,false);
Mat surf_desc_db;
Mat surf_classes_db;
Mat hog_desc_db;
Mat hog_classes_db;
int winWidth,winHeight;

int POS_LABEL = 1;
int NEG_LABEL = -1;


Mat features::get_surf_desc_db(){ return surf_desc_db; }
Mat features::get_surf_classes_db(){ return surf_classes_db; }

Mat features::get_hog_desc_db(){ return hog_desc_db; }
Mat features::get_hog_classes_db(){ return hog_classes_db; }

void features::empty_matrix(Mat& m)
{
	unsigned int size = m.rows;
	for(unsigned int i=0;i<size;i++)
	{
		m.pop_back();
	}
}
void clear_db_matrix(string type)
{
	if(type=="HOG")
	{
		unsigned int size = hog_desc_db.rows;
		for(unsigned int i=0;i<size;i++)
		{
			hog_desc_db.pop_back();
			hog_classes_db.pop_back();
		}		
	}
	else if(type=="SURF")
	{
		unsigned int size = surf_desc_db.rows;
		for(unsigned int i=0;i<size;i++)
		{
			surf_desc_db.pop_back();
			surf_classes_db.pop_back();
		}		
	}	
}
Mat features::get_class(Mat& descriptors,int sampleType)
{
	Mat classes(descriptors.rows,1,DataType<int>::type);	
	for(int i=0;i<descriptors.rows;i++)
	{
		classes.at<int>(i,0) = sampleType;
	}
	return classes;
}
void features::extract_keypoints(Mat& image,int sampleType,Mat& descriptors,Mat& classes)
{	
	vector<KeyPoint> keypoints;
	detector.detect(image,keypoints);
	//cout<<"keypoints total"<< keypoints.size() <<endl;
	extractor.compute(image,keypoints,descriptors);	
	classes.push_back(get_class(descriptors,sampleType));
	
	/*vector<KeyPoint> k;
	SiftFeatureDetector d;
	SurfDescriptorExtractor e;
	Mat dsc;
	t = (double)getTickCount();
	d.detect(image,keypoints);
	e.compute(image,k,dsc);
	t = ((double)getTickCount() - t)/getTickFrequency();
	cot<<"sift computation time:"<<t<<endl;*/
	
}
void features::extract_keypoints(Mat& image,Mat& descriptors)
{	
	vector<KeyPoint> keypoints;
	detector.detect(image,keypoints);	
	extractor.compute(image,keypoints,descriptors);	
}
void features::compute_hog_features(Mat& image,int sampleType,Mat& descriptors,Mat& classes)
{
	//cout<<"width:"<<image.cols<<":height:"<<image.rows<<endl;
	gpu::GpuMat gpu_img;
	gpu_img.upload(image);	
	gpu::HOGDescriptor hog(Size(DET_WIN_WIDTH,DET_WIN_HEIGHT),Size(16, 16),Size(8,8),Size(8, 8),9,-1,0.2,true,64);
	
	gpu::GpuMat gpu_desc;
	hog.getDescriptors(gpu_img,Size(16,16),gpu_desc);
	
	gpu_desc.download(descriptors);	
	classes.push_back(get_class(descriptors,sampleType));
	/*cout <<"desc rows(HOG):"<<descriptors.rows<<endl;
	cout<<"desc cols(HOG):"<<descriptors.cols<<endl;
	cout<<"classs rows(HOG):"<< classes.rows<<endl;
	cout<<"class cols(HOG):"<<classes.cols<<endl;*/
	
}

void features::compute_hog_features(Mat& image,Mat& descriptors)
{
	//cout<<"width:"<<image.cols<<":height:"<<image.rows<<endl;
	gpu::GpuMat gpu_img;
	gpu_img.upload(image);	
	gpu::HOGDescriptor hog(Size(DET_WIN_WIDTH,DET_WIN_HEIGHT),Size(16, 16),Size(8,8),Size(8, 8),9,-1,0.2,true,64);
	
	gpu::GpuMat gpu_desc;
	hog.getDescriptors(gpu_img,Size(16,16),gpu_desc);
	
	gpu_desc.download(descriptors);	
}
void features::extract_keypoints(IplImage* frame,int sampleType,Mat& descriptors,Mat& classes)
{	
	Mat image = frame;
	vector<KeyPoint> keypoints;
	detector.detect(image,keypoints);
	//cout<<"keypoints total"<< keypoints.size() <<endl;
	extractor.compute(image,keypoints,descriptors);

	classes.push_back(get_class(descriptors,sampleType));
	
	/*cout <<"desc rows:"<<descriptors.rows<<endl;
	cout<<"desc cols:"<<descriptors.cols<<endl;
	cout<<"classs rows:"<< classes.rows<<endl;
	cout<<"class cols:"<<classes.cols<<endl;*/
}
void features::compute_hog_features(IplImage* frame,int sampleType,Mat& descriptors,Mat& classes)
{
	Mat image = frame;
	//cout<<"width:"<<image.cols<<":height:"<<image.rows<<endl;
	gpu::GpuMat gpu_img;
	gpu_img.upload(image);
	gpu::HOGDescriptor hog(Size(DET_WIN_WIDTH,DET_WIN_HEIGHT),Size(16, 16),Size(8, 8),Size(8, 8),9,-1,0.2,true,64);
	
	gpu::GpuMat gpu_desc;
	hog.getDescriptors(gpu_img,Size(16,16),gpu_desc);
	
	gpu_desc.download(descriptors);	
	classes.push_back(get_class(descriptors,sampleType));
}
	
void populate_central_db(Mat& descriptors,Mat& classes,string type)
{
	if(type == "SURF"){ surf_desc_db.push_back(descriptors); surf_classes_db.push_back(classes);}
	/*cout<<"surf rows:"<<surf_desc_db.rows<<endl;
	cout<<"surf classes:"<<surf_classes_db.rows<<endl;*/
	
	//cout<<"About to populate the HOG central DB"<<endl;
	if(type == "HOG"){ hog_desc_db.push_back(descriptors); hog_classes_db.push_back(classes);}
	//cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	//cout<<"hog classes:"<<hog_classes_db.rows<<endl;

}
void add_missing_values(string& file_name,int &x,int &y)
{
	if(file_name.find("Kinect1629.jpg")!=string::npos)
	{
		y = y-3;
	}				
	if(file_name.find("Kinect1646.jpg")!=string::npos)
	{
		y = y-2;
	}
	if(file_name.find("Kinect2161.jpg")!=string::npos)
	{
		x = x-1;
	}
	if(file_name.find("Kinect2162.jpg")!=string::npos)
	{
		x = x-1;
	}
	if(file_name.find("Kinect2334.jpg")!=string::npos)
	{
		y = y-4;
	}
	if(file_name.find("Kinect2343.jpg")!=string::npos)
	{
		y = y-1;
	}
	if(file_name.find("Kinect2807.jpg")!=string::npos)
	{
		y = y-1;
	}
	if(file_name.find("Kinect2808.jpg")!=string::npos)
	{
		y = y-1;
	}
	if(file_name.find("Kinect2809.jpg")!=string::npos)
	{
		y = y-1;
	}
	if(file_name.find("Kinect2810.jpg")!=string::npos)
	{
		y = y-1;
	}
	if(file_name.find("Kinect2825.jpg")!=string::npos)
	{
		x = x-4;
	}				
	if(file_name.find("Kinect2970.jpg")!=string::npos)
	{
		x = x-17;
	}
	if(file_name.find("P1010708.JPG")!=string::npos)
	{
		if(x==129)
			x = x-9;
	}
	if(file_name.find("P1010710.JPG")!=string::npos)
	{
		if(x==121)
			x = x-1;
	}
	if(file_name.find("P1010716.JPG")!=string::npos)
	{
		if(x==136)
			x = x-16;
	}
	if(file_name.find("P1010734.JPG")!=string::npos)
	{
		if(y==124)
			y = y-4;
	}
	if(file_name.find("P1010745.JPG")!=string::npos)
	{
		if(y==132)
			y = y-16;
	}
	if(file_name.find("P1010765.JPG")!=string::npos)
	{
		if(x==125)
			x = x-5;
	}
			
}

vector<vec_entry> trim_entries(vector<vec_entry>& entries,int treshold)
{
	vector<vec_entry> newList;	
	unsigned int K = entries.size();
	map<int, int> recordMap;	
	//int pivot = (K/2);we can use this at some point to pick out random entries from the features database
	for(unsigned int i = 0;i<K;i++)
	{			
		if(newList.size()>=(unsigned)treshold){break;}
		//we are loading up the image to check if the training image is actually there
		//there are some images missing in the data
		//for this one we are fetching the images from the front of the vector
		Mat frame = imread(entries[i].filename,CV_LOAD_IMAGE_GRAYSCALE);
	
		if(!frame.empty()) { newList.push_back(entries[i]);}
		
		if(newList.size()>=(unsigned)treshold){break;}
		
		//we are loading up the image to check if the training image is actually there
		//there are some images missing in the data
		//for this one we are fetching the images from the bottom
		frame = imread(entries[(entries.size()-i)-1].filename,CV_LOAD_IMAGE_GRAYSCALE);
		if(!frame.empty()) { newList.push_back(entries[(entries.size()-i)-1]);}		
	}
	return newList;
}

feature_class_pair features::get_surf_data(string pos_vec,string neg_vec,int rand_neg_wins,int noOfPosEntries,int noOfNegEntries)
{
	clear_db_matrix("SURF");
	vector<vec_entry>pos_entries;
	vector<vec_entry>neg_entries;
	utils::get_vec_entries(pos_vec,pos_entries);
	utils::get_vec_entries(neg_vec,neg_entries);
	
	cout<<"Random neg width:"<<RAND_WIN_WIDTH<<endl;
	cout<<"Random neg height:"<<RAND_WIN_HEIGHT<<endl;
	
	pos_entries = trim_entries(pos_entries,noOfPosEntries);
	neg_entries = trim_entries(neg_entries,noOfNegEntries);
	
	feature_class_pair feature_data;
	
	cout<<"pos size:"<<pos_entries.size()<<endl;
	cout<<"neg size:"<<neg_entries.size()<<endl;
	for(unsigned int i=0;i<pos_entries.size();i++)
	{
		vec_entry entry = pos_entries[i];		
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);
		//cout<<"doing pos image:"<<entry.filename<<endl;		
		GaussianBlur(query_image,query_image,Size(3,3),1);
		if(!query_image.empty())
		{			
			for(unsigned int k=0;k<entry.objects.size();k++)
			{
				vec_object object = entry.objects[k];
				add_missing_values(entry.filename,object.x,object.y);
				Mat roi(query_image,Rect(object.x,object.y,object.width,object.height));
									
				Mat s_desc,s_classes;	
				extract_keypoints(roi,POS_LABEL,s_desc,s_classes);
				populate_central_db(s_desc,s_classes,"SURF");
			}
		}
	}
	for(unsigned int i=0;i<neg_entries.size();i++)
	{
		vec_entry entry = neg_entries[i];
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);	
		GaussianBlur(query_image,query_image,Size(3,3),1);
		if(!query_image.empty())
		{
			int i=0;
			do{				
				int x = rand()% (query_image.cols - RAND_WIN_WIDTH);
				int y= rand()% (query_image.rows - RAND_WIN_HEIGHT);	
				Mat roi(query_image,Rect(x,y,RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
				
				Mat s_desc,s_classes;
				extract_keypoints(roi,NEG_LABEL,s_desc,s_classes);
				populate_central_db(s_desc,s_classes,"SURF");						
				i++;
			}while(i < rand_neg_wins);			
		}
	}	
	feature_data.surf_features = get_surf_desc_db();
	feature_data.surf_classes = get_surf_classes_db();
	cout<<"hog rows:"<<surf_desc_db.rows<<endl;
	cout<<"hog classes:"<<surf_classes_db.rows<<endl;
	return feature_data;
}
feature_class_pair features::get_hog_data_for_tud(string pos_vec,string neg_vec,int rand_neg_wins,int doFullBODY,int noOfPosEntries,int noOfNegEntries)
{
	
	string filename = "vecfiles/TUDuprightpeople.vec";
	vector<vec_entry> entries;
	utils::get_vec_entries(filename,entries);	
/*	cout<<"size of the file"<<entries.size()<<endl;
	cout<<"great success in test..."<<entries.size()<<endl;*/
	
	clear_db_matrix("HOG");
	vector<vec_entry>pos_entries;
	vector<vec_entry>neg_entries;
	/* get the entries */	
	cout<<"pos vec:"<<pos_vec<<endl;
	cout<<"neg vec:"<<neg_vec<<endl;
	utils::get_vec_entries(pos_vec,pos_entries);
	utils::get_vec_entries(neg_vec,neg_entries);	
	cout<<"size of the file"<<pos_entries.size()<<endl;
	/* check if we need to trim the entiries*/
	if(noOfPosEntries > 0){
		pos_entries = trim_entries(pos_entries,noOfPosEntries);
	}		
	if(noOfNegEntries > 0)	{
		neg_entries = trim_entries(neg_entries,noOfNegEntries);
	}
		
	feature_class_pair feature_data;
	
	cout<<"pos size:"<<pos_entries.size()<<endl;
	cout<<"neg size:"<<neg_entries.size()<<endl;
	
	for(unsigned int i=0;i<pos_entries.size();i++)
	{	
		vec_entry entry = pos_entries[i];		
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);						
		//cout<<query_image.rows<<","<<query_image.cols<<endl;		
		cout<<entry.filename<<endl;
		if(!query_image.empty())
		{	
			/**if(doFullBODY>0){
				cout<<"I am about to resize the image:"<<endl;
				resize(query_image,query_image,Size(DET_WIN_WIDTH,DET_WIN_HEIGHT));
			}*/
			Mat h_desc,h_classes;
			if(entry.objects.size() > 0)
			{
				for(unsigned int k=0;k<entry.objects.size();k++)
				{
					vec_object object = entry.objects[k];
					cout<<entry.filename<<endl;
					cout<<"About to calculate abs width,height"<<endl;
					int width = abs(object.width - object.x);//abs(object.width - object.x);
					int height = abs(object.height - object.y);//abs(object.height - object.y);				
					cout<<"x,y,width,height,"<<object.x<<","<<object.y<<","<<width<<","<<height<<endl;
					cout<<"width,height:"<<object.width<<","<<object.height<<endl;										
					cout<<"det width,height:"<<DET_WIN_WIDTH<<":"<<DET_WIN_HEIGHT<<endl;
					Mat roi(query_image,Rect(object.x,object.y,width,height));
					cout<<"det width,height:"<<roi.rows<<":"<<roi.cols<<endl;
					Mat localDesc,localClasses;
					compute_hog_features(roi,POS_LABEL,localDesc,localClasses);		
					cout<<"rowsize:"<<localDesc.rows<<endl;
					h_desc.push_back(localDesc);
					h_classes.push_back(localClasses);
					cout<<"Added data?"<<endl;					
				}
			}
			else
			{
				compute_hog_features(query_image,POS_LABEL,h_desc,h_classes);
			}
			
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}
	for(unsigned int i=0;i<neg_entries.size();i++)
	{
		vec_entry entry = neg_entries[i];
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);		
		//cout<<entry.filename<<endl;
		
		if(!query_image.empty())
		{			
			int i=0;
			do{
				int x = rand()% (query_image.cols - RAND_WIN_WIDTH);
				int y= rand()% (query_image.rows - RAND_WIN_HEIGHT);
				Mat roi(query_image,Rect(x,y,RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
				
				Mat h_desc,h_classes;
				compute_hog_features(roi,NEG_LABEL,h_desc,h_classes);
				populate_central_db(h_desc,h_classes,"HOG");				
				i++;
			}while(i < rand_neg_wins);			
		}
	}	
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;
	return feature_data;
}
feature_class_pair features::get_hog_data_wout_resize(string pos_vec,string neg_vec,int rand_neg_wins,int noOfPosEntries,int noOfNegEntries)
{
	clear_db_matrix("HOG");
	vector<vec_entry>pos_entries;
	vector<vec_entry>neg_entries;
	/*cout<<"pos vec:"<<pos_vec<<endl;
	cout<<"neg vec:"<<neg_vec<<endl; */
	utils::get_vec_entries(pos_vec,pos_entries);
	utils::get_vec_entries(neg_vec,neg_entries);	
	/* check if we need to trim the entiries*/
	if(noOfPosEntries > 0){
		pos_entries = trim_entries(pos_entries,noOfPosEntries);
	}		
	if(noOfNegEntries > 0)	{
		neg_entries = trim_entries(neg_entries,noOfNegEntries);
	}		
	feature_class_pair feature_data;	
	cout<<"pos size:"<<pos_entries.size()<<endl;
	cout<<"neg size:"<<neg_entries.size()<<endl;
	
	for(unsigned int i=0;i<pos_entries.size();i++)
	{	
		vec_entry entry = pos_entries[i];		
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);						
		//cout<<query_image.rows<<","<<query_image.cols<<endl;		
		//cout<<entry.filename<<endl;
		if(!query_image.empty())
		{	
			Mat h_desc,h_classes;
			compute_hog_features(query_image,POS_LABEL,h_desc,h_classes);
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}
	cout<<"pos hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"pos hog classes:"<<hog_classes_db.rows<<endl;
	for(unsigned int i=0;i<neg_entries.size();i++)
	{
		vec_entry entry = neg_entries[i];
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);		
		//cout<<entry.filename<<endl;		
		if(!query_image.empty())
		{			
			int i=0;
			do{
				int x = rand()% (query_image.cols - RAND_WIN_WIDTH);
				int y= rand()% (query_image.rows - RAND_WIN_HEIGHT);
				Mat roi(query_image,Rect(x,y,RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
				
				Mat h_desc,h_classes;
				compute_hog_features(roi,NEG_LABEL,h_desc,h_classes);
				populate_central_db(h_desc,h_classes,"HOG");				
				i++;
			}while(i < rand_neg_wins);			
		}
	}	
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;
	return feature_data;
}

feature_class_pair features::get_hog_data(string pos_vec,string neg_vec,int rand_neg_wins,int noOfPosEntries,int noOfNegEntries)
{
	clear_db_matrix("HOG");
	vector<vec_entry>pos_entries;
	vector<vec_entry>neg_entries;
	/* get the entries */	
	cout<<"pos vec:"<<pos_vec<<endl;
	cout<<"neg vec:"<<neg_vec<<endl;
	utils::get_vec_entries(pos_vec,pos_entries);
	utils::get_vec_entries(neg_vec,neg_entries);	
	/* check if we need to trim the entiries*/
	
	if(noOfPosEntries > 0){
		pos_entries = trim_entries(pos_entries,noOfPosEntries);
	}		
	if(noOfNegEntries > 0)	{
		neg_entries = trim_entries(neg_entries,noOfNegEntries);
	}
		
	feature_class_pair feature_data;
	
	cout<<"pos size:"<<pos_entries.size()<<endl;
	cout<<"neg size:"<<neg_entries.size()<<endl;
	
	for(unsigned int i=0;i<pos_entries.size();i++)
	{	
		vec_entry entry = pos_entries[i];		
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);			
		if(!query_image.empty())
		{	
			for(unsigned int k=0;k<entry.objects.size();k++)
			{
				vec_object object = entry.objects[k];
				//add_missing_values(entry.filename,object.x,object.y);								
				cout<<entry.filename<<endl;				
				Mat roi(query_image,Rect(object.x,object.y,object.width,object.height));			
				Mat h_desc,h_classes;					
				compute_hog_features(roi,POS_LABEL,h_desc,h_classes);			
				populate_central_db(h_desc,h_classes,"HOG");					
			}			
		}
	}
	for(unsigned int i=0;i<neg_entries.size();i++)
	{
		vec_entry entry = neg_entries[i];
		Mat query_image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);		
		cout<<entry.filename<<endl;
		if(!query_image.empty())
		{			
			int i=0;
			do{
				int x = rand()% (query_image.cols - RAND_WIN_WIDTH);
				int y= rand()% (query_image.rows - RAND_WIN_HEIGHT);					
				Mat roi(query_image,Rect(x,y,RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
				cout<<"got here?"<<endl;
				Mat h_desc,h_classes;
				compute_hog_features(roi,NEG_LABEL,h_desc,h_classes);
				populate_central_db(h_desc,h_classes,"HOG");				
				i++;
			}while(i < rand_neg_wins);			
		}
	}	
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;
	return feature_data;
}
/* the size we are dealing with here is about 64x64, also this function
 * assumes that the vec is incomplete i.e. the only info is the image
 * name and hence the prefix and postfix are needed to get the abs path 
 * */
feature_class_pair features::extract_from_vec(string vecfilename,string negvec,string pos_prefix,string neg_prefix,string extToAppend)
{
	ifstream vecfile(vecfilename);	
	int rand_neg_wins = 3;
	while(true)
	{
		if(vecfile.eof())
		{
			break;
		}
		string facename;
		vecfile >> facename;
		
		if(extToAppend.length() > 0)	
		{
			if(facename.find(".pgm") == string::npos)
			{
				facename.append(".pgm");
			}
		}
		string filename;
		filename.append(pos_prefix);filename.append(facename);
		cout<<filename<<endl;
		Mat roi = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);	
		string errorfile = "trainimages/lfwcrop_face/faces/.pgm";
		if(!roi.empty())
		{
			if(filename.compare(errorfile) ==0)			
				cout<<"we are trying to access features from an empty file"<<endl;
				
			resize(roi,roi,Size(DET_WIN_WIDTH,DET_WIN_HEIGHT));
			Mat h_desc,h_classes;					
			compute_hog_features(roi,POS_LABEL,h_desc,h_classes);			
			populate_central_db(h_desc,h_classes,"HOG");	
			
		}
	}
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;
	vecfile.close();
	/* get the entries */
	ifstream negvecFile(negvec);	
	cout<<negvec<<endl;
	while(true)
	{
		if(negvecFile.eof())
		{
			break;
		}
		string facename;
		negvecFile >> facename;		
		if(facename.find(".pnm") == string::npos)
		{
			facename.append(".pnm");
		}		
		string filename;
		filename.append(neg_prefix);filename.append(facename);
		cout<<filename<<endl;
		Mat roi = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
		/* the reason for resize is because the NICTA neg ped db images are
		 * 64 by 80 */		
		if(!roi.empty())
		{
			resize(roi,roi,Size(RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
			Mat h_desc,h_classes;					
			compute_hog_features(roi,NEG_LABEL,h_desc,h_classes);			
			populate_central_db(h_desc,h_classes,"HOG");	
		}
	}	
	feature_class_pair feature_data;
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;
	return feature_data;
}
feature_class_pair features::extract_vec_for_surf(string vecfilename,string negvec,string pos_prefix,string neg_prefix)
{
	ifstream vecfile(vecfilename);		
	while(true)
	{
		if(vecfile.eof())
		{
			break;
		}
		string facename;
		vecfile >> facename;		
		if(facename.find(".pgm") == string::npos)
		{
			facename.append(".pgm");
		}
		string filename;
		filename.append(pos_prefix);filename.append(facename);
		//cout<<filename<<endl;
		Mat roi = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
		GaussianBlur(roi,roi,Size(3,3),1);
		if(!roi.empty())
		{
			resize(roi,roi,Size(DET_WIN_WIDTH,DET_WIN_HEIGHT));
			Mat h_desc,h_classes;				
			extract_keypoints(roi,POS_LABEL,h_desc,h_classes);			
			populate_central_db(h_desc,h_classes,"SURF");				
		}
	}		
	/* get the entries */
	ifstream negvecFile(negvec);	
	while(true)
	{
		if(negvecFile.eof())
		{
			break;
		}
		string facename;
		negvecFile >> facename;		
		string filename;
		filename.append(neg_prefix);filename.append(facename);
		//cout<<filename<<endl;
		Mat roi = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
		GaussianBlur(roi,roi,Size(3,3),1);
		/* the reason for resize is because the NICTA neg ped db images are
		 * 64 by 80 */		
		if(!roi.empty())
		{
			resize(roi,roi,Size(RAND_WIN_WIDTH,RAND_WIN_HEIGHT));
			Mat h_desc,h_classes;					
			extract_keypoints(roi,NEG_LABEL,h_desc,h_classes);			
			populate_central_db(h_desc,h_classes,"SURF");	
		}
	}	
	feature_class_pair feature_data;
	feature_data.surf_features = get_surf_desc_db();
	feature_data.surf_classes = get_surf_classes_db();
	cout<<"SURF rows:"<<surf_desc_db.rows<<endl;
	cout<<"SURF classes:"<<surf_classes_db.rows<<endl;
	return feature_data;
}
//get_face_images

feature_class_pair features::get_hog_data_for_imgs(vector<Mat> posSamples,vector<Mat> negSamples,int WIN_WIDTH,int WIN_HEIGHT)
{
	cout<<"got data with wxh specified"<<endl;
	feature_class_pair feature_data;
	for(unsigned int i =0;i<posSamples.size();i++)
	{
		Mat query_image = posSamples[i];
		if(!query_image.empty())
		{	
			Mat h_desc,h_classes;
			resize(query_image,query_image,Size(WIN_WIDTH,WIN_HEIGHT));
			compute_hog_features(query_image,POS_LABEL,h_desc,h_classes);
			cout<<"hog rows:"<<h_desc.rows<<endl;
			//cout<<"Pos image:"<<i<<endl;
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}	
	for(unsigned int i =0;i<negSamples.size();i++)
	{
		Mat query_image = negSamples[i];
		if(!query_image.empty())
		{	
			Mat h_desc,h_classes;			
			//cvtColor(query_image,query_image,CV_RGB2GRAY);
			resize(query_image,query_image,Size(WIN_WIDTH,WIN_HEIGHT));
			//cout<<"Neg image:"<<i<<endl;			
			compute_hog_features(query_image,NEG_LABEL,h_desc,h_classes);
			cout<<"hog rows:"<<h_desc.rows<<endl;
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}	
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;	
	cout<<"about to leave?"<<endl;	
	cout<<"got data with wxh specified"<<endl;
	return feature_data;
}


feature_class_pair features::get_hog_data_for_imgs(vector<Mat> posSamples,vector<Mat> negSamples)
{
	feature_class_pair feature_data;
	for(unsigned int i =0;i<posSamples.size();i++)
	{
		Mat query_image = posSamples[i];
		if(!query_image.empty())
		{	
			Mat h_desc,h_classes;
			cvtColor(query_image,query_image,CV_RGB2GRAY);
			compute_hog_features(query_image,POS_LABEL,h_desc,h_classes);
			//cout<<"Pos image:"<<i<<endl;
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}	
	for(unsigned int i =0;i<negSamples.size();i++)
	{
		Mat query_image = negSamples[i];
		if(!query_image.empty())
		{	
			Mat h_desc,h_classes;			
			//cvtColor(query_image,query_image,CV_RGB2GRAY);
			resize(query_image,query_image,Size(64,64));
			cout<<"Neg image:"<<i<<endl;
			compute_hog_features(query_image,NEG_LABEL,h_desc,h_classes);
			populate_central_db(h_desc,h_classes,"HOG");
		}
	}	
	feature_data.hog_features = get_hog_desc_db();
	feature_data.hog_classes = get_hog_classes_db();
	
	cout<<"hog rows:"<<hog_desc_db.rows<<endl;
	cout<<"hog classes:"<<hog_classes_db.rows<<endl;	
	cout<<"about to leave?"<<endl;	
	return feature_data;
}

