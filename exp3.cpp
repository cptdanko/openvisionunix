#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"


#define POS 1
#define NEG -1
#define EXP_HOG_ONLY = 0
#define EXP_SURF_ONLY = 1
#define EXP_BOTH_TYPES = 2

mutex knnMutex;
mutex svmMutex;
mutex boostMutex;

feature_class_pair trainingData;

feature_class_pair trainingDataForBoost;

typedef struct{
	double accuracy;
	int vote;
}df_outcome;

ofstream log_str("resultlog.txt");

CvSVM aSurfSvm,s_idle_svm;
CvSVM aHogSvm,h_idle_svm;
CvKNearest aSurfKnn,aHogKnn;
CvKNearest s_idleKnn,h_idleKnn;
CvBoost aHogBoost,aSurfBoost;
CvBoost h_idleBoost,s_idleBoost;

CvBoostParams boostParams(CvBoost::REAL,75,0.95,2,false,0);

int KTH=24;
string posVec,negVec;

bool activeSvmInUse = true;
bool idleSvmInUse = false;

bool activeKnnInUse = true;
bool idleKnnInUse = false;

bool activeBoostInUse = true;
bool idleBoostInUse = false;



CvSVM* hogSvmPtr;CvSVM* surfSvmPtr;
CvBoost* hogBoostPtr; CvBoost* surfBoostPtr;

float sKnnTreshold = 0.7;	
float sBoostTreshold = 2;
float sSvmTreshold = 0.3;

deque<int> sSvmDetHist,sKnnDetHist,sBoostDetHist;
deque<int> hSvmDetHist,hKnnDetHist,hBoostDetHist;

int HISTORY_TH = 20;

int updateFeaturesCount = 100;

//setup all the variables for the update
feature_class_pair s_passiveKnnData,s_activeKnnData;
feature_class_pair h_passiveKnnData,h_activeKnnData;
feature_class_pair s_passiveSvmData,s_activeSvmData;
feature_class_pair h_passiveSvmData,h_activeSvmData;
feature_class_pair s_passiveBoostData,s_activeBoostData;
feature_class_pair h_passiveBoostData,h_activeBoostData;

bool s_useKnnPassive = false;
bool s_useSvmPassive = false;
bool s_useBoostPassive = false;

bool h_useKnnPassive = false;
bool h_useSvmPassive = false;
bool h_useBoostPassive = false;

bool s_updateKnn = false;
bool s_updateSvm = false;
bool s_updateBoost = false;

bool h_updateKnn = false;
bool h_updateSvm = false;
bool h_updateBoost = false;


CvSVMParams svmParams;


void initialize_svm_params()
{
	svmParams.svm_type = CvSVM::C_SVC;
	svmParams.kernel_type = CvSVM::RBF;
	svmParams.degree = 0.5; svmParams.gamma = 1;
	svmParams.coef0 =1; svmParams.C = 10; svmParams.nu = 0;
	svmParams.p = 0; svmParams.term_crit = cvTermCriteria(CV_TERMCRIT_ITER,1000,0.01);	
}

void populate_50_50_history()
{
        for(int i=0;i<10;i++)
        {
                 sSvmDetHist.push_back(1);
                 sKnnDetHist.push_back(1);
                 sBoostDetHist.push_back(1);
                 hSvmDetHist.push_back(1);
                 hKnnDetHist.push_back(1);
                 hBoostDetHist.push_back(1);
        }
        for(int i=0;i<10;i++)
        {
                 sSvmDetHist.push_back(-1);
                 sKnnDetHist.push_back(-1);
                 sBoostDetHist.push_back(-1);
                 hSvmDetHist.push_back(-1);
                 hKnnDetHist.push_back(-1);
                 hBoostDetHist.push_back(-1);
        }
}

void train_boost(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"Boosting training"<<endl;
	
	CvBoostParams boostParams(CvBoost::REAL,75,0.95,2,false,0);
	//Mat upper = features.rowRange(features.rows)
	cout<< "***********************Boosting training started***********************"<<endl;	
	if(isHTrain)
	{
		Mat varType(1,data.hog_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.hog_features.cols) = CV_VAR_CATEGORICAL;

		cout<<"rows to use for boosting:"<<data.hog_features.rows<<endl;
		aHogBoost.train(data.hog_features,CV_ROW_SAMPLE,data.hog_classes,Mat(),Mat(),varType,Mat(),boostParams,false);		
		cout<<"boost training finished"<<endl;
		
	}
	if(isSTrain){
		cout<<"boosting stuff"<<endl;
		Mat varType(1,data.surf_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.surf_features.cols) = CV_VAR_CATEGORICAL;
		aSurfBoost.train(data.surf_features,CV_ROW_SAMPLE,data.surf_classes,Mat(),Mat(),Mat(),Mat(),boostParams,false);
		
	}
	cout<< "***********************Boosting training finished***********************"<<endl;	
	
}

void train_svm(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"in the classifier"<<endl;

	cout<< "***********************SVM training started***********************"<<endl;	
	if(isHTrain){
		cout<<"training hog"<<endl;
		aHogSvm.train(data.hog_features,data.hog_classes,Mat(),Mat(),svmParams);
		//aHogSvm.save("classifiers/hogsvm.xml");
		
	}
	if(isSTrain){
		cout<<"a surf training"<<endl;
		aSurfSvm.train(data.surf_features,data.surf_classes,Mat(),Mat(),svmParams);
		//aSurfSvm.save("classifiers/surfsvm.xml");
	}
	cout<< "***********************SVM training finished***********************"<<endl;

	//detector.compute_hog_features(&image,1,h_desc,h_class);
}
void train_knn(feature_class_pair& data,bool isHTrain,bool isSTrain)
{	
	cout<< "***********************KNN training started***********************"<<endl;
	if(isHTrain){
		aHogKnn.train(data.hog_features,data.hog_classes,Mat(),true,KTH);
		//iHogKnn.train(data.hog_features,data.hog_classes,Mat(),false,KTH);
	}
	if(isSTrain){
		//iSurfKnn.train(data.surf_features,data.surf_classes,Mat(),false,KTH);
		aSurfKnn.train(data.surf_features,data.surf_classes,Mat(),true,KTH);
	}
	cout<< "***********************KNN training finished***********************"<<endl;
}

void record_classifier_history(int pCount,int nCount,bool useHog,bool useSurf
							  ,int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs)
{
	if(pCount>nCount)
	{
		if(useHog){			
			if(hgSvmRs>0){	utils::update_cls_hist(hSvmDetHist,-1,1);}
			else {	utils::update_cls_hist(hSvmDetHist,1,-1);	}
			
			if(hKnnRs>0){	utils::update_cls_hist(hKnnDetHist,-1,1); } 
			else {	utils::update_cls_hist(hKnnDetHist,1,-1);	}
			
			if(hBoostRs>0){	utils::update_cls_hist(hBoostDetHist,-1,1);	} 
			else {	utils::update_cls_hist(hBoostDetHist,1,-1);	}
		}
		if(useSurf){
			
			if(sSvmRs>0){utils::update_cls_hist(sSvmDetHist,-1,1);} 
			else {utils::update_cls_hist(sSvmDetHist,1,-1);}
			if(sKnnRs>0){utils::update_cls_hist(sKnnDetHist,-1,1);} 
			else {utils::update_cls_hist(sKnnDetHist,1,-1);}
			if(sBoostRs>0){utils::update_cls_hist(sBoostDetHist,-1,1);} 
			else {utils::update_cls_hist(sBoostDetHist,1,-1);}
		}
	}
	else{
		if(useHog)
		{			
			if(hgSvmRs<0){ utils::update_cls_hist(hSvmDetHist,-1,1);} 
			else {	utils::update_cls_hist(hSvmDetHist,1,-1); }
			
			if(hKnnRs<0){	utils::update_cls_hist(hKnnDetHist,-1,1);	} 
			else {utils::update_cls_hist(hKnnDetHist,1,-1);}
			
			if(hBoostRs<0){	utils::update_cls_hist(hBoostDetHist,-1,1); } 
			else { utils::update_cls_hist(hBoostDetHist,1,-1);}			
		}
		if(useSurf)
		{
			if(sSvmRs<0){utils::update_cls_hist(sSvmDetHist,-1,1);} 
			else {utils::update_cls_hist(sSvmDetHist,1,-1);}
			
			if(sKnnRs<0){utils::update_cls_hist(sKnnDetHist,-1,1);} 
			else {utils::update_cls_hist(sKnnDetHist,1,-1);}
			
			if(sBoostRs<0){utils::update_cls_hist(sBoostDetHist,-1,1);} 
			else {utils::update_cls_hist(sBoostDetHist,1,-1);}
		}
	}
}

df_outcome find_max(double svm, double knn, double boost,int svmVote,int knnVote,int boostVote)
{
	df_outcome outcome;
	if(svm>knn){
		if(svm > boost){	
			outcome.accuracy = svm;
			outcome.vote = svmVote;
		}
		else{	
			outcome.accuracy = boost;
			outcome.vote = boostVote;
		}		
	}
	else{		
		if(knn>boost){	
			outcome.accuracy = knn;
			outcome.vote = knnVote;
		}
		else {	
			outcome.accuracy = boost;
			outcome.vote = boostVote;			
		}
	}
	//if they are all the same then we should return the accuracy of the majority
	if(svm==knn && knn==boost)
	{
		int majority = svmVote+knnVote+boostVote;
		if(svmVote ==majority){ outcome.accuracy = svm;	outcome.vote = svmVote; }
		else if(knnVote ==majority){ outcome.accuracy = knn;outcome.vote = knnVote; }
		else{ outcome.accuracy = boost; outcome.vote = boostVote; }
	}
	return outcome;
}

int apply_decision_function(int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs,
							  bool useHog,bool useSurf)
{
	df_outcome hogWinner,surfWinner;
	int outcome = 0;
	if(useHog){
		
		double svmAccuracy = utils::find_historic_accuracy(hSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(hKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(hBoostDetHist);
		//cout<<"hog accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		hogWinner= find_max(svmAccuracy,knnAccuracy,boostAccuracy,hgSvmRs,hKnnRs,hBoostRs);
		//cout<<"hog"<<hogWinner.accuracy<<":and vote:"<<hogWinner.vote<<endl;
		outcome = hogWinner.vote;
	}	
	if(useSurf){
		
		double svmAccuracy = utils::find_historic_accuracy(sSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(sKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(sBoostDetHist);
		//cout<<"accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		surfWinner =  find_max(svmAccuracy,knnAccuracy,boostAccuracy,sSvmRs,sKnnRs,sBoostRs);		
		outcome = surfWinner.vote;
	}
	
	if(useHog && useSurf)
	{
		if(hogWinner.accuracy > surfWinner.accuracy)
			outcome= hogWinner.vote;
		else
			outcome= surfWinner.vote;
		
	}
	return outcome;
}
//we should know what the outcome is by the time we come here
void store_data_for_cls_update(int svmvote,int knnvote, int boostvote,int outcome,
							  bool useHog,bool useSurf,Mat& descriptor,features& detector)
{
	Mat clas = detector.get_class(descriptor,outcome);
	//cout<<"about to store the data for some class updates:"<<endl;
	if(useHog){		
		//cout<<"we are using hog:"<<endl;
		if(svmvote!=outcome)
		{			
			//cout<<"wrong svm vote:"<<endl;
			if(!h_useSvmPassive){//means there is no active training happening at the moment				
				h_activeSvmData.hog_features.push_back(descriptor);
				h_activeSvmData.hog_classes.push_back(clas);				
			}
			else
			{
				h_passiveSvmData.hog_features.push_back(descriptor);
				h_passiveSvmData.hog_classes.push_back(clas);	
			}
		}
		else if(knnvote!=outcome)
		{
			//cout<<"wrong knn vote:"<<endl;
			if(!h_useKnnPassive){//means there is no active training happening at the moment				
				h_activeKnnData.hog_features.push_back(descriptor);
				h_activeKnnData.hog_classes.push_back(clas);				
			}
			else
			{
				h_passiveKnnData.hog_features.push_back(descriptor);
				h_passiveKnnData.hog_classes.push_back(clas);	
			}
		}
		else if(boostvote!=outcome)
		{
			//cout<<"wrong boost vote:"<<endl;
			if(!h_useBoostPassive){//means there is no active training happening at the moment				
				h_activeBoostData.hog_features.push_back(descriptor);
				h_activeBoostData.hog_classes.push_back(clas);				
			}
			else
			{
				h_passiveBoostData.hog_features.push_back(descriptor);
				h_passiveBoostData.hog_classes.push_back(clas);	
			}
		}		
	}
	else if(useSurf)
	{
		if(svmvote!=outcome)
		{			
			if(!s_useSvmPassive){//means there is no active training happening at the moment				
				s_activeSvmData.surf_features.push_back(descriptor);
				s_activeSvmData.surf_classes.push_back(clas);				
			}
			else
			{
				s_passiveSvmData.surf_features.push_back(descriptor);
				s_passiveSvmData.surf_classes.push_back(clas);	
			}
		}
		else if(knnvote!=outcome)
		{
			if(!s_useKnnPassive){//means there is no active training happening at the moment				
				s_activeKnnData.surf_features.push_back(descriptor);
				s_activeKnnData.surf_classes.push_back(clas);				
			}
			else
			{
				s_passiveKnnData.surf_features.push_back(descriptor);
				s_passiveKnnData.surf_classes.push_back(clas);	
			}
		}
		else if(boostvote!=outcome)
		{
			if(!s_useBoostPassive){//means there is no active training happening at the moment				
				s_activeBoostData.surf_features.push_back(descriptor);
				s_activeBoostData.surf_classes.push_back(clas);				
			}
			else
			{
				s_passiveBoostData.surf_features.push_back(descriptor);
				s_passiveBoostData.surf_classes.push_back(clas);	
			}
		}
	}
}

/*
 * classifier to use
 * 0-> just use HOG
 * 1-> just use SURF
 * 2-> use both of the above
 **/
int operate_slidewins(Mat frame,vector<Rect> wins,string imgOutLoc,
			string frameCount,ofstream& csvlog,features& detector,
			classify& predictor,int classifiers_to_use,Mat origFrame,bool useDecisionRule)
{	
	
	int winsDetected = 0;
	if(classifiers_to_use!=2)
	{
		csvlog<<"svmVote,KnnVote,BoostVote,PosDets,NegDets"<<endl;
	}
	else{
		csvlog<<"HogSVMVote,HogKnnVote,HogBoostVote,SurfSVMVote,SurfKnnVote,SurfBoostVote,PosDets,NegDets"<<endl;
	}	
	for(unsigned int winCount=0; winCount< wins.size();winCount++)
	{
		Mat s_desc,s_classes;
		Mat h_desc,h_classes;
		Rect roi = wins[winCount];
		Mat image(frame,roi);
		float hgSvmRs=0;float hKnnRs=0;float hBoostRs=0; 
		float sSvmRs=0;float sKnnRs=0;float sBoostRs=0;
		float pCount =0;float nCount = 0;
	
		bool useHog = false;
		bool useSurf = false;
		if(classifiers_to_use==0 ||classifiers_to_use==2){useHog = true;}
		if(classifiers_to_use==1 ||classifiers_to_use==2){useSurf = true;}				
		
		bool skipDR = false;
		
		/*********************pending*****************************************
		 * Need to specify the decision rule logic for the big bad ensemble of 
		 * 6 classifiers.
		 * ********************************************************************
		 */		
		if(useHog)//only HOG
		{
			detector.compute_hog_features(image,1,h_desc,h_classes);	
			//cout<<"did we reach here?.."<<endl;
			hgSvmRs = predictor.predict_svm(hogSvmPtr,h_desc,0);
			//cout<<"did we reach here,knn?.."<<endl;
			hKnnRs = predictor.predict_knn(aHogKnn,h_desc,sKnnTreshold);			
			//cout<<"did we reach here?..boost"<<endl;
			hBoostRs = predictor.predict_boost(hogBoostPtr,h_desc,sBoostTreshold);						
		
			if(hgSvmRs>0){pCount++;} else {nCount++;}
			if(hKnnRs>0){pCount++;} else {nCount++;}
			if(hBoostRs>0){pCount++;} else {nCount++;}	
			
			if(pCount ==3 || nCount ==3){	skipDR = true;	}
			//cout<<hgSvmRs<<","<<hKnnRs<<","<<hBoostRs<<","<<pCount<<","<<nCount<<endl;
		}
		if(useSurf)//only SURF
		{
			detector.extract_keypoints(image,1,s_desc,s_classes);

			int tempBoostThreshold = 0.9;
			sSvmRs = predictor.predict_svm(surfSvmPtr,s_desc,0);
			sBoostRs = predictor.predict_boost(surfBoostPtr,s_desc,tempBoostThreshold);
			sKnnRs = predictor.predict_knn(aSurfKnn,s_desc,sKnnTreshold);
						
			if(sSvmRs>0){pCount++;} else {nCount++;}
			if(sKnnRs>0){pCount++;} else {nCount++;}
			if(sBoostRs>0){pCount++;} else {nCount++;}	
			
			if(pCount ==3 || nCount ==3){	skipDR = true;	}	
		}
		
		if(useDecisionRule && !skipDR)
		{
			/*to change from the decision function to a simple majority vote, just comment out the line below
			 */
			//cout<<"about to apply the decision function";
			int outcome = 0;
			outcome = apply_decision_function(hgSvmRs,hKnnRs,hBoostRs,
								  sSvmRs,sKnnRs,sBoostRs,
								  useHog,useSurf);
			//cout<<"outcome:"<<outcome<<endl;
			pCount = 0;
			nCount = 0;
			if(outcome == 1){pCount++;}else{nCount++;}
			//cout<<pCount<<nCount<<endl;
			//at this point we are storing the data that can be used to update the classifier	
			//the next thing should be about us updating the classifiers
			/*************************do remember to change this so HE+SE can work ***********************************/
			/*************************right now it does not work***********************************/
			if(useSurf){	
				store_data_for_cls_update(sSvmRs,sKnnRs,sBoostRs,outcome,useHog,useSurf,s_desc,detector);
			}
			else if(useHog){				
				store_data_for_cls_update(hgSvmRs,hKnnRs,hBoostRs,outcome,useHog,useSurf,h_desc,detector);
			}			
		}
		record_classifier_history(pCount,nCount,useHog,useSurf,hgSvmRs,hKnnRs,hBoostRs,sSvmRs,sKnnRs,sBoostRs);
		//first lets find out what does not checkout		
		if(pCount>nCount)
		{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			//cout<<detectionScoresOutput.str()<<endl;
			winsDetected++;			
			rectangle(origFrame,roi,CV_RGB(255,0,0),3);
			//since we have made it here, it means that it is positive outcome
		}
		else{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			//cout<<detectionScoresOutput.str()<<endl;
		}
	}
	return winsDetected;
}
void build_initial_hist(feature_class_pair data,bool useSurf, bool useHog)
{
	classify predictor;	
	Mat features = data.hog_features;
	Mat classes = data.hog_classes;
	cout<<"About to build initial history:"<<features.rows<<endl;
	int T =(HISTORY_TH/2);
	if(useHog) {		
		for(int i=0;i<features.rows;i++)
		{		
			if(i==T)	{	break;	}
			int truth = classes.at<int>(i,0);		
			cout<<truth<<":that was the truth value"<<endl;
			int vote = 0;
			Mat feature = features.row(i);
			vote = predictor.predict_svm(hogSvmPtr,feature,0);
			
			if(vote!=truth){	hSvmDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aHogKnn,feature,sKnnTreshold);					
			if(vote!=truth){	hKnnDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(hogBoostPtr,feature,sBoostTreshold);
			if(vote!=truth){	hBoostDetHist.push_back(-1); }
			else {	hBoostDetHist.push_back(1); }
			
			int rowNo = features.rows-(i+1);
			cout <<rowNo<<endl;
			feature = features.row(rowNo);
			truth = classes.at<int>(rowNo,0);	
			cout<<truth<<":that was the second truth value"<<endl;
			vote = predictor.predict_svm(hogSvmPtr,feature,0);
			
			if(vote!=truth){	hSvmDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aHogKnn,feature,sKnnTreshold);					
			if(vote!=truth){	hKnnDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(hogBoostPtr,feature,sBoostTreshold);
			if(vote!=truth){	hBoostDetHist.push_back(-1); }
			else {	hBoostDetHist.push_back(1); }

		}			
	}	
	features = data.surf_features;
	classes = data.surf_classes;
	cout<<"About to build initial history:"<<features.rows<<endl;
	if(useSurf)
	{
		for(int i=0;i<features.rows;i++)
		{			
			if(i==T)	{	break;	}
			Mat feature = features.row(i);
			int tempBoostThreshold = 0.9;			
			int truth = classes.at<int>(i,0);		
			int vote = 0;
			vote = predictor.predict_svm(surfSvmPtr,feature,0);
			if(vote!=truth){	sSvmDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aSurfKnn,feature,sKnnTreshold);
			if(vote!=truth){	sKnnDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(surfBoostPtr,feature,tempBoostThreshold);
			if(vote!=truth){	sBoostDetHist.push_back(-1); }
			else {	sBoostDetHist.push_back(1); }
			
			int rowNo = features.rows-(i+1);
			cout <<rowNo<<endl;
			feature = features.row(rowNo);
			truth = classes.at<int>(rowNo,0);	
			cout<<truth<<":that was the second truth value"<<endl;
			truth = classes.at<int>(i,0);		
			
			vote = predictor.predict_svm(surfSvmPtr,feature,0);
			if(vote!=truth){	sSvmDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aSurfKnn,feature,sKnnTreshold);
			if(vote!=truth){	sKnnDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(surfBoostPtr,feature,tempBoostThreshold);
			if(vote!=truth){	sBoostDetHist.push_back(-1); }
			else {	sBoostDetHist.push_back(1); }
		}		
	}	
}

void updateBoost(bool isHTrain,bool isSTrain)
{	
	cout<< "***********************Boosting training started***********************"<<endl;	
	if(isHTrain)
	{
		boostMutex.lock();
		h_useBoostPassive = true;
		boostMutex.unlock();
				
		Mat varType(1,trainingDataForBoost.hog_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( trainingDataForBoost.hog_features.cols) = CV_VAR_CATEGORICAL;
		if(activeBoostInUse)
			h_idleBoost.train(trainingDataForBoost.hog_features,CV_ROW_SAMPLE,trainingDataForBoost.hog_classes,Mat(),Mat(),varType,Mat(),boostParams,false);		
		else
			aHogBoost.train(trainingDataForBoost.hog_features,CV_ROW_SAMPLE,trainingDataForBoost.hog_classes,Mat(),Mat(),varType,Mat(),boostParams,false);		
	}
	if(isSTrain){
		cout<<"boosting surf"<<endl;
		Mat varType(1,trainingDataForBoost.surf_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( trainingDataForBoost.surf_features.cols) = CV_VAR_CATEGORICAL;
		if(activeBoostInUse)		
			s_idleBoost.train(trainingDataForBoost.surf_features,CV_ROW_SAMPLE,trainingDataForBoost.surf_classes,Mat(),Mat(),Mat(),Mat(),boostParams,false);
		else
			aSurfBoost.train(trainingDataForBoost.surf_features,CV_ROW_SAMPLE,trainingDataForBoost.surf_classes,Mat(),Mat(),Mat(),Mat(),boostParams,false);
	}
	cout<< "***********************Boosting training finished***********************"<<endl;	
}
void updateSvm(bool isHTrain,bool isSTrain)
{
	if(isHTrain)
	{
		svmMutex.lock();
		h_useSvmPassive = true;
		svmMutex.unlock();

		cout<<"about to update the hog svm with features:"<<trainingData.hog_features.rows<<endl;	
		if(activeSvmInUse)
			h_idle_svm.train(trainingData.hog_features,trainingData.hog_classes,Mat(),Mat(),svmParams);	
		else
			aHogSvm.train(trainingData.hog_features,trainingData.hog_classes,Mat(),Mat(),svmParams);	
		//svm.train(trainingData.hog_features,trainingData.hog_classes,Mat(),Mat(),svmParams);		
	}
	if(isSTrain)
	{
		svmMutex.lock();
		s_useSvmPassive = true;
		svmMutex.unlock();
		cout<<"started updating the srf svm:"<<endl;
		cout<<"about to update the hog svm with features:"<<trainingData.surf_features.rows<<endl;	
		if(activeKnnInUse)
			s_idle_svm.train(trainingData.surf_features,trainingData.surf_classes,Mat(),Mat(),svmParams);		
		else
			aSurfSvm.train(trainingData.surf_features,trainingData.surf_classes,Mat(),Mat(),svmParams);		
		//svm.train(trainingData.hog_features,trainingData.hog_classes,Mat(),Mat(),svmParams);		
	}
	
	if(isHTrain)
	{ 
		svmMutex.lock();
		h_updateSvm = true;
		cout<<"update complete now time to update h_knn"<<endl;
		svmMutex.unlock();
	}
	else { 
		svmMutex.lock();
		s_updateSvm = true; 
		cout<<"update complete now time to update s_knn"<<endl;
		svmMutex.unlock();
	}
}
void updateKnn(bool isHTrain,bool isSTrain)
{
	cout<<"we are in the async function"<<endl;
	knnMutex.lock();
	h_useKnnPassive = true;
	knnMutex.unlock();
	if(isHTrain)
	{
		cout<<"about to update the hog knn"<<endl;
		h_idleKnn.train(h_activeKnnData.hog_features,h_activeKnnData.hog_classes,Mat(),true,KTH,true);
	}
	if(isSTrain)
	{
		cout<<"started updating the s knn:"<<endl;
		s_idleKnn.train(s_activeKnnData.surf_features,s_activeKnnData.surf_classes,Mat(),true,KTH,true);
	}
	
	if(isHTrain)
	{ 
		knnMutex.lock();
		h_updateKnn = true;
		cout<<"update complete now time to update h_knn"<<endl;
		knnMutex.unlock();
	}
	else { 
		knnMutex.lock();
		s_updateKnn = true; 
		cout<<"update complete now time to update s_knn"<<endl;
		knnMutex.unlock();
	}
}

void trigger_boost_change(bool trainForHog,bool trainforSurf,features& detector)
{
	if(trainForHog && (!h_useSvmPassive))
	{
		/*cout<<"checking for updates"<<endl;
		cout<<"train for hog..."<<endl;
		cout<<"h_activeSvmData.hog_features.rows:"<<h_activeBoostData.hog_features.rows<<endl;*/
		async(std::launch::async,updateBoost,trainForHog,trainforSurf);
	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}	
	if(trainforSurf && (!s_useSvmPassive))
	{
		//cout<<"checking for updates"<<endl;
		async(std::launch::async,updateBoost,trainForHog,trainforSurf);
	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}
}
void trigger_svm_change(bool trainForHog,bool trainforSurf,features& detector)
{
	if(trainForHog && (!h_useSvmPassive))
	{
		/*cout<<"checking for updates"<<endl;
		cout<<"train for hog..."<<endl;
		cout<<"h_activeSvmData.hog_features.rows:"<<h_activeSvmData.hog_features.rows<<endl;*/
		async(std::launch::async,updateSvm,trainForHog,trainforSurf);

	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}	
	if(trainforSurf && (!s_useSvmPassive))
	{
		//cout<<"checking for updates"<<endl;
		async(std::launch::async,updateSvm,trainForHog,trainforSurf);
	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}
}
void trigger_cls_change(bool trainForHog,bool trainforSurf,features& detector)
{	
	if(trainForHog && (!h_useKnnPassive))
	{
		cout<<"checking for updates"<<endl;
		cout<<"train for hog..."<<endl;
		cout<<"h_activeKnnData.hog_features.rows:"<<h_activeKnnData.hog_features.rows<<endl;		
		cout<<"More than the update features count.."<<endl;
		h_idleKnn = aHogKnn;
		//invoke the training function then
		async(std::launch::async,updateKnn,trainForHog,trainforSurf);

	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}
	
	if(trainforSurf && (!s_useKnnPassive))
	{
		cout<<"checking for updates"<<endl;	
		s_idleKnn =aSurfKnn;
		async(std::launch::async,updateKnn,trainForHog,trainforSurf);
	}
	else{
		cout<<"there is already something being trained in the background..."<<endl;
	}
}
void check_boost_updates(bool trainForHog,bool trainforSurf,features& detector)
{
	if(h_updateSvm)
	{
		if(activeBoostInUse)
		{
			cout<<"idle..."<<endl;							
			hogBoostPtr = &h_idleBoost;
			activeBoostInUse = false;			
		}
		else
		{
			cout<<"active..."<<endl;							
			hogBoostPtr = &aHogBoost;
			activeBoostInUse = true;			
		}
		detector.empty_matrix(h_activeBoostData.hog_features);
		detector.empty_matrix(h_activeBoostData.hog_classes);
		h_activeBoostData.hog_features = h_passiveBoostData.hog_features.clone();
		h_activeBoostData.hog_classes = h_passiveBoostData.hog_classes.clone();		
		detector.empty_matrix(h_passiveBoostData.hog_features);
		detector.empty_matrix(h_passiveBoostData.hog_classes);
		h_updateKnn = false;
		h_useSvmPassive = false;
	}
	else if(s_updateSvm)
	{
		if(activeBoostInUse)
		{
			cout<<"idle..."<<endl;							
			hogBoostPtr = &s_idleBoost;
			activeBoostInUse = false;			
		}
		else
		{
			cout<<"active..."<<endl;							
			hogBoostPtr = &aHogBoost;
			activeBoostInUse = true;			
		}
		detector.empty_matrix(s_activeBoostData.surf_features);
		detector.empty_matrix(s_activeBoostData.surf_classes);
		s_activeBoostData.surf_features = s_passiveBoostData.surf_features.clone();
		s_activeBoostData.surf_classes = s_passiveBoostData.surf_classes.clone();		
		detector.empty_matrix(s_passiveBoostData.surf_features);
		detector.empty_matrix(s_passiveBoostData.surf_classes);
		s_updateKnn = false;
		s_useSvmPassive = false;
	}
}
void check_svm_updates(bool trainForHog,bool trainforSurf,features& detector)
{
	if(h_updateSvm)
	{
		//cout<<"time to change svms (if we ever get here....) baby.."<<endl;
	
		//cout<<"and back up....."<<endl;		
		if(activeSvmInUse)//if active is being used than the idle is the one that got trained
		{
			cout<<"idle..."<<endl;							
			hogSvmPtr = &h_idle_svm;
			activeSvmInUse = false;
		}
		else
		{
			cout<<"active..."<<endl;
			activeSvmInUse = true;							
			hogSvmPtr = &aHogSvm;
		}
		//cout<<"and pointing back to things..."<<endl;
		detector.empty_matrix(h_activeSvmData.hog_features);
		detector.empty_matrix(h_activeSvmData.hog_classes);
		
		h_activeSvmData.hog_features = h_passiveSvmData.hog_features.clone();
		h_activeSvmData.hog_classes = h_passiveSvmData.hog_classes.clone();		
		detector.empty_matrix(h_passiveSvmData.hog_features);
		detector.empty_matrix(h_passiveSvmData.hog_classes);
		//clear the contents of the active and copy the 
		//contents of the passive to active
		h_updateKnn = false;
		h_useSvmPassive = false;
	}
	else if (s_updateSvm){		
		cout<<"surf:to change svms (if we ever get here....) baby.."<<endl;
		
		if(activeSvmInUse)
		{
			cout<<"idle..."<<endl;							
			surfSvmPtr = &s_idle_svm;
			activeSvmInUse = false;
		}
		else
		{
			cout<<"active..."<<endl;	
			surfSvmPtr = &aSurfSvm;
			activeSvmInUse = true;
		}		
		detector.empty_matrix(s_activeSvmData.surf_features);
		detector.empty_matrix(s_activeSvmData.surf_classes);
		
		s_activeSvmData.hog_features = s_passiveSvmData.surf_features.clone();
		s_activeSvmData.hog_classes = s_passiveSvmData.surf_classes.clone();
	
		detector.empty_matrix(s_passiveSvmData.surf_features);
		detector.empty_matrix(s_passiveSvmData.surf_classes);
		//clear the contents of the active and copy the 
		//contents of the passive to active
		s_updateSvm = false;
		s_useSvmPassive = false;
		cout<<"we have changed the classifiers,say something...(and press enter)"<<endl;
		/*string temp;
		cin>>temp;*/
	}
}
void check_for_updates(bool trainForHog,bool trainforSurf,features& detector)
{
	if(h_updateKnn)
	{
		cout<<"time to change classifiers baby.."<<endl;
		aHogKnn = h_idleKnn;
		detector.empty_matrix(h_activeKnnData.hog_features);
		detector.empty_matrix(h_activeKnnData.hog_classes);
		
		h_activeKnnData.hog_features = h_passiveKnnData.hog_features.clone();
		h_activeKnnData.hog_classes = h_passiveKnnData.hog_classes.clone();		
		detector.empty_matrix(h_passiveKnnData.hog_features);
		detector.empty_matrix(h_passiveKnnData.hog_classes);
		//clear the contents of the active and copy the 
		//contents of the passive to active
		h_updateKnn = false;
		h_useKnnPassive = false;
		cout<<"we have changed the classifiers,say something...(and press enter)"<<endl;
		/*string temp;
		cin>>temp;*/
	}
	else if (s_updateKnn){		
		cout<<"time to change classifiers baby.."<<endl;
		aSurfKnn = s_idleKnn;
		detector.empty_matrix(s_activeKnnData.surf_features);
		detector.empty_matrix(s_activeKnnData.surf_classes);
		
		s_activeKnnData.hog_features = s_passiveKnnData.surf_features.clone();
		s_activeKnnData.hog_classes = s_passiveKnnData.surf_classes.clone();
	
		detector.empty_matrix(s_passiveKnnData.surf_features);
		detector.empty_matrix(s_passiveKnnData.surf_classes);
		//clear the contents of the active and copy the 
		//contents of the passive to active
		s_updateKnn = false;
		s_useKnnPassive = false;
		cout<<"we have changed the classifiers,say something...(and press enter)"<<endl;
		/*string temp;
		cin>>temp;*/
	}
}
/**
	Params expected:
	param 1:experiment no
	Param 2:the name of the video
	Param 3:type of features it uses i.e. hog or surf-pass the res as well
	Param 4:the pos vec
	Param 5:the neg vec
	Param 6:the directory where the detected imgs would be stored
	Param 7:the location of the test video images
	param 8:the output filename
	param 9 & 10:the win width and height respectively
	param 11:the detector classifier combination to use 0-hog, 1-surf, 2-both
*/

void exp3(char** argv)
{
	string expNo = argv[1];
	string vidName = argv[2];
	string featuresUsed = argv[3];
	string pVec = argv[4];
	string nVec = argv[5];
	string imgOutputLoc = argv[6];
	string testimgLoc = argv[7];
	string finalLogName = argv[8];
	string swidth =argv[9];
	string sheight =argv[10];
	int winwidth = atoi(swidth.c_str());
	int winheight = atoi(sheight.c_str());
	string ext=".jpg";
	string sExpcombo = argv[11];
	int expComboType = atoi(sExpcombo.c_str());
	string DR_USAGE = argv[12];
	int useDR = atoi(DR_USAGE.c_str());
	bool useDecisionRule = false;
	if(useDR==1)
	{
		useDecisionRule = true;
	}
		
	ofstream rLog(finalLogName.c_str());
	string frameLogName =imgOutputLoc+finalLogName+"_frame.txt";
	ofstream frameLog(frameLogName.c_str());
	
	int posAsPos = 0;int negAsPos = 0;
	int posAsNeg = 0;int negAsNeg = 0;
	
	classify predictor;
	features detector(winwidth,winheight);
	
	//feature_class_pair data = detector.prepare_train_data(pVec.c_str(), nVec.c_str(),7);
	
	trainingData = detector.get_limited_data(pVec.c_str(), nVec.c_str(),7,100,100);
		
	bool trainForHog=false;bool trainforSurf=false;
	
	if((expComboType==0)||(expComboType==2))
	{ 
		trainForHog = true;
		hogSvmPtr = new CvSVM;
		hogBoostPtr = new CvBoost;
		/*hogSvmPtr->load("classifiers/hogsvm.xml");
		hogBoostPtr->load("classifiers/hogBoost.xml");*/
	}	
	if(expComboType==1||expComboType==2)
	{
		trainforSurf =true;
		surfBoostPtr = new CvBoost;
		surfSvmPtr = new CvSVM;				
		/*surfSvmPtr->load("classifiers/surfsvm.xml");
		surfBoostPtr->load("classifiers/surfBoost.xml");*/
	}
	
	/*train_svm(data,trainForHog,trainforSurf);
	 *train_boost(data,trainForHog,trainforSurf);
	 */	
	train_svm(trainingData,trainForHog,trainforSurf);
	train_boost(trainingData,trainForHog,trainforSurf);
	train_knn(trainingData,trainForHog,trainforSurf);
	hogSvmPtr = &aHogSvm;
	hogBoostPtr = &aHogBoost;
	surfSvmPtr = &aSurfSvm;
	surfBoostPtr = &aSurfBoost;	
	
	//once we have the classifiers built, lets populate the intial history		
	feature_class_pair dataForInitialHist = detector.get_limited_data(pVec.c_str(), nVec.c_str(),7,2,2);
			
	build_initial_hist(dataForInitialHist,trainforSurf,trainForHog);

	string posdir = testimgLoc;
	posdir+="pos";
	cout<<"posdir:"<<posdir<<endl;
	string negdir  =testimgLoc;
	negdir+="neg";
	//cout<<"negdir:"<<negdir<<endl;
	vector<string> posfiles;
	utils::get_files_in_dir(posdir,posfiles);
	vector<string> negfiles;
	utils::get_files_in_dir(negdir,negfiles);
	frameLog<<"expNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"imgName"<<","<<"Type"<<",totalWins"<<","<<"posAsPos"<<","<<"posAsNeg,"<<"negAsPos,"<<"negAsNeg"<<endl;
	for(unsigned int filecount=0;filecount<posfiles.size();filecount++)
	{	
		//this is where we commence the async stuff
		if(h_activeKnnData.hog_features.rows > updateFeaturesCount)
		{
			trigger_cls_change(trainForHog,trainforSurf,detector);
		}
		check_for_updates(trainForHog,trainforSurf,detector);
		
		if(h_activeSvmData.hog_features.rows > updateFeaturesCount)
		{
			trainingData.hog_features.push_back(h_activeSvmData.hog_features);
			trainingData.hog_classes.push_back(h_activeSvmData.hog_classes);			
			trigger_svm_change(trainForHog,trainforSurf,detector);
		}
		check_svm_updates(trainForHog,trainforSurf,detector);
		
		/*if(h_activeBoostData.hog_features.rows > updateFeaturesCount)
		{
			trainingDataForBoost.hog_features.push_back(h_activeBoostData.hog_features);
			trainingDataForBoost.hog_classes.push_back(h_activeBoostData.hog_classes);
		}*/
		//so we have gone off and launched the function
		//now what? ok lets do a check for that th
		Mat newFrame = imread(posdir+"/"+posfiles[filecount]);	
		Mat frame;	
		int size = h_activeSvmData.hog_features.rows;
		cout<<"knn active features size:"<<size<<endl;
		cout<<"svm active features size:"<<h_activeSvmData.hog_features.rows<<endl;
		cout<<"boost active features size:"<<h_activeBoostData.hog_features.rows<<endl;
		
		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		string imgName = posfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
				
		//cout<<"detecting on image:"<<imgName.c_str()<<endl;
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		ofstream csvLog(tempLogName.c_str());
		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame,useDecisionRule);
		if(result>0)
			posAsPos++;
		else
			posAsNeg++;
		
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"POS,"<<wins.size()<<","<<posAsPos<<","<<posAsNeg<<",0,0"<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();	
		//so heres the template, experiment name, name,filename,res,total wins, posWins	
	}
	for(unsigned int filecount=0;filecount<negfiles.size();filecount++)
	{
		cout<<"knn active features size:"<<h_activeKnnData.hog_features.rows<<endl;
		if(h_activeKnnData.hog_features.rows > updateFeaturesCount)
		{
			trigger_cls_change(trainForHog,trainforSurf,detector);
		}
		check_for_updates(trainForHog,trainforSurf,detector);
		
		if(s_activeSvmData.surf_features.rows > updateFeaturesCount)
		{
			trainingData.surf_features.push_back(s_activeSvmData.surf_features);
			trainingData.surf_classes.push_back(s_activeSvmData.surf_classes);			
			trigger_svm_change(trainForHog,trainforSurf,detector);
		}
		check_svm_updates(trainForHog,trainforSurf,detector);

		Mat newFrame = imread(negdir+"/"+negfiles[filecount]);		
		Mat frame;		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		
		string imgName = negfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
		
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		cout<<"detecting on image:"<<imgName.c_str()<<endl;
				
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		ofstream csvLog(tempLogName.c_str());		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame,useDecisionRule);
		
		if(result>0)
			negAsPos++;
		else
			negAsNeg++;
					
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"NEG,"<<wins.size()<<",0,0,"<<negAsPos<<","<<negAsNeg<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();		
	}
	rLog<<"ExpNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"FilesTotal"<<","<<"posAsPos"<<","<<"posAsNeg"<<","<<"negAsPos"<<","<<"negAsNeg"<<endl;
	rLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<(posfiles.size()+negfiles.size())<<","<<posAsPos<<","<<posAsNeg<<","<<negAsPos<<","<<negAsNeg<<endl;
	frameLog.close();
	rLog.close();	
}
int main(int argc,char** argv)
{
	//test_DF();
	initialize_svm_params();
	string firstArg = argv[1]; 
	if(firstArg=="help")
	{
		utils::help();
		return 0;
	}
	exp3(argv);	
	cout<<"**************************************files saved**************************************"<<endl;
}
