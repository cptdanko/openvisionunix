#include "vision.h"

typedef struct{
	Mat surf_features;
	Mat surf_classes;
	Mat hog_features;
	Mat hog_classes;

}feature_class_pair;

enum TEST_SAMPLES{
	POS,
	NEG
};

enum ALGORITHM_TYPE{
	SIFT,
	SURF,
	HOG,
	HAAR
};

enum ML_ALGORITHM{
	SpVM,
	BAYES,
	ANN
};

typedef struct{
	double keypoint_calc;
	double descriptors_calc;
}extraction_times;

typedef struct{
	string kp_name;
	string desc_name;
	vector<extraction_times> times;
}extraction_details;

typedef struct{
	SVM *svm;
	NormalBayesClassifier *bayes;
	ML_ALGORITHM to_use;
	}classifier_ls;
	


class features{

	int DET_WIN_WIDTH;
	int DET_WIN_HEIGHT;
	
	int RAND_WIN_WIDTH;
	int RAND_WIN_HEIGHT;
public:
	
	features();
	features(int detWidth,int detHeight):
					DET_WIN_WIDTH(detWidth),
					DET_WIN_HEIGHT(detHeight){};
	
	features(int detWinWidth,int detWinHeight,int randWinWidth,int randWinHeight):
				DET_WIN_WIDTH(detWinWidth),
				DET_WIN_HEIGHT(detWinHeight),
				RAND_WIN_WIDTH(randWinWidth),
				RAND_WIN_HEIGHT(randWinHeight){};
	
	feature_class_pair prepare_train_data(string pos_vec_file,
						string neg_vec_file,
						//string extraction_log,
						int rand_neg_wins);
	feature_class_pair get_train_data_from_vec(string pos_vec_file,
						string neg_vec_file,
						//string extraction_log,
						int rand_neg_wins,
						int feature_type);
	
	feature_class_pair get_limited_data(string pos_vec,string neg_vec,int rand_neg_wins,int noOfPosEntries =-1,int noOfNegEntries=-1);
	
	
	feature_class_pair get_surf_data(string pos_vec,string neg_vec,
											int rand_neg_wins,int noOfPosEntries = -1,
											int noOfNegEntries = -1);
											
	/* use all the images specified in the dataset by default */										
	feature_class_pair get_hog_data(string pos_vec,string neg_vec,
									int rand_neg_wins,int noOfPosEntries =-1,
									int noOfNegEntries=-1);
	
	feature_class_pair get_hog_data_for_tud(string pos_vec,string neg_vec,int rand_neg_wins,int doFullBODY = -1,int noOfPosEntries =-1,int noOfNegEntries=-1);
	
	feature_class_pair get_hog_data_wout_resize(string pos_vec,string neg_vec,int rand_neg_wins,int noOfPosEntries =-1,int noOfNegEntries=-1);
	
	Mat get_surf_desc_db();
	Mat get_surf_classes_db();	
	Mat get_hog_desc_db();
	Mat get_hog_classes_db();
	
	Mat get_class(Mat& descriptors,int sampleType);
	
	void compute_hog_features(IplImage* frame,int sampleType,Mat& descriptors,Mat& classes);
	void compute_hog_features(Mat& frame,int sampleType,Mat& descriptors,Mat& classes);	
	void compute_hog_features(Mat& image,Mat& descriptors);
	
	void extract_keypoints(IplImage* frame,int sampleType,Mat& descriptors,Mat& classes);
	void extract_keypoints(Mat& frame,int sampleType,Mat& descriptors,Mat& classes);
	void extract_keypoints(Mat& image,Mat& descriptors);
	
	void empty_matrix(Mat& m);
	
	feature_class_pair extract_from_vec(string vecfilename,string negvec,string pos_prefix,string neg_prefix,string extToAppend = "");
	feature_class_pair extract_vec_for_surf(string vecfilename,string negvec,string pos_prefix,string neg_prefix);
	
	feature_class_pair get_hog_data_for_imgs(vector<Mat> posSamples,vector<Mat> negSamples);
};	



