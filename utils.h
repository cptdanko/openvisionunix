#include "vision.h"

typedef struct{
	int x,y,width,height;
} vec_object;

typedef struct{
	string filename;
	int noOfObjects;
	vector<vec_object> objects;	
}vec_entry;
class utils{


public:
	static vector<Rect> get_sliding_windows(Mat& image,int winWidth,int winHeight);
	static vector<Rect> get_sliding_windows(Mat& image,int winWidth,int winHeight,int strideX,int strideY);
	static void help();
	static bool validate_params(char** argv);
	static void prep_rs_header(ofstream& stream);
	static void get_files_in_dir(string dirpath,vector<string>& filenames);
	static float round(float n);//round up a float type and show one decimal place
	static void get_vec_entries(string vecfilename,vector<vec_entry>& entries);
	
	static double calculate_dist(Point p1,Point p2);
	static double calculate_dist(Rect p1,Rect p2);
	
	static void bubble_sort(vector<Point>& points);
	static void bubble_sort(vector<Rect>& boxes);
	
	static double find_historic_accuracy(deque<int>& history);
	static void update_cls_hist(deque<int>& history,int toRemove,int toInsert);
	
	static int get_max(vector<Point> points, int type);
	static int get_min(vector<Point> points, int type);
	
	static int get_max(vector<Rect> points, int type);
	static int get_min(vector<Rect> points, int type);
	
	static void print_vector(vector<Rect> p);	
	
	static double rand_btwn_zero_and_one();
	static double rand_number(int lowerBound, int upperBound);
	static IplImage* rotateImage(const IplImage *src, float angleDegrees);
	
	static int gen_natural_rand_no(int upperBound);
	
	static void write_images(string videoname,string destinationDir);

	static void get_LMDa_torso_crops(vector<Mat>& images);
	static void get_nicta_neg_peds(vector<Mat>& images);
	static void get_body_part_vecs(string vecfilename,vector<string>& entries);
};
