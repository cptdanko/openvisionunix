/**********shogun imports**********************/
#include <shogun/kernel/GaussianKernel.h>
#include <shogun/features/SimpleFeatures.h>
#include <shogun/classifier/svm/LibSVM.h>
#include <shogun/mathematics/Math.h>
#include <shogun/lib/common.h>
#include <shogun/base/init.h>
#include <shogun/base/Parameter.h>
#include <shogun/io/SerializableAsciiFile.h>
#include <shogun/io/SGIO.h>
#include <shogun/lib/ShogunException.h>
#include <shogun/evaluation/CrossValidation.h>
#include <shogun/evaluation/ContingencyTableEvaluation.h>
#include <shogun/evaluation/StratifiedCrossValidationSplitting.h>

/**********opencv imports**********************/
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <opencv2/legacy/legacy.hpp>
/**************************************************/
#include <opencv/ml.h>
#include <opencv/cv.h>

/**************************************************/
#include <vector>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include <dirent.h>
#include <cmath>
#include <deque>
#include <map>
#include <future>
#include <cstdlib>

using namespace std;
using namespace cv;
using namespace shogun;
