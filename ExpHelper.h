#include "vision.h"

typedef struct{
	int svmVote;
	int knnVote;
	int boostVote;
}vote_collection;

typedef struct{
	deque<int> svm;
	deque<int> knn;
	deque<int> boost;
}ensemble_hist;

typedef struct{
	double accuracy;
	int vote;
}df_outcome;

typedef struct
{
	Rect rect;
	int groupId;	
}node;

typedef struct{	
	CLibSVM* shogun_svm;	
	SVM* cv_svm;	
	bool useShogun;
}cls_ensemble;

typedef struct:cls_ensemble{
	CLibSVM* hand_svm;	
	CLibSVM* leg_svm;
	CLibSVM* torso_svm;
	CLibSVM* face_svm;
}parts_ensemble;

typedef struct{
	int PAsP;
	int PAsN;
	int NAsP;
	int NAsN;	
}conf_matrix;

typedef struct{
	string posvec;
	string negvec;
	int algorithmType;
	string savefilename;	
	string posprefix;
	string negprefix;
	int trainimgwidth;
	int trainimgheight;
	int samplewidth;
	int sampleheight;
	int randwidth;
	int randheight;
	string imgextensiontoadd;
}training_params;

typedef struct{
	string classifiername;
	string videoname;
	string imageoutputLoc;
	int testtype;
	double svmthreshold;
	int samplewidth;
	int sampleheight;
	string detectionLogName;
}testing_params;

typedef struct:training_params{
	string testvecfile;	
	int iterations;
}bootstrap_params;

typedef struct:testing_params{
	string faceclassifiername;
	string torsoclassifiername;
	string handclassifiername;
	int FACE_DET_WIDTH;
	int FACE_DET_HEIGHT;
	
}ensemble_tst_params;
class exp_controller{

public:
	static const int POSITIVE = 1;
	static const int NEGATIVE =-1;
	
	static const int TYPE_HOG = 1;
	static const int TYPE_SURF = 2;
	
	double SVM_DET_THRESHOLD;	
	exp_controller(double svm_threshold=0.00):SVM_DET_THRESHOLD(svm_threshold){};
		
	static void record_classifier_history(int pCount,int nCount,bool useHog,bool useSurf,
									vote_collection votes,ensemble_hist& history);
																		
	static int apply_decision_function(vote_collection votes,ensemble_hist& history);	
									
	static void find_groups(vector<node>& nodes,Mat& clone,
							map<int,vector<Rect>>& groups,ofstream& detLog,string imagename,int distThreshold = 100);
	
	int detect_object(int DW_WIDTH,int DW_HEIGHT,string imagename,cls_ensemble& ensemble,Mat& drawnImage);
	int detect_surf_object(int DW_WIDTH,int DW_HEIGHT,string imagename,cls_ensemble& ensemble,Mat& drawnImage);
	
	void analyse_imgs_for_object(string imagefolder,cls_ensemble& ensemble);
	void loop_helper(vector<string>& filecollection, int type,cls_ensemble& ensemble,
				conf_matrix& resultMat,string outputLocation,string testImagesLoc);
				
	static void train_svm(training_params params);
	static void train_svm_fullbody(training_params params);
	
	void test_svm(testing_params params);
	
	void bootstrap_train(bootstrap_params& params);

	void train_LMDa_upper_body(training_params params);
	int detect_obj_for_ensemble(int DW_WIDTH,int DW_HEIGHT,string imagename,parts_ensemble& ensemble,Mat& drawnImage,ofstream& detlog);
	void test_ensemble(ensemble_tst_params params);
};
