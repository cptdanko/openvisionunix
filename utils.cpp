#include "utils.h"

double utils::find_historic_accuracy(deque<int>& history)
{
	int pCount =0;
	for(unsigned int i=0;i<history.size();i++)
	{
		if(history[i]==1)
		{ 			
			pCount++;			
		}
	}
	double p = (double)pCount;
	double s = (double)history.size();
	/*cout<<"p:"<<pCount<<endl;
	cout<<"s:"<<s<<endl;*/
	return (p/s);
}
void utils::update_cls_hist(deque<int>& history,int toRemove,int toInsert)
{
	unsigned int i;
	for(i=0;i<history.size();i++)
	{
		if(history[i]==toRemove)
		{ 
			break;
		}
	}
	history.erase(history.begin()+i);
	history.push_back(toInsert);
}
vector<Rect> utils::get_sliding_windows(Mat& image,int winWidth,int winHeight,int strideX,int strideY)
{
	vector<Rect> rects;
	for(int i=0;i<image.rows;i+=strideY)
	{
		if((i+winHeight)>image.rows){break;}
		for(int j=0;j< image.cols;j+=strideX)	
		{
			if((j+winWidth)>image.cols){break;}
			Rect rect(j,i,winWidth,winHeight);
			rects.push_back(rect);
		}
	}
	return rects;
}
/**
 * This uses the default params for the win stride, i.e. 16x16
 **/
vector<Rect> utils::get_sliding_windows(Mat& image,int winWidth,int winHeight)
{
	vector<Rect> rects;
	int step = 16;
	for(int i=0;i<image.rows;i+=step)
	{
		if((i+winHeight)>image.rows){break;}
		for(int j=0;j< image.cols;j+=step)	
		{
			if((j+winWidth)>image.cols){break;}
			Rect rect(j,i,winWidth,winHeight);
			rects.push_back(rect);
		}
	}	
	return rects;
}
void utils::help()
{
	cout<<"************************************Training help*************************************"<<endl;
	cout<<"Param 1: train new classifier or load"<<endl;
	cout<<"Param 1: pos vec file"<<endl;
	cout<<"Param 2: neg vec file"<<endl;
	cout<<"Param 3: random negative windows"<<endl;
	cout<<"Param 4: the name of surf svm classifier"<<endl;
	cout<<"Param 5: the name of hog svm classifier"<<endl;
	cout<<"Param 6: the name of surf knn classifier"<<endl;
	cout<<"Param 7: the name of hog knn classifier"<<endl;
	cout<<"Param 8: this should either be the test video or the directory where they are stored"<<endl;
	cout<<"************************************Training help*************************************"<<endl;
}
bool utils::validate_params(char** argv)
{
	if(argv[1]==NULL){
		cout<<"param 1 eko wapi?try the help command to know what the params are!"<<endl;
		return false;
	}
	if(argv[2]==NULL){
		cout<<"param 2 eko wapi?try the help command to know what the params are!"<<endl;
		return false;
	}
	if(argv[3]==NULL){
		cout<<"param 3 eko wapi?try the help command to know what the params are!"<<endl;
		return false;
	}
	if(argv[4]==NULL){
		cout<<"param 4 eko wapi?try the help command to know what the params are!"<<endl;
		return false;
	}
	if(argv[5]==NULL){
		cout<<"param 5 eko wapi?try the help command to know what the params are!"<<endl;
		return false;
	}
	return true;
}
void utils::prep_rs_header(ofstream& stream)
{
	stream<<"vidname,"<<"frame-no,"<<"detName,"<<"svm-vote,"<<"knn-vote,"<<"tree-vote,"<<"winsDetected,"<<"detOutcome"<<endl;
}
float utils::round(float n)//round up a float type and show one decimal place
{
      float t;
      t=n-floor(n);
      if (t>=0.5)    
      {
              n*=10;//where n is the multi-decimal float
              ceil(n);
              n/=10;
              }
      else 
      {
              n*=10;//where n is the multi-decimal float
              floor(n);
              n/=10;
              }
      return n;
}    
void utils::get_files_in_dir(string dirpath,vector<string>& filenames)
{
	DIR *dir;
	struct dirent *entry;
	dir = opendir(dirpath.c_str());
	if(dir!=NULL){
		while((entry =readdir(dir))!=NULL)
		{
			if(strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0)
			{
				string name(entry->d_name);
				filenames.push_back(name);
			}	
		}
		closedir(dir);
	}
	else{
		cout<<"could not open directory"<<endl;
	}
}
void write_out_images()
{
	ofstream logger("slidewins.txt");
	Mat newframe = imread("../test.png");
	IplImage image = newframe;
	vector<Rect> wins = utils::get_sliding_windows(newframe,80,120);
	for(unsigned int i=0; i< wins.size();i++)
	{
		Rect r = wins[i];
		ostringstream tr;
		tr<<"i="<<i<<":x,y,win,height:"<<r.x<<","<<r.y<<","<<r.width<<","<<r.height<<",";
		logger<<tr.str().c_str()<<endl;
		cvSetImageROI(&image,r);
		ostringstream stream;
		stream<<"test/"<<i<<".jpg";
		cvSaveImage(stream.str().c_str(),&image);
		cvResetImageROI(&image);
	}
	logger.close();
}

void utils::get_vec_entries(string vecfilename,vector<vec_entry>& entries)
{
	ifstream file(vecfilename.c_str());	
	while(true)
	{
		if(file.eof()){ break; }
		
		vec_entry entry;
		vector<vec_object> objects;
		int obj_count;
		file >> entry.filename >> obj_count;
		entry.noOfObjects = obj_count;
		//cout<<"filename:"<<entry.filename<<endl;
		for(int i=0;i<obj_count;i++)
		{
			vec_object object;
			file>>object.x >> object.y>>object.width>>object.height;
			objects.push_back(object);
		}
		entry.objects = objects;
		entries.push_back(entry);
	}
}
void utils::get_body_part_vecs(string vecfilename,vector<string>& entries)
{
	ifstream file(vecfilename.c_str());	
	while(true)
	{
		if(file.eof()){ break; }
	
		string filename;
		string partname;
		file >> filename >> partname;
		vec_object object;
		file>>object.x >> object.y>>object.width>>object.height;		
		cout<<"pushing back:"<<filename<<endl;
		entries.push_back(filename);
	}
}

Point find_centroid(Rect r)
{
	int endX = r.x+r.width;
	int endY = r.y+r.height;
	int centreX = (endX + r.x)/2;
	int centreY = (endY + r.y)/2;
	return Point(centreX,centreY);
}
double utils::calculate_dist(Point p1,Point p2)
{	
	double value =pow((p2.x-p1.x),2) + pow((p2.y - p1.y),2);
	return sqrt(value);
}
/*
 * distance calculations between rects should be between their centroids
 */
double utils::calculate_dist(Rect p1,Rect p2)
{
	/*double value =pow((p2.x-p1.x),2) + pow((p2.y - p1.y),2);
	return sqrt(value);*/
	return calculate_dist(find_centroid(p1),find_centroid(p2));
}

void utils::bubble_sort(vector<Rect>& boxes)
{
	bool sorted = false;
	while(!sorted)
	{
		sorted = true;
		for(unsigned int i =0;i!=(boxes.size()-1);i++)
		{
			if(boxes[i].x > boxes[i+1].x)
			{
				Rect temp = boxes[i];
				boxes[i] = boxes[i+1];
				boxes[i+1]=temp;
				sorted = false;
			}
		}	
	}
}
void utils::bubble_sort(vector<Point>& points)
{
	bool sorted = false;
	while(!sorted)
	{
		sorted = true;
		for(unsigned int i =0;i!=(points.size()-1);i++)
		{
			if(points[i].x > points[i+1].x)
			{
				Point temp = points[i];
				points[i] = points[i+1];
				points[i+1]=temp;
				sorted = false;
			}
		}	
	}
}
/*
 * Make sure the bounds are checked and the length is 2 or more
 **/
int utils::get_max(vector<Point> points, int type)
{
	int max = 0;
	if(type==0)//0 is x
	{
		max  = points[0].x;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].x > max)
				max = points[i].x;
		}
	}
	else if(type == 1)//this means y
	{
		max  = points[0].y;
		for(unsigned int i=1;i<points.size();i++)
		{
			if (points[i].y> max)
				max = points[i].y;
		}
	}
	return max;
}
int utils::get_min(vector<Point> points, int type)
{
	int min = 0;
	if(type==0)//0 is x
	{
		min  = points[0].x;
		//cout<<"points size:"<<points.size()<<endl;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].x < min)
				min = points[i].x;
		}
	}
	if(type == 1)
	{
		min  = points[0].y;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].y < min)
				min = points[i].y;
		}
	}
	return min;
}
int utils::get_min(vector<Rect> points, int type)
{
	int min = 0;
	if(type==0)//0 is x
	{
		min  = points[0].x;
		//cout<<"points size:"<<points.size()<<endl;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].x < min)
				min = points[i].x;
		}
	}
	if(type == 1)
	{
		min  = points[0].y;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].y < min)
				min = points[i].y;
		}
	}
	return min;
}
int utils::get_max(vector<Rect> points, int type)
{
	int max = 0;
	if(type==0)//0 is x
	{
		max  = points[0].x;
		for(unsigned int i=1;i<points.size();i++)
		{
			if(points[i].x > max)
				max = points[i].x;
		}
	}
	else if(type == 1)//this means y
	{
		max  = points[0].y;
		for(unsigned int i=1;i<points.size();i++)
		{
			if (points[i].y> max)
				max = points[i].y;
		}
	}
	return max;
}
void utils::print_vector(vector<Rect> p)
{
	for(unsigned int i=0;i<p.size();i++)
	{
		cout<<"("<<p[i].x<<":"<<p[i].y<<"),";
	}
	cout<<endl;
}
double utils::rand_btwn_zero_and_one()
{
	double randnum;
	//srand( unsigned(time(NULL)));

	randnum =(double)rand()/RAND_MAX;
	randnum = floor(randnum*1000)/1000;		
	
	return randnum;
} 

double utils::rand_number(int lowerBound, int upperBound)
{
	double randnum;
	//srand( unsigned(time(NULL)));

	randnum =(double)rand()/RAND_MAX +lowerBound +(rand()%upperBound);
	randnum = floor(randnum*1000)/1000;		
	
	return randnum;
}
int utils::gen_natural_rand_no(int upperBound)
{
	return rand()%upperBound;
}
IplImage* utils::rotateImage(const IplImage *src, float angleDegrees)
{
	// Create a map_matrix, where the left 2x2 matrix
	// is the transform and the right 2x1 is the dimensions.
	float m[6];
	CvMat M = cvMat(2, 3, CV_32F, m);
	int w = src->width;
	int h = src->height;
	float angleRadians = angleDegrees * ((float)CV_PI / 180.0f);
	m[0] = (float)( cos(angleRadians) );
	m[1] = (float)( sin(angleRadians) );
	m[3] = -m[1];
	m[4] = m[0];
	m[2] = w*0.5f;  
	m[5] = h*0.5f;  

	// Make a spare image for the result
	CvSize sizeRotated;
	sizeRotated.width = cvRound(w);
	sizeRotated.height = cvRound(h);

	// Rotate
	IplImage *imageRotated = cvCreateImage( sizeRotated,
		src->depth, src->nChannels );

	// Transform the image
	cvGetQuadrangleSubPix( src, imageRotated, &M);	
	cvSaveImage("f203Rotate.jpg",imageRotated);
	return imageRotated;
}

void utils::write_images(string videoname,string destinationDir)
{
	VideoCapture cap(videoname);
	if(!cap.isOpened())  // check if we succeeded
			cout<<"unable to open the video"<<endl;
	int count = 0;	
	string pred = "frame";
	string ext = ".jpg";
	while(true)
	{
		if(!cap.isOpened())
		{
			cout<<"break reached"<<endl;
			break;
		}
		Mat frame; 
		cap >> frame;		
		if(frame.empty()) break;

		resize(frame,frame,Size(640,480));
		ostringstream str;
		str << destinationDir << pred<<"_"<<count<<ext;
		imwrite(str.str(),frame);
		count++;		
		imshow("frame",frame);
		if(waitKey(30) >= 0) break;
	}	
}
void utils::get_nicta_neg_peds(vector<Mat>& images)
{
	//ifstream negvecFile("vecfiles/nictaNegPeds8K.vec");
	ifstream negvecFile("vecfiles/nictaNeg15K.vec");
	
	string neg_prefix = "trainimages/nictaNegPeds/";
	while(true)
	{
		if(negvecFile.eof())
		{
			break;
		}
		string facename;
		negvecFile >> facename;		
		if(facename.find(".pnm") == string::npos)
		{
			facename.append(".pnm");
		}		
		string filename;
		filename.append(neg_prefix);filename.append(facename);
		cout<<filename<<endl;
		Mat roi = imread(filename,CV_LOAD_IMAGE_GRAYSCALE);
		if(!roi.empty())
		{
			images.push_back(roi);
		}
	}	
}
void add_rotations(Mat& source,vector<Mat>& images)
{	
	IplImage image = source;
	/* add the first image */	
	Mat rotatedImage;
	
	IplImage* rotate45 = utils::rotateImage(&image,-45);
	rotatedImage = rotate45;
	imwrite("1.jpg",rotatedImage);
	images.push_back(rotatedImage);
	
	IplImage* rotate90 = utils::rotateImage(&image,-90);
	rotatedImage = rotate90;
	images.push_back(rotate90);
	imwrite("2.jpg",rotatedImage);

	rotate45 = utils::rotateImage(&image,-135);
	rotatedImage = rotate45;
	images.push_back(rotatedImage);
	imwrite("3.jpg",rotatedImage);

	rotate45 = utils::rotateImage(&image,45);
	rotatedImage = rotate45;
	images.push_back(rotatedImage);
	imwrite("4.jpg",rotatedImage);

	rotate90 = utils::rotateImage(&image,90);
	rotatedImage = rotate90;
	images.push_back(rotatedImage);
	imwrite("5.jpg",rotatedImage);

	rotate90 = utils::rotateImage(&image,135);
	rotatedImage = rotate45;
	images.push_back(rotatedImage);
	imwrite("6.jpg",rotatedImage);

	//cvShowImage("images",utils::rotateImage(&image,90));
	cvReleaseImage(&rotate90);
	cvReleaseImage(&rotate45);
}
void utils::get_LMDa_torso_crops(vector<Mat>& images)
{
	//showBBBox();
	bool add_rotates = true;
	string anno_filename = "testvideos/LAMDa/torso_annotation.txt";
	ifstream file(anno_filename.c_str());	
	string parentDirname = "testvideos/LAMDa/";
	while(true)
	{
		if(file.eof()){ break; }
		string filename,part_type;
			
		double x1=0.00;double y1=0.00;
		double x2=0.00;double y2=0.00;
		
		file >> filename; 
		file >> part_type;		
		file >> x1;file >> y1;
		file >> x2;file >> y2;

		string fname;
		fname.append(parentDirname);
		fname.append(filename);

		//cout<<fname<<endl;
		Mat image = imread(fname);
		if(image.empty()){
			cout<<"cannot read the image?"<<endl;
			break;
		}
		int width = abs(abs(x2)-abs(x1));
		int height = abs(abs(y2)-abs(y1));		
		
		IplImage* plImg = cvLoadImage(fname.c_str());
		cvSetImageROI(plImg,cvRect(abs(x1),abs(y1),width,height));
		Mat newImg = plImg;
		resize(newImg,newImg,Size(64,64));
		images.push_back(newImg);

		if(add_rotates)		
			add_rotations(newImg,images);

		/*imshow("cropped",newImg);
		Mat roi = image(rect);
		rectangle(image,Point(x1,y1),Point(x2,y2),CV_RGB(0,255,0),2);
		imshow("window",image);
		imshow("roi",roi);
		if(waitKey(300) >= 0) break;*/
		cvReleaseImage(&plImg);		
	}	
}
void utils::get_face_images(vector<Mat>& images,bool addRotations)
{
	string filename ="vecfiles/facetrainLFW_01diff.vec";	
	vector<vec_entry> entries;
	get_vec_entries(filename,entries);
	int TOTAL_IMGS_THRESHOLD = 1301;
	for(int i=0;i<TOTAL_IMGS_THRESHOLD;i++)
	{
		vec_entry entry = entries[i];		
		Mat image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);		
		//cout<<"processing:"<<entry.filename<<endl;
		if(!image.empty())
		{
			images.push_back(image);
			if(addRotations)		
				add_rotations(image,images);		
		}
	}
}
/*
		IplImage image = images[i];
		
		imshow("images",images[i]);		
		
		IplImage* rotate45 = utils::rotateImage(&image,-45);
		Mat rotatedImage = rotate45;
		imshow("45",rotatedImage);
		IplImage* rotate90 = utils::rotateImage(&image,-90);
		rotatedImage = rotate90;
		imshow("90",rotatedImage);
		//cvShowImage("90",rotate90);
		rotate45 = utils::rotateImage(&image,-135);
		cvShowImage("145",rotate45);
		
		//cvShowImage("images",utils::rotateImage(&image,90));
		cvReleaseImage(&rotate90);
		cvReleaseImage(&rotate45);*/
