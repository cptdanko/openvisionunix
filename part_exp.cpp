#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"
#include "ExpHelper.h"
#include "test.h"

/*
#define NUM 100
#define DIMS 100
#define DIST 0.5

float64_t* lab;
float64_t* feat;

void gen_rand_data()
{
	lab=SG_MALLOC(float64_t, NUM);
	feat=SG_MALLOC(float64_t, NUM*DIMS);

	for (int32_t i=0; i<NUM; i++)
	{
		if (i<NUM/2)
		{
			lab[i]=-1.0;

			for (int32_t j=0; j<DIMS; j++)
				feat[i*DIMS+j]=CMath::random(0.0,1.0)+DIST;
		}
		else
		{
			lab[i]=1.0;

			for (int32_t j=0; j<DIMS; j++)
				feat[i*DIMS+j]=CMath::random(0.0,1.0)-DIST;
		}
	}
	CMath::display_vector(lab,NUM);
	CMath::display_matrix(feat,DIMS, NUM);
}

void test_cross()
{
	init_shogun();
	const int32_t feature_cache=0;
	const int32_t kernel_cache=0;
	const float64_t rbf_width=10;
	const float64_t svm_C=10;
	const float64_t svm_eps=0.001;	
	gen_rand_data();
	//SG_MALLOC(float64_t, featureMat.rows);
	//float64_t* feat = SG_MALLOC(float64_t, featureMat.rows*featureMat.cols);	
	cout<<"features converted"<<endl;
	CLabels* labels=new CLabels(SGVector<float64_t>(lab, NUM));
	SG_REF(labels);
	
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);	
	SG_REF(features);
	
	features->set_feature_matrix(feat, DIMS, NUM);	
	CGaussianKernel* kernel = new CGaussianKernel(kernel_cache, rbf_width);
	SG_REF(kernel);
	kernel->init(features, features);

	
	CLibSVM* svm = new CLibSVM(svm_C, kernel, labels);
	SG_REF(svm);
	svm->set_epsilon(svm_eps);	
	svm->train();

	int32_t num_subsets =20;
	
	CStratifiedCrossValidationSplitting* splitting_strategy = 
		new CStratifiedCrossValidationSplitting(labels, num_subsets);
	
	CContingencyTableEvaluation* evaluation_criteria = 
		new CContingencyTableEvaluation(ACCURACY);
	
	CCrossValidation* cross = new CCrossValidation(svm, features, labels, 
											splitting_strategy, evaluation_criteria);
											
	CrossValidationResult results = cross->evaluate();
	cout<<results.mean<<endl;
	cout<<results.conf_int_low<<endl;
	cout<<results.conf_int_up<<endl;
	cout<<results.conf_int_alpha<<endl;
	exit_shogun();
}*/
void get_random_rects(int limit,int xLimit,int yLimit,vector<node>& nodes)
{
	int width=80;int height=80;
	while(nodes.size() < (unsigned) limit)
	{
		Rect rect;
		rect.x = (rand()%xLimit);//width
		rect.y = (rand()%yLimit);//height
		if((rect.x+width)< xLimit && (rect.y+height)< yLimit) 
		{
			rect.width = width;
			rect.height = height;
			node nde;
			nde.rect = rect;			
			nde.groupId = 0;		
			nodes.push_back(nde);
		}
	}
}
void labelling()
{
	int DIST_THRESHOLD = 130;
	int TOTAL_RECTS = 50;	
	
	//before anything lets sort the array
	Mat image = imread("frame_203.jpg");
	Mat clone = image.clone();
	Mat newClone = image.clone();
	//vector<Rect> rects;
	vector<node> nodes;
	get_random_rects(TOTAL_RECTS,image.cols,image.rows,nodes);
	
	for(unsigned int i=0;i < nodes.size();i++)
	{
		//circle(image,find_centroid(nodes[i].rect),2,CV_RGB(255,0,0));
		rectangle(image,nodes[i].rect,CV_RGB(0,255,0),3);
	}	
	imshow("clones baby",clone);
	//find_connected_boxes(rects,clone);	
	map<int,vector<Rect>> groups;
	//exp_controller::find_groups(nodes,clone,groups,DIST_THRESHOLD);
	//drawLines(nodes,DIST_THRESHOLD,clone);
	
	imshow("upped",image);	
	
	imshow("clones baby",clone);
	//imshow("clones baby 2",newClone);
	waitKey(100000000);
}
void test_crossval()
{
	
	init_shogun();
	vector<Mat> posSamples, negSamples;
	utils::get_LMDa_torso_crops(posSamples);
	utils::get_nicta_neg_peds(negSamples);		
	features det2(64,64,64,64);	classify cls;
	
	feature_class_pair hogData = det2.get_hog_data_for_imgs(posSamples,negSamples);	
	cls.do_shogun_crossVal(hogData.hog_features,hogData.hog_classes);
	/*string svmname = "classifiers/hogfaceSVM32.txt";
	string posVec = "vecfiles/smallFacePos.vec";	
	string negVec = "vecfiles/smallNeg.vec";
	//string posPrefix= "trainimages/imm_handDB/all/images/";
	string posPrefix="trainimages/lfwcrop_face/faces/";
	string negPrefix = "trainimages/nictaNegPeds/";
	
	
	classify cls;
	

	data = det2.extract_from_vec(posVec,negVec,psPrefix,negPrefix);
	
	cls.do_shogun_crossVal(data.hog_features,data.hog_classes);*/
	
	exit_shogun();
}

void test_rand_no()
{
	for(int i=0;i<100;i++)
	{
		//int no = utils::gen_natural_rand_no(10);
		int no = utils::rand_number(1,20);
		//int n0 = rand()%10;		
		
		cout<<utils::rand_number(0.25,20)<<",";		
	}
	cout<<endl;
}
void bootstrap()
{
	init_shogun();
	bootstrap_params facetrainparams;
	facetrainparams.posvec = "vecfiles/facetrainLFW_01same.vec";//arguments[2];
	facetrainparams.negvec = "vecfiles/negNICTAFullpath.vec";//arguments[3];
	facetrainparams.iterations = 5;
	facetrainparams.testvecfile = "vecfiles/facetrainLFW_01diff.vec";
	//facetrainparams.testvecfile = "vecfiles/facetrainLFW_01same.vec";
	facetrainparams.algorithmType = exp_controller::TYPE_HOG;
	facetrainparams.trainimgwidth = 320;
	facetrainparams.trainimgheight = 240;
	facetrainparams.samplewidth = 32;
	facetrainparams.sampleheight = 32;
	facetrainparams.randwidth = 32;
	facetrainparams.randheight = 32;
	facetrainparams.savefilename = "bootstrapTrain.txt";
	
	exp_controller controller(0);
	
	controller.bootstrap_train(facetrainparams);
	
	exit_shogun();
}
void tud_train_fullbody()
{
	training_params fullbodyparams;
	fullbodyparams.posvec = "vecfiles/TUDuprightpeople.vec";	
	fullbodyparams.negvec = "vecfiles/negfacesNICTApath.vec";
	fullbodyparams.savefilename = "classifiers/tudFullbodysvm.txt";
	fullbodyparams.trainimgwidth = 320;
	fullbodyparams.trainimgheight = 240;
	fullbodyparams.samplewidth = 32;
	fullbodyparams.sampleheight = 32;
	fullbodyparams.randwidth = 32;
	fullbodyparams.randheight = 32;
	fullbodyparams.algorithmType = exp_controller::TYPE_HOG;		
	exp_controller::train_svm_fullbody(fullbodyparams);	
}

void train_LAMDa_body()
{
	training_params bodyParams;
	bodyParams.samplewidth = 64;
	bodyParams.sampleheight = 64;
	bodyParams.randwidth = 64;
	bodyParams.randheight = 64;
	bodyParams.algorithmType = exp_controller::TYPE_HOG;
	bodyParams.savefilename= "classifiers/hogLAMDa64WithRotations.txt";
	exp_controller controller(0);	
	controller.train_LMDa_upper_body(bodyParams);
}
void train()
{
	training_params facetrainparams;
	training_params fullbodyparams;
	training_params handparams;
	training_params torsoparams;
	
	facetrainparams.posvec = "vecfiles/lfwfacecroptrain.txt";//arguments[2];
	facetrainparams.negvec = "vecfiles/negFacesNicta.vec";//arguments[3];
	facetrainparams.posprefix = "trainimages/lfwcrop_face/faces/";//arguments[4];
	facetrainparams.negprefix = "trainimages/nictaNegPeds/";	
	
	facetrainparams.savefilename ="classifiers/hogfaceSVM32.txt";
	facetrainparams.algorithmType = exp_controller::TYPE_HOG;
	facetrainparams.trainimgwidth = 320;
	facetrainparams.trainimgheight = 240;
	facetrainparams.samplewidth = 32;
	facetrainparams.sampleheight = 32;
	facetrainparams.randwidth = 32;
	facetrainparams.randheight = 32;
	facetrainparams.imgextensiontoadd=".pgm";
	
	exp_controller::train_svm(facetrainparams);
	
	
}
void do_image_extraction(char** argv)
{
	utils::write_images(argv[2],argv[3]);
}

void test_with_LAMDa(char** argv)
{
	cout<<"Doing the LAMDA test!!!!!"<<endl;
	testing_params params;
	params.classifiername = argv[2];
	params.videoname = argv[3];
	params.imageoutputLoc = argv[4];
	string typeArg = argv[5];
	int type = atoi(typeArg.c_str());
	params.testtype = type;
	/* perhaps these values below could be parametrised too */
	params.samplewidth = 64;
	params.sampleheight = 64;	
	exp_controller controller(0.23);	
	controller.test_svm(params);
}
void test(char** argv)
{
	testing_params params;
	params.classifiername = argv[2];
	params.videoname = argv[3];
	params.imageoutputLoc = argv[4];
	string typeArg = argv[5];
	int type = atoi(typeArg.c_str());
	params.testtype = type;
	/* perhaps these values below could be parametrised too */
	params.samplewidth = 32;
	params.sampleheight = 32;
	exp_controller controller(0);	
	controller.test_svm(params);
}
void test_ensemble(char** argv)
{
	ensemble_tst_params params;
	params.faceclassifiername = "classifiers/face32withRotations.txt";  //argv[2];
	params.torsoclassifiername  = "classifiers/hogLAMDa64WithRotations.txt";//= argv[3];
	params.handclassifiername =""; // argv[4];
	params.imageoutputLoc = "expdata/partstest/";//argv[5];
	string typeArg = "1"; //argv[6];	
	int type = atoi(typeArg.c_str());
	params.testtype = type;
	params.samplewidth = 64;
	params.sampleheight = 64;
	params.FACE_DET_WIDTH = 32;
	params.FACE_DET_HEIGHT = 32;

	params.detectionLogName = "faceTorsoLAMDAresult.txt";
	exp_controller controller(0.2);	
	controller.test_ensemble(params);
}
void train_face_svm()
{
	training_params bodyParams;
	bodyParams.samplewidth = 32;
	bodyParams.sampleheight = 32;
	bodyParams.randwidth = 32;
	bodyParams.randheight = 32;
	bodyParams.algorithmType = exp_controller::TYPE_HOG;
	bodyParams.savefilename= "classifiers/face32withRotations.txt";
	exp_controller controller(0);	
	controller.train_face_svm(bodyParams);
}

int main(int argc,char** argv)
{
	//test_markings("testvideos/LAMDa/torso_annotation.txt");
	//test_image_function();
	//test_markings("testvideos/LAMDa/limb_annotation.txt");
	//unit_test::test_vec_process();
	//test(argv);
	//bootstrap();
	//tud_train_fullbody();
	//train_LAMDa_body();
	//merge_images();
	string type = argv[1];
	if(type.compare("train") == 0)
	{
		train();
	}
	else if(type.compare("test") == 0)
	{
		test(argv);	
	}
	else if(type.compare("imgextract") == 0)
	{
		do_image_extraction(argv);
	}
	else if(type.compare("testLAMDa")==0)
	{
		test_with_LAMDa(argv);
	}
	else if(type.compare("trainLAMDa")==0)
	{
		train_LAMDa_body();
	}
	else if(type.compare("testEnsemble")==0)
	{
		test_ensemble(argv);
	}
	else if(type.compare("faceROT")==0)
	{
		train_face_svm();
	}
	else if(type.compare("crossval")==0)
	{
		test_crossval();
	}
	else{
		cout<<"unknown command"<<endl;
	}

	return 0;
}
/*
 * 	/*string posvec = "vecfiles/TUDuprightpeople.vec";
	string negvec = "vecfiles/nictaNegFile.vec";
	string tudsvmname ="classifiers/tudFullbodysvm.txt";
	//exp_controller::train_svm_fullbody(tudsvmname,posvec,negvec,exp_controller::TYPE_HOG);	
utils::write_images("testvideos/MULTI_2/t_2.MOV","testvideos/MULTI_2/320/");*/
	//test_rand_no();
	//test_crossval();
	/*Mat image = imread("Mike_Brey_0001.pgm");
	affine("Mike_Brey_0001.pgm");
	IplImage img = image;
	* 
	rotateImage(&img,90.0);*/
	//utils::rotateImage(&img,90.0);
	/*features f(64,64,64,64);
	classify cls;
	string pos_vec = "vecfiles/testPos.vec";
	string neg_vec = "vecfiles/testNeg.vec";
	feature_class_pair data = f.get_hog_data(pos_vec, neg_vec, 2, -1,10);
	cls.do_shogun_crossVal(data.hog_features,data.hog_classes);*/

