Welcome to CVHelper

This is work in progress and given that i am building this while doing 
my research, it is not as organized as i would like it to be at the moment.

Due to time constraints i am basically just doing things in the quickest possible way
to get the results for my research, which would enable me to publish. However the 
segragation of parts do talk about my initial ideas of how i wish to modularize code.

Components: classes

features: This should contain all the code for feature extraction and 
feature matrices related manipulation.

classify: This is where all the code related to the classifier training or 
the classification process should be.

utils(this should really be something else): at the moment this houses 
the utility functions that wouldnt logically fit anywhere else in the project.

ExpHelper: The Experiment Helper class is actually the controller i.e. this should
delegate the requests from the main class to other areas of the application. 
Actually this should eventually be renamed to BaseExpController or something.

objMarker.cpp: This is a standalone component which has the image labelling 
functionality.

Main: Currently part_exp file contains the main class, the only thing that 
this class should really do is accept the parameters and pass it to the 
ExpHelper class which should do the rest of the tasks.

Outstanding tasks:
1) HOG features extracted using the CPU implementation
2) Asynchronous feature extraction for each feature type
3) Code refactoring
4) Cleanup of he methods which are no longer used
5) Add the build initial history for the classifiers in ExpHelper class
6) Fix the memory mangement for Shogun
7) Remove all the print statements

