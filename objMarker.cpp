/*
Author:Bhuman Soni
Email:bhuman.soni@gmail.com
Bitbucket profile: cptDanko
*/
#include "vision.h"
#include "utils.h"

IplImage* image;
IplImage* image2;
int start_roi;
int roi_x0;
int roi_y0;
int roi_x1;
int roi_y1;
int width=80;
int height=80;
int numOfRec;   

CvPoint previousPt;
string window_name;

typedef struct{
        int num_of_recs;
        string markerInfo;
        int x; int y;
        int width; int height;
} marked_object;

typedef struct{
	
	string name;	
	int x; int y;
    int width; int height;
    
}body_part;

typedef struct{
	string imagename;
	vector<body_part> parts;	
}activemarker;

string IntToString(int num)
{
        ostringstream myStream; //creates an ostringstream object
        myStream << num << flush;
        /*
        * outputs the number into the string stream and then flushes
        * the buffer (makes sure the output is put into the stream)
        */
        return(myStream.str()); //returns the string form of the stringstream object
};

void on_mouse(int event,int x,int y,int flag,void* param)
{
    if(event==CV_EVENT_LBUTTONDOWN)
    {
		start_roi=1;
		roi_x0=x;
		roi_y0=y;
		
    }  
    /* the flag key of 33 works when the button it marks the pressed state of the mouse btn */
    if(event==CV_EVENT_MOUSEMOVE && flag==33)
    {
		
		roi_x1=x;
		roi_y1=y;
		//redraw ROI selection		
		image2=cvCloneImage(image);
		cvRectangle(image2,cvPoint(roi_x0,roi_y0),cvPoint(roi_x1,roi_y1),CV_RGB(255,0,255),1);
		cvShowImage(window_name.c_str(),image2);
		cvReleaseImage(&image2);
    }
    if(event==CV_EVENT_LBUTTONUP)
    {
		start_roi=0;
    }
};
void move_box(int new_x,int new_y)
{
        image2 = cvCloneImage(image);   
        cvRectangle(image2,cvPoint(new_x,new_y),cvPoint(new_x+width,new_y+height),CV_RGB(255,0,255),1);
        cvShowImage(window_name.c_str(),image2);        
        cvReleaseImage(&image2);
}
void on_mouse_click(int event,int x,int y,int flag,void* param)
{
        int new_x,new_y;
        if(event == CV_EVENT_LBUTTONDOWN)
        {
                start_roi = 1;
                roi_x0 =x;
                roi_y0=y;
        }//CV_EVENT_FLAG_ALTKEY ,CV_EVENT_FLAG_CTRLKEY CV_EVENT_FLAG_SHIFTKEY, cv_event_
        if(event==CV_EVENT_LBUTTONUP)
        {
                roi_x0=x;
                roi_y0=y;
                image2 = cvCloneImage(image);   
                cvRectangle(image2,cvPoint(x,y),cvPoint(x+width,y+height),CV_RGB(255,0,255),1);
                cvShowImage(window_name.c_str(),image2);                
                cvReleaseImage(&image2);
                previousPt.x = x;
                previousPt.y = y;
        }
        if(event==CV_EVENT_FLAG_CTRLKEY)
        {       
                new_x = previousPt.x;
                new_y = previousPt.y;
                new_x -= 10;
                move_box(new_x,new_y);
        }
}
void mark_static_images(string dir_name,string extension,string vec_filename,int USE_VECFILE)
{  
        int iKey=0;
        int marked_obj_count = 0;
        image = 0;
        image2 = 0;      
        deque<marked_object> objects;
		vector<string> filesindir;
		string vecfilename ="testset.vec";

		if(USE_VECFILE==1){			
			utils::get_body_part_vecs(vecfilename,filesindir);		
			cout<<"about to use vecfile"<<endl;			
		}
		else{			
			utils::get_files_in_dir(dir_name,filesindir);
			cout<<"about to dir file vecfile"<<endl;
		}
		
		cout<<"files total::"<<filesindir.size()<<endl;
		ofstream output(vec_filename.c_str());
		for(int i=0;i<filesindir.size();i++)
		{
			activemarker marker;
			string filename;
			if(USE_VECFILE!=1)
			{
				cvAddSearchPath(dir_name.c_str());				
				filename = dir_name.c_str();
			}	
			
			filename.append(filesindir[i]);
			marker.imagename = filename;
			
			window_name = "Marker Window";
			cvNamedWindow(window_name.c_str(),1);
			//cvSetMouseCallback(window_name.c_str(),on_mouse_click,NULL);
			cvSetMouseCallback(window_name.c_str(),on_mouse,NULL);
			
			numOfRec = 0;
			string markerInfo;
			
			cout << filename << endl;
			image = cvLoadImage(filename.c_str(),1);
			//int current_x,current_y,current_width,current_height;
			do{
				cvShowImage(window_name.c_str(),image);
				iKey = cvWaitKey(0);
				//cout<<"now looping"<<endl;
				//cout <<"key pressed:"<<iKey<<endl;
				                      
				switch(iKey)
				{
						std::cout <<"key pressed:"<<iKey<<endl;					
						case 1048695: //we are doing the face, head etc -> W	
						{						
							cout<<"face"<<endl;
							body_part face;
							face.name = "face";
							face.x = roi_x0;
							face.y = roi_y0;
							face.width = (roi_x1-roi_x0);
							face.height = (roi_y1-roi_y0);
							marker.parts.push_back(face);
							break;
						}
						
						case 1048676://we are doing the leg -> D
						{
							cout<<"leg"<<endl;	
							body_part leg;
							leg.name = "leg";
							leg.x = roi_x0;
							leg.y = roi_y0;
							leg.width = (roi_x1-roi_x0);
							leg.height = (roi_y1-roi_y0);
							marker.parts.push_back(leg);
							break;
						}
						
						case 1048691: //we are doing the torso -> S
						{
							cout<<"torso"<<endl;							
							body_part torso;
							torso.name = "torso";
							torso.x = roi_x0;
							torso.y = roi_y0;
							torso.width = (roi_x1-roi_x0);
							torso.height = (roi_y1-roi_y0);
							marker.parts.push_back(torso);
							break;
						}
						
						case 1048673: //we are doing the hand -> A					
						{
							cout<<"hand"<<endl;
							body_part hand;
							hand.name = "hand";
							hand.x = roi_x0;
							hand.y = roi_y0;
							hand.width = (roi_x1-roi_x0);
							hand.height = (roi_y1-roi_y0);
							marker.parts.push_back(hand);
							break;
						}
							
						case 1048586:
								cvReleaseImage(&image);
								cvDestroyWindow(window_name.c_str());

						case 1048694://1048694-> V
							numOfRec++;
							//cout << " x:"<<roi_x0 << " y:" << roi_y0<< " width:" << width << " height:"<<height<<endl;
							/*markerInfo.append(" "+IntToString(roi_x0)+" "+IntToString(roi_y0)+" "+IntToString(width)+" "+IntToString(height));
							current_x = roi_x0;
							current_y = roi_y0;
							current_width = width;
							current_height = height; */
							break;
						case 1048679:
							cout<<"we are trying to use the previously marked object, feature not implemented yet!"<<endl;
							/*marked_object object = objects.front();
							numOfRec = object.num_of_recs;
							markerInfo = object.markerInfo;
							cout << filename<<" "<<numOfRec <<markerInfo<< endl;
							current_x = object.x;
							current_y = object.y;
							current_width = object.width;
							current_height = object.height;*/
							break;
				}               
			}while(iKey!=1048677);//b->1048674; g->1048679; 1048586->enter
			
			if(/*iKey==1048674||*/iKey==1048677)
			{
				cout<<"About to write the marked stuff out to file::"<<endl;
				
				cout<<"size of the part collections:"<<marker.parts.size()<<endl;
				for(unsigned int i=0;i<marker.parts.size();i++)
				{
					body_part part = marker.parts[i];
					
					output << marker.imagename<<" "<< part.name<<" " << IntToString(part.x)<<" "<<IntToString(part.y)<<" "<<IntToString(part.width)<<" "<<IntToString(part.height)<<endl;
					
				}
				
				/*output << filename <<" "<< numOfRec << markerInfo<<endl;                                
				
				marked_object marker;                           
				marker.num_of_recs = numOfRec;
				marker.markerInfo = markerInfo;
				marker.x = current_x;
				marker.y = current_y;
				marker.width = current_width;
				marker.height = current_height;
				objects.push_front(marker);*/
			}			
		}
		cvDestroyWindow(window_name.c_str());
		cvReleaseImage(&image);						
		output.close();
}
int main(int argc,char** argv)
{
	cout<<"Marker baby:"<<endl;	
	if(argv[1]==NULL)
	{
		cout<<"Specify the dir name!!!!!!!!!!!"<<endl;
		return 0;
	}
	string dir = argv[1];
	string extension = ".jpg";
	if(argv[2] == NULL)
	{
		cout<<"Specify the vec filename!!!!!!!!!!!!!!!!"<<endl;
		return 0;
	}
	
	string vecfilename = argv[2];
	//mark_images(dir,vecfilename);
	string fileType = argv[3];	
	int fType = atoi(fileType.c_str());
	mark_static_images(dir,extension,vecfilename,fType);
}
