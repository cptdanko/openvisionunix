#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"


#define POS 1
#define NEG -1
#define EXP_HOG_ONLY = 0
#define EXP_SURF_ONLY = 1
#define EXP_BOTH_TYPES = 2

typedef struct{
	double accuracy;
	int vote;
}df_outcome;

ofstream log_str("resultlog.txt");

SVM aSurfSvm;
SVM aHogSvm;
CvKNearest aSurfKnn,aHogKnn;
CvBoost aHogBoost,aSurfBoost;

int KTH=24;
string posVec,negVec;

CvSVM* hogSvmPtr;CvSVM* surfSvmPtr;
CvBoost* hogBoostPtr; CvBoost* surfBoostPtr;

float sKnnTreshold = 0.7;	
float sBoostTreshold = 2;
float sSvmTreshold = 0.3;

deque<int> sSvmDetHist,sKnnDetHist,sBoostDetHist;
deque<int> hSvmDetHist,hKnnDetHist,hBoostDetHist;

int HISTORY_TH = 20;

mutex m;

int updateTreshold = 50;

CvKNearest aSurfKnn,surfKnnCopy,aHogKnn,hogKnnCopy;

bool knnRetrainingActive = false;
bool knnTrainingDone = false;

feature_class_pair knnActive,knnPassive;

feature_class_pair global_training_data;

void updateKnn(bool isHTrain,bool isSTrain)
{
	m.lock();
	knnRetrainingActive = true;
	m.unlock();		
	if(isHTrain)
	{
		aHogKnn.train(knnActive.hog_features,knnActive.hog_classes,Mat(),true,KTH,32,true);
	}
	if(isSTrain)
	{
		aSurfKnn.train(knnActive.surf_features,knnActive.surf_classes,Mat(),true,KTH,32,true);
	}
	lock_guard<std::mutex> lk(m);
	knnTrainingDone = true;	
}
void train_boost(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"Boosting training"<<endl;	
	CvBoostParams params(CvBoost::REAL,75,0.95,2,false,0);
	cout<< "***********************Boosting training started***********************"<<endl;	
	if(isHTrain){
		Mat varType(1,data.hog_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.hog_features.cols) = CV_VAR_CATEGORICAL;
		aHogBoost.train(data.hog_features,CV_ROW_SAMPLE,data.hog_classes,Mat(),Mat(),varType,Mat(),params,false);		
	}
	if(isSTrain){
		Mat varType(1,data.surf_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.surf_features.cols) = CV_VAR_CATEGORICAL;
		aSurfBoost.train(data.surf_features,CV_ROW_SAMPLE,data.surf_classes,Mat(),Mat(),Mat(),Mat(),params,false);
	}
	cout<< "***********************Boosting training finished***********************"<<endl;
}

void train_svm(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"in the classifier"<<endl;
	SVM svm;
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.degree = 0.5; params.gamma = 1;
	params.coef0 =1;params.C = 10;
	params.nu = 0;params.p = 0;
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER,1000,0.01);
	cout<<isHTrain<<endl;
	cout<<isSTrain<<endl;
	cout<< "***********************SVM training started***********************"<<endl;	
	if(isHTrain){
		aHogSvm.train(data.hog_features,data.hog_classes,Mat(),Mat(),params);
		//aHogSvm.save("classifiers/hogsvm.xml");		
	}
	if(isSTrain){

		aSurfSvm.train(data.surf_features,data.surf_classes,Mat(),Mat(),params);
		//aSurfSvm.save("classifiers/surfsvm.xml");
	}
	cout<< "***********************SVM training finished***********************"<<endl;
}
void train_knn(feature_class_pair& data,bool isHTrain,bool isSTrain)
{	
	cout<< "***********************KNN training started***********************"<<endl;
	if(isHTrain){
		aHogKnn.train(data.hog_features,data.hog_classes,Mat(),true,KTH);
	}
	if(isSTrain){
		aSurfKnn.train(data.surf_features,data.surf_classes,Mat(),true,KTH);
	}
	cout<< "***********************KNN training finished***********************"<<endl;
}
void record_classifier_history(int pCount,int nCount,bool useHog,bool useSurf
							  ,int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs)
{
	if(pCount>nCount)
	{
		if(useHog)
		{
			if(hgSvmRs>0){hSvmDetHist.push_back(1);} else {hSvmDetHist.push_back(-1);}
			if(hKnnRs>0){hKnnDetHist.push_back(1);} else {hKnnDetHist.push_back(-1);}
			if(hBoostRs>0){hBoostDetHist.push_back(1);} else {hBoostDetHist.push_back(-1);}
			if(hSvmDetHist.size()>=HISTORY_TH)
			{
				hSvmDetHist.pop_front();
				hKnnDetHist.pop_front();
				hBoostDetHist.pop_front();
			}
		}
		if(useSurf)
		{
			if(sSvmRs>0){sSvmDetHist.push_back(1);} else {sSvmDetHist.push_back(-1);}
			if(sKnnRs>0){sKnnDetHist.push_back(1);} else {sKnnDetHist.push_back(-1);}
			if(sBoostRs>0){sBoostDetHist.push_back(1);} else {sBoostDetHist.push_back(-1);}
			
			if(sSvmDetHist.size()>=HISTORY_TH)
			{
				sSvmDetHist.pop_front();
				sKnnDetHist.pop_front();
				sBoostDetHist.pop_front();
			}
		}
	}
	else
	{
		if(useHog)
		{
			if(hgSvmRs<0){hSvmDetHist.push_back(1);} else {hSvmDetHist.push_back(-1);}
			if(hKnnRs<0){hKnnDetHist.push_back(1);} else {hKnnDetHist.push_back(-1);}
			if(hBoostRs<0){hBoostDetHist.push_back(1);} else {hBoostDetHist.push_back(-1);}
			if(hSvmDetHist.size()>=HISTORY_TH)
			{
				hSvmDetHist.pop_front();
				hKnnDetHist.pop_front();
				hBoostDetHist.pop_front();
			}
		}
		if(useSurf)
		{
			if(sSvmRs<0){sSvmDetHist.push_back(1);} else {sSvmDetHist.push_back(-1);}
			if(sKnnRs<0){sKnnDetHist.push_back(1);} else {sKnnDetHist.push_back(-1);}
			if(sBoostRs<0){sBoostDetHist.push_back(1);} else {sBoostDetHist.push_back(-1);}
			
			if(sSvmDetHist.size()>=HISTORY_TH)
			{
				sSvmDetHist.pop_front();
				sKnnDetHist.pop_front();
				sBoostDetHist.pop_front();
			}
		}
	}
}

df_outcome find_max(double svm, double knn, double boost,int svmVote,int knnVote,int boostVote)
{
	df_outcome outcome;
	if(svm>knn)
	{
		if(svm > boost){	
			outcome.accuracy = svm;
			outcome.vote = svmVote;
		}
		else{	
			outcome.accuracy = boost;
			outcome.vote = boostVote;
		}
	}
	else
	{
		if(knn>boost){	
			outcome.accuracy = knn;
			outcome.vote = knnVote;
		}
		else {	
			outcome.accuracy = boost;
			outcome.vote = boostVote;			
		}
	}
	return outcome;
}

int apply_decision_function(int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs,
							  bool useHog,bool useSurf)
{
	df_outcome hogWinner,surfWinner;
	if(useHog)
	{
		//hSvmDetHist,hKnnDetHist,hBoostDetHist;
		double svmAccuracy = utils::find_historic_accuracy(hSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(hKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(hBoostDetHist);
		cout<<"accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		if( svmAccuracy == knnAccuracy&& knnAccuracy==boostAccuracy)
		{
			//they are all the same so return the majority vote
		}
		else //apply the decision function
		{
			hogWinner= find_max(svmAccuracy,knnAccuracy,boostAccuracy,hgSvmRs,hKnnRs,hBoostRs);
		}
	}	
	if(useSurf)
	{
		double svmAccuracy = utils::find_historic_accuracy(sSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(sKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(sBoostDetHist);
		cout<<"accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		if( svmAccuracy == knnAccuracy&& knnAccuracy==boostAccuracy)
		{
			//they are all the same so return the majority vote
		}
		else //apply the decision function
		{
			surfWinner =  find_max(svmAccuracy,knnAccuracy,boostAccuracy,sSvmRs,sKnnRs,sBoostRs);
		}
	}
	int outcome;
	if(useHog&& useSurf)
	{
		if(hogWinner.accuracy > surfWinner.accuracy)
			outcome= hogWinner.vote;
		else
			outcome= surfWinner.vote;

	}

}
/*
 * classifier to use
 * 0-> just use HOG
 * 1-> just use SURF
 * 2-> use both of the above
 **/
int operate_slidewins(Mat frame,vector<Rect> wins,string imgOutLoc,
			string frameCount,ofstream& csvlog,features& detector,
			classify& predictor,int classifiers_to_use,Mat origFrame)
{	
	
	int winsDetected = 0;	
	for(unsigned int winCount=0; winCount< wins.size();winCount++)
	{
		Mat desc,classes;
		Rect roi = wins[winCount];
		Mat image(frame,roi);
		float hgSvmRs=0;float hKnnRs=0;float hBoostRs=0; 
		float sSvmRs=0;float sKnnRs=0;float sBoostRs=0;
		float pCount =0;float nCount = 0;
	
		bool useHog = false;
		bool useSurf = false;
		if(classifiers_to_use==0 ||classifiers_to_use==2){useHog = true;}
		if(classifiers_to_use==1 ||classifiers_to_use==2){useSurf = true;}				
		
		if(useHog)//only HOG
		{
			detector.compute_hog_features(image,1,desc,classes);
			hgSvmRs = predictor.predict_svm(hogSvmPtr,desc,0);
			hKnnRs = predictor.predict_knn(aHogKnn,desc,sKnnTreshold);			
			hBoostRs = predictor.predict_boost(hogBoostPtr,desc,sBoostTreshold);
			
			if(hgSvmRs>0){	pCount++;	} else {	nCount++;	}
			if(hKnnRs>0){	pCount++;	} else {	nCount++;	}
			if(hBoostRs>0){	pCount++;	} else {	nCount++;	}					
		}
		if(useSurf)//only SURF
		{
			detector.extract_keypoints(image,1,desc,classes);
			int tempBoostThreshold = 0.9;
			sSvmRs = predictor.predict_svm(surfSvmPtr,desc,0);
			sBoostRs = predictor.predict_boost(surfBoostPtr,desc,tempBoostThreshold);
			sKnnRs = predictor.predict_knn(aSurfKnn,desc,sKnnTreshold);
						
			if(sSvmRs>0){pCount++;} else {nCount++;}
			if(sKnnRs>0){pCount++;} else {nCount++;}
			if(sBoostRs>0){pCount++;} else {nCount++;}				
		}		
		int outcome;	//apply the decision function here			
		record_classifier_history(pCount,nCount,useHog,useSurf,hgSvmRs,hKnnRs,hBoostRs,sSvmRs,sKnnRs,sBoostRs);		
		cout<<"detection history size:"<<hKnnDetHist.size()<<endl;
		cout<<"detection history size:"<<hSvmDetHist.size()<<endl;		
		
		if(pCount>nCount)
		{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			cout<<detectionScoresOutput.str()<<endl;
			winsDetected++;
			rectangle(origFrame,roi,CV_RGB(255,0,0));
			ostringstream str;
			str <<imgOutLoc<<frameCount<<"_"<<winCount<<".jpg"<<endl;						
			
			for(int i=0;i<nCount;i++){
				cout<<"re-training"<<endl;
			}	
		}
		else{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			//cout<<detectionScoresOutput.str()<<endl;
		}
	}
	return winsDetected;
}
/**
	Params expected:
	param 1:experiment no
	Param 2:the name of the video
	Param 3:type of features it uses i.e. hog or surf-pass the res as well
	Param 4:the pos vec
	Param 5:the neg vec
	Param 6:the directory where the detected imgs would be stored
	Param 7:the location of the test video images
	param 8:the output filename
	param 9 & 10:the win width and height respectively
	param 11:the detector classifier combination to use 0-hog, 1-surf, 2-both
*/
void exp3(char** argv)
{
	string expNo = argv[1];
	string vidName = argv[2];
	string featuresUsed = argv[3];
	string pVec = argv[4];
	string nVec = argv[5];
	string imgOutputLoc = argv[6];
	string testimgLoc = argv[7];
	string finalLogName = argv[8];
	string swidth =argv[9];
	string sheight =argv[10];
	cout<<"we are here"<<endl;
	int winwidth = atoi(swidth.c_str());
	int winheight = atoi(sheight.c_str());
	string ext=".jpg";
	string sExpcombo = argv[11];
	int expComboType = atoi(sExpcombo.c_str());
	
	ofstream rLog(finalLogName.c_str());
	string frameLogName =imgOutputLoc+finalLogName+"_frame.txt";
	ofstream frameLog(frameLogName.c_str());
	
	int posAsPos = 0;
	int negAsPos = 0;
	int posAsNeg = 0;
	int negAsNeg = 0;
	
	classify predictor;
	features detector(winwidth,winheight);
	
	global_training_data = detector.get_limited_data(pVec.c_str(), nVec.c_str(),7,100,100);
	
	bool trainForHog=false;bool trainforSurf=false;

	//get  the values from param 2 onwwards
	if((expComboType==0)||(expComboType==2))
	{ 
		trainForHog = true;
		hogSvmPtr = new CvSVM;
		hogBoostPtr = new CvBoost;
		hogSvmPtr->load("classifiers/hogsvm.xml");
		hogBoostPtr->load("classifiers/hogBoost.xml");
	}
	
	if(expComboType==1||expComboType==2)
	{
		trainforSurf =true;
		surfBoostPtr = new CvBoost;
		surfSvmPtr = new CvSVM;
		surfSvmPtr->load("classifiers/surfsvm.xml");
		surfBoostPtr->load("classifiers/surfBoost.xml");
	}	
	/*train_svm(data,trainForHog,trainforSurf);
	 *train_boost(data,trainForHog,trainforSurf);
	 */
	train_knn(data,trainForHog,trainforSurf);
		
	string posdir = testimgLoc;
	posdir+="pos";
	cout<<"posdir:"<<posdir<<endl;
	string negdir  =testimgLoc;
	negdir+="neg";
	cout<<"negdir:"<<negdir<<endl;
	vector<string> posfiles;
	utils::get_files_in_dir(posdir,posfiles);
	vector<string> negfiles;
	utils::get_files_in_dir(negdir,negfiles);
	frameLog<<"expNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"imgName"<<","<<"Type"<<",totalWins"<<","<<"posAsPos"<<","<<"posAsNeg,"<<"negAsPos,"<<"negAsNeg"<<endl;
	for(unsigned int filecount=0;filecount<posfiles.size();filecount++)
	{	

		Mat newFrame = imread(posdir+"/"+posfiles[filecount]);
		//resize(newFrame,newFrame,Size(160,120));
		Mat frame;	
		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		string imgName = posfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
				
		cout<<"detecting on image:"<<imgName.c_str()<<endl;
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		ofstream csvLog(tempLogName.c_str());
		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame);
		if(result>0)
			posAsPos++;
		else
			posAsNeg++;
		
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"POS,"<<wins.size()<<","<<posAsPos<<","<<posAsNeg<<",0,0"<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();		
	}
	for(unsigned int filecount=0;filecount<negfiles.size();filecount++)
	{
		Mat newFrame = imread(negdir+"/"+negfiles[filecount]);
		//resize(newFrame,newFrame,Size(160,120));
		Mat frame;		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		
		string imgName = negfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
		
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		cout<<"detecting on image:"<<imgName.c_str()<<endl;
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		ofstream csvLog(tempLogName.c_str());		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame);
		
		if(result>0)
			negAsPos++;
		else
			negAsNeg++;
		
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"NEG,"<<wins.size()<<",0,0,"<<negAsPos<<","<<negAsNeg<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();		
	}
	//the first three things should uniquely identify the exp record
	//then there should be frame count
	rLog<<"ExpNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"FilesTotal"<<","<<"posAsPos"<<","<<"posAsNeg"<<","<<"negAsPos"<<","<<"negAsNeg"<<endl;
	rLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<(posfiles.size()+negfiles.size())<<","<<posAsPos<<","<<posAsNeg<<","<<negAsPos<<","<<negAsNeg<<endl;
	frameLog.close();
	rLog.close();
	delete surfSvmPtr;
	delete surfBoostPtr;
	delete hogSvmPtr;
	delete hogBoostPtr;
	
}
void train_classifiers()
{
	classify predictor;
	features detector(120,80);
	
	//feature_class_pair data = detector.get_train_data_from_vec("pos_320x240.vec","neg_320x240.vec",7,2);
	feature_class_pair data = detector.get_limited_data("pos_320x240.vec","neg_320x240.vec",7);
	train_svm(data,true,true);
	train_boost(data,true,true);
	
	aSurfBoost.save("classifiers/surfLTDboost.xml");
	aSurfSvm.save("classifiers/surfLTDsvm.xml");
	aHogSvm.save("classifiers/hogLTDsvm.xml");
	aHogBoost.save("classifiers/hogLTDboost.xml");
	
}
void print_v(vector<Point> p)
{
	for(int i=0;i<p.size();i++)
	{
		cout<<"("<<p[i].x<<":"<<p[i].y<<"),";
	}
	cout<<endl;
}
void labelling()
{
	int T = 60;
	int width = 80;int height=80;
	Point p1(109,94);
	Point p2(138,102);
	Point p3(192,115);
	Point p4(38,148);
	vector<Point> points;
	points.push_back(p1);points.push_back(p2);
	points.push_back(p3);points.push_back(p4);
	map<Point,map<Point,int>>values;
	int top=0;int bottom = 0;
	print_v(points);
	//before anything lets sort the array
	
	utils::bubble_sort(points);
	cout<<"Now that we have sorted the array:"<<endl;	
	
	//map<int,int>foundPoints;

	bool connectMap[4][4];//4x4 2d array
	int skipElems = 0;
	for(int i=0;i<points.size();i++)
	{
		
		Point fatherP = points[i];
		cout<<"doing:x,"<<fatherP.x<<":with:"<<points[i+1].x<<endl;
		if(utils::calculate_dist(fatherP,points[i+1])<T)//if this is true then the 2 points are connected
		{
			cout<<"about to set things to true"<<endl;
			connectMap[i][i+1] = true;
		}
		else{ connectMap[i][i+1] = false; }
		cout<<utils::calculate_dist(fatherP,points[i+1])<<endl;
		//at this point we should check how many more elements are we skipping
	}	
	cout <<connectMap[0][1]<<endl;
	cout <<connectMap[1][2]<<endl;
	cout <<connectMap[2][3]<<endl;
	cout <<connectMap[3][4]<<endl;
	/*for(int i=0;i<4;i++)
	{	
		for(int j=0;j<4;j++)		
		{
			cout<<connectMap[i][j]<<",";
		}
		cout<<endl;
	}*/
}
void do_hog_patch()
{
	features detector(32,32);
	Mat i1,i2,d1,d2;
	Mat id1,id2,dd1,dd2;
	Mat classes;
	i1 = imread("p_defect_1.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	i2 = imread("p_defect_2.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	d1 = imread("p_normal_1.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	d2 = imread("p_normal_2.bmp",CV_LOAD_IMAGE_GRAYSCALE);
	detector.compute_hog_features(i1,1,id1,classes);
	cout<<id1.rows<<endl;
	detector.compute_hog_features(i2,1,id2,classes);
	
	detector.compute_hog_features(d1,1,dd1,classes);
	detector.compute_hog_features(d2,1,dd2,classes);
	
	ofstream stream("posfeatures.txt");
	cout<<"rows:"<<id1.cols<<endl;
	
	for(int i=0;i<id1.rows;i++)
	{	
		for(int j=0;j<id1.cols;j++)		
		{
			stream << id1.at<float>(i,j)<<",";
		}
		stream<<endl;
	}
	for(int i=0;i<id2.rows;i++)
	{	
		for(int j=0;j<id2.cols;j++)		
		{
			stream << id2.at<float>(i,j)<<",";
		}
		stream<<endl;
	}
	stream.close();
	ofstream nstream("negFeatures.txt");
	cout<<"rows:"<<dd1.rows<<endl;
	
	for(int i=0;i<dd1.rows;i++)
	{	
		for(int j=0;j<dd1.cols;j++)		
		{
			nstream << dd1.at<float>(i,j)<<",";
		}
		nstream<<endl;
	}
	for(int i=0;i<dd2.rows;i++)
	{	
		for(int j=0;j<dd2.cols;j++)		
		{
			nstream << dd2.at<float>(i,j)<<",";
		}
		nstream<<endl;
	}
	nstream.close();
}
float calc_bayes(double p1,double p2, double h)
{
	cout<<"p1:"<<p1<<endl;
	double a1 = (p1*h);
	cout<<"a1:"<<a1<<endl;
	double a2 = ((p1*h)+(p2*h));
	cout<<"a2:"<<a2<<endl;
	
	a1/=a2;
	double num = (p1*h)/((p1*h)+(p2*h));
	cout<<"final prob is:"<<a1<<endl;
	//cout<<"final prob is:"<<a2<<endl;
	return num;
}
int main(int argc,char** argv)
{
	string firstArg = argv[1]; 
	if(firstArg=="help")
	{
		utils::help();
		return 0;
	}
	experiment1(argv);
	if(!utils::validate_params(argv))
	{
		return 0;
	}
	//run_feature_tests(argv);

	cout<<"**************************************files saved**************************************"<<endl;
}
