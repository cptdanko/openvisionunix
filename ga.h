#include "vision.h"

typedef struct{
	double fitness;
	int kernel;
	double softmargin;
	double gamma;
	chromosome(int k, double c, double g=10):
		kernel(k),softmargin(c),gamma(g);
}svm_chromosome;

/*class chromosome{
	
	public:
		int FITNESS_VALUE
		
};
class svm_chromsome: chromsome{
	
}
/**
 * for the chromosome remember the kernel value mappings
 *0 -> RBF
 *1 -> SIGMOID
 *2 -> POLY
 **/
class GA{	
	
	public:		
		int GENERATIONS;
		int ELITISM_COUNT;
		int FITNESS_VALUE;
		int POPULATION_SIZE;
		GA(int generations =5, int elitism = 10, int pop_size = 20 int fitValue):
			GENERATIONS(generations),
			ELITISM_COUNT(elitism),
			POPULATION_SIZE(pop_size),
			FITNESS_VALUE(fitValue) {};		
		
		svm_chromosome gen_svm_chromosome();
		svm_chromosome* gen_population();
		
			
			
};
