#include "vision.h"

class classify{
	
	
public:
	
	typedef struct{
		double accuracy;
		double error;
		double recall;
		double precision;
		double wracc;
		double f1;
		
		void print()
		{
			ostringstream str;
			cout<<"accuracy:"<<accuracy<<" ,error:"<<error<<" ,recall"<<recall<<" ,precision:"<<precision<<" ,wracc:"<<wracc<<" ,f1:"<<f1<<endl;			
		}
	}kval_results;
	
	int32_t CROSSVAL_FOLDS;
	classify(int crossValFolds=10):CROSSVAL_FOLDS(crossValFolds){};
	classify(int svmTresh,int knnTresh);
	//classify(){};
	SVM* train_svm(Mat& features, Mat& classes);
	CvDTree train_dtree(Mat& features, Mat& classes);
	CvNormalBayesClassifier train_bayes(Mat& features, Mat& classes);
	CvKNearest train_knn(Mat& features, Mat& classes);
	
	float predict_knn(CvKNearest& classifier,Mat& feature,float treshold);	
	float predict_boost(CvBoost& classifier,Mat& feature,float treshold);
	//and now the pointers
	float predict_boost(CvBoost* classifier,Mat& feature,float treshold);
	float predict_svm(SVM* svm,Mat& feature,float treshold);
	
	CLibSVM* trainShogunSVM(Mat& features,Mat& classes,string svmSaveFileName);
	
	double predict_shogun_svm(Mat& featureMat,CLibSVM* svm);
	double predict_sh_svm_surf(Mat& featureMat,CLibSVM* svm);
	void do_shogun_crossVal(Mat& featureMat,Mat& classesMat);
	void trainAndSaveShogunSVM(Mat& featureMat,Mat& classesMat,string svmSaveFileName);
};
