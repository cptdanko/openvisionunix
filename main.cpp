#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"
#include "ExpHelper.h"


#define POS 1
#define NEG -1
#define EXP_HOG_ONLY = 0
#define EXP_SURF_ONLY = 1
#define EXP_BOTH_TYPES = 2

typedef struct{
	double accuracy;
	int vote;
}df_outcome;

ofstream log_str("resultlog.txt");

SVM aSurfSvm;
SVM aHogSvm;
CvKNearest aSurfKnn,aHogKnn;
CvBoost aHogBoost,aSurfBoost;

int KTH=24;
string posVec,negVec;

CvSVM* hogSvmPtr;CvSVM* surfSvmPtr;
CvBoost* hogBoostPtr; CvBoost* surfBoostPtr;

float sKnnTreshold = 0.7;	
float sBoostTreshold = 2;
float sSvmTreshold = 0.3;

deque<int> sSvmDetHist,sKnnDetHist,sBoostDetHist;
deque<int> hSvmDetHist,hKnnDetHist,hBoostDetHist;

int HISTORY_TH = 20;

void populate_50_50_history()
{
        for(int i=0;i<10;i++)
        {
                 sSvmDetHist.push_back(1);
                 sKnnDetHist.push_back(1);
                 sBoostDetHist.push_back(1);
                 hSvmDetHist.push_back(1);
                 hKnnDetHist.push_back(1);
                 hBoostDetHist.push_back(1);
        }
        for(int i=0;i<10;i++)
        {
                 sSvmDetHist.push_back(-1);
                 sKnnDetHist.push_back(-1);
                 sBoostDetHist.push_back(-1);
                 hSvmDetHist.push_back(-1);
                 hKnnDetHist.push_back(-1);
                 hBoostDetHist.push_back(-1);
        }
}
void train_boost(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"Boosting training"<<endl;
	
	CvBoostParams params(CvBoost::REAL,75,0.95,2,false,0);
	//Mat upper = features.rowRange(features.rows)
	cout<< "***********************Boosting training started***********************"<<endl;	
	if(isHTrain)
	{
		Mat varType(1,data.hog_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.hog_features.cols) = CV_VAR_CATEGORICAL;

		cout<<"rows to use for boosting:"<<data.hog_features.rows<<endl;
		aHogBoost.train(data.hog_features,CV_ROW_SAMPLE,data.hog_classes,Mat(),Mat(),varType,Mat(),params,false);		
		cout<<"boost training finished"<<endl;
		aHogBoost.save("classifiers/hogBoost.xml");
	}
	if(isSTrain){
		cout<<"boosting stuff"<<endl;
		Mat varType(1,data.surf_features.cols+1,CV_8UC1,Scalar(CV_VAR_ORDERED));
		varType.at<uchar>( data.surf_features.cols) = CV_VAR_CATEGORICAL;
		aSurfBoost.train(data.surf_features,CV_ROW_SAMPLE,data.surf_classes,Mat(),Mat(),Mat(),Mat(),params,false);
		aSurfBoost.save("classifiers/surfBoost.xml");
		//iSurfBoost.trainc(data.surf_features,CV_ROW_SAMPLE,data.hog_classes,Mat(),Mat(),Mat(),Mat(),params,false);
	}
	cout<< "***********************Boosting training finished***********************"<<endl;	
	
}

void train_svm(feature_class_pair& data,bool isHTrain,bool isSTrain)
{
	cout<<"in the classifier"<<endl;
	SVM svm;
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.degree = 0.5;
	params.gamma = 1;
	params.coef0 =1;
	params.C = 10;
	params.nu = 0;
	params.p = 0;
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER,1000,0.01);
	cout<<isHTrain<<endl;
	cout<<isSTrain<<endl;
	cout<< "***********************SVM training started***********************"<<endl;	
	if(isHTrain){
		cout<<"training hog"<<endl;
		aHogSvm.train(data.hog_features,data.hog_classes,Mat(),Mat(),params);
		aHogSvm.save("classifiers/hogsvm.xml");
		
	}
	if(isSTrain){
		cout<<"a surf training"<<endl;
		aSurfSvm.train(data.surf_features,data.surf_classes,Mat(),Mat(),params);
		aSurfSvm.save("classifiers/surfsvm.xml");
	}
	cout<< "***********************SVM training finished***********************"<<endl;

	//detector.compute_hog_features(&image,1,h_desc,h_class);
}
void train_knn(feature_class_pair& data,bool isHTrain,bool isSTrain)
{	
	cout<< "***********************KNN training started***********************"<<endl;
	if(isHTrain){
		aHogKnn.train(data.hog_features,data.hog_classes,Mat(),true,KTH);
		//iHogKnn.train(data.hog_features,data.hog_classes,Mat(),false,KTH);
	}
	if(isSTrain){
		//iSurfKnn.train(data.surf_features,data.surf_classes,Mat(),false,KTH);
		aSurfKnn.train(data.surf_features,data.surf_classes,Mat(),true,KTH);
	}
	cout<< "***********************KNN training finished***********************"<<endl;
}
void record_classifier_history(int pCount,int nCount,bool useHog,bool useSurf
							  ,int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs)
{
	if(pCount>nCount)
	{
		if(useHog){			
			if(hgSvmRs>0){	utils::update_cls_hist(hSvmDetHist,-1,1);}
			else {	utils::update_cls_hist(hSvmDetHist,1,-1);	}
			
			if(hKnnRs>0){	utils::update_cls_hist(hKnnDetHist,-1,1); } 
			else {	utils::update_cls_hist(hKnnDetHist,1,-1);	}
			
			if(hBoostRs>0){	utils::update_cls_hist(hBoostDetHist,-1,1);	} 
			else {	utils::update_cls_hist(hBoostDetHist,1,-1);	}
		}
		if(useSurf){
			
			if(sSvmRs>0){utils::update_cls_hist(sSvmDetHist,-1,1);} 
			else {utils::update_cls_hist(sSvmDetHist,1,-1);}
			if(sKnnRs>0){utils::update_cls_hist(sKnnDetHist,-1,1);} 
			else {utils::update_cls_hist(sKnnDetHist,1,-1);}
			if(sBoostRs>0){utils::update_cls_hist(sBoostDetHist,-1,1);} 
			else {utils::update_cls_hist(sBoostDetHist,1,-1);}
		}
	}
	else{
		if(useHog)
		{			
			if(hgSvmRs<0){ utils::update_cls_hist(hSvmDetHist,-1,1);} 
			else {	utils::update_cls_hist(hSvmDetHist,1,-1); }
			
			if(hKnnRs<0){	utils::update_cls_hist(hKnnDetHist,-1,1);	} 
			else {utils::update_cls_hist(hKnnDetHist,1,-1);}
			
			if(hBoostRs<0){	utils::update_cls_hist(hBoostDetHist,-1,1); } 
			else { utils::update_cls_hist(hBoostDetHist,1,-1);}			
		}
		if(useSurf)
		{
			if(sSvmRs<0){utils::update_cls_hist(sSvmDetHist,-1,1);} 
			else {utils::update_cls_hist(sSvmDetHist,1,-1);}
			
			if(sKnnRs<0){utils::update_cls_hist(sKnnDetHist,-1,1);} 
			else {utils::update_cls_hist(sKnnDetHist,1,-1);}
			
			if(sBoostRs<0){utils::update_cls_hist(sBoostDetHist,-1,1);} 
			else {utils::update_cls_hist(sBoostDetHist,1,-1);}
		}
	}
}

df_outcome find_max(double svm, double knn, double boost,int svmVote,int knnVote,int boostVote)
{
	df_outcome outcome;
	if(svm>knn){
		if(svm > boost){	
			outcome.accuracy = svm;
			outcome.vote = svmVote;
		}
		else{	
			outcome.accuracy = boost;
			outcome.vote = boostVote;
		}
	}
	else{
		
		if(knn>boost){	
			outcome.accuracy = knn;
			outcome.vote = knnVote;
		}
		else {	
			outcome.accuracy = boost;
			outcome.vote = boostVote;			
		}
	}
	//if they are all the same then we should return the accuracy of the majority
	if(svm==knn && knn==boost)
	{
		int majority = svmVote+knnVote+boostVote;
		if(svmVote ==majority){ outcome.accuracy = svm;	outcome.vote = svmVote; }
		else if(knnVote ==majority){ outcome.accuracy = knn;outcome.vote = knnVote; }
		else{ outcome.accuracy = boost; outcome.vote = boostVote; }
	}
	return outcome;
}

int apply_decision_function(int hgSvmRs,int hKnnRs,int hBoostRs,
							  int sSvmRs,int sKnnRs,int sBoostRs,
							  bool useHog,bool useSurf)
{
	df_outcome hogWinner,surfWinner;
	int outcome =0;
	if(useHog){
		
		double svmAccuracy = utils::find_historic_accuracy(hSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(hKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(hBoostDetHist);
		cout<<"hog accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		hogWinner= find_max(svmAccuracy,knnAccuracy,boostAccuracy,hgSvmRs,hKnnRs,hBoostRs);
		cout<<"hog"<<hogWinner.accuracy<<":and vote:"<<hogWinner.vote<<endl;
		outcome = hogWinner.vote;
	}	
	if(useSurf){
		
		double svmAccuracy = utils::find_historic_accuracy(sSvmDetHist);
		double knnAccuracy = utils::find_historic_accuracy(sKnnDetHist);
		double boostAccuracy = utils::find_historic_accuracy(sBoostDetHist);
		cout<<"accuracies baby:"<<svmAccuracy<<":"<<knnAccuracy<<":"<<boostAccuracy<<endl;
		surfWinner =  find_max(svmAccuracy,knnAccuracy,boostAccuracy,sSvmRs,sKnnRs,sBoostRs);		
		outcome = surfWinner.vote;
	}
	
	if(useHog&& useSurf)
	{
		if(hogWinner.accuracy > surfWinner.accuracy)
			outcome= hogWinner.vote;
		else
			outcome= surfWinner.vote;

	}
	return outcome;
}
/*
 * classifier to use
 * 0-> just use HOG
 * 1-> just use SURF
 * 2-> use both of the above
 **/
int operate_slidewins(Mat frame,vector<Rect> wins,string imgOutLoc,
			string frameCount,ofstream& csvlog,features& detector,
			classify& predictor,int classifiers_to_use,Mat origFrame,bool useDecisionRule)
{	
	
	int winsDetected = 0;
	if(classifiers_to_use!=2)
	{
		csvlog<<"svmVote,KnnVote,BoostVote,PosDets,NegDets"<<endl;
	}
	else{
		csvlog<<"HogSVMVote,HogKnnVote,HogBoostVote,SurfSVMVote,SurfKnnVote,SurfBoostVote,PosDets,NegDets"<<endl;
	}	
	for(unsigned int winCount=0; winCount< wins.size();winCount++)
	{
		
		Rect roi = wins[winCount];
		Mat image(frame,roi);
		/*int hgSvmRs=0;int hKnnRs=0;int hBoostRs=0; 
		int sSvmRs=0;int sKnnRs=0;int sBoostRs=0;
		int pCount =0;int nCount = 0;*/
		float hgSvmRs=0;float hKnnRs=0;float hBoostRs=0; 
		float sSvmRs=0;float sKnnRs=0;float sBoostRs=0;
		float pCount =0;float nCount = 0;
	
		bool useHog = false;
		bool useSurf = false;
		if(classifiers_to_use==0 ||classifiers_to_use==2){useHog = true;}
		if(classifiers_to_use==1 ||classifiers_to_use==2){useSurf = true;}				
		
		bool skipDR = false;
		
		/*********************pending*****************************************
		 * Need to specify the decision rule logic for the big bad ensemble of 
		 * 6 classifiers.
		 * ********************************************************************
		 */		
		 Mat s_desc,s_classes, h_desc,h_classes;
		if(useHog)//only HOG
		{
			detector.compute_hog_features(image,1,h_desc,h_classes);
			//hgSvmRs = predictor.predict_svm(desc,aHogSvm,0);
			hgSvmRs = predictor.predict_svm(hogSvmPtr,h_desc,0);
			hKnnRs = predictor.predict_knn(aHogKnn,h_desc,sKnnTreshold);			
			hBoostRs = predictor.predict_boost(hogBoostPtr,h_desc,sBoostTreshold);
			
			/*********to be changed********************/
			
			if(hgSvmRs>0){pCount++;} else {nCount++;}
			if(hKnnRs>0){pCount++;} else {nCount++;}
			if(hBoostRs>0){pCount++;} else {nCount++;}	
			
			if(pCount ==3 || nCount ==3){	skipDR = true;	}
			cout<<hgSvmRs<<","<<hKnnRs<<","<<hBoostRs<<","<<pCount<<","<<nCount<<endl;
		}
		if(useSurf)//only SURF
		{
			detector.extract_keypoints(image,1,s_desc,s_classes);

			int tempBoostThreshold = 1.5;
			sSvmRs = predictor.predict_svm(surfSvmPtr,s_desc,0);
			sBoostRs = predictor.predict_boost(surfBoostPtr,s_desc,tempBoostThreshold);
			sKnnRs = predictor.predict_knn(aSurfKnn,s_desc,sKnnTreshold);
						
			if(sSvmRs>0){pCount++;} else {nCount++;}
			if(sKnnRs>0){pCount++;} else {nCount++;}
			if(sBoostRs>0){pCount++;} else {nCount++;}	
			
			if(pCount ==3 || nCount ==3){	skipDR = true;	}	
		}
		cout<<useDecisionRule<<":"<<skipDR<<endl;
	
		if(useDecisionRule && !skipDR)
		{
			/*to change from the decision function to a simple majority vote, just comment out the line below
			 */
			cout<<"about to apply the decision function";
			int outcome = 0;
			outcome = apply_decision_function(hgSvmRs,hKnnRs,hBoostRs,
								  sSvmRs,sKnnRs,sBoostRs,
								  useHog,useSurf);
			cout<<"outcome:"<<outcome<<endl;
			pCount = 0;
			nCount = 0;
			if(outcome == 1){pCount++;}else{nCount++;}
			cout<<pCount<<nCount<<endl;
		}
		record_classifier_history(pCount,nCount,useHog,useSurf,hgSvmRs,hKnnRs,hBoostRs,sSvmRs,sKnnRs,sBoostRs);
		
		ostringstream detWinProps;
		detWinProps<<"win("<<roi.x<<"X"<<roi.y<<")";
		csvlog<<detWinProps.str().c_str()<<",";
		if(classifiers_to_use==0)
		{
			csvlog<<hgSvmRs<<","<<hKnnRs<<","<<hBoostRs<<","<<pCount<<","<<nCount<<endl;			
		}
		if(classifiers_to_use==1)
		{			
			csvlog<<sSvmRs<<","<<sKnnRs<<","<<sBoostRs<<","<<pCount<<","<<nCount<<endl;
		}
		if(classifiers_to_use==2)
		{
			csvlog<<hgSvmRs<<","<<hKnnRs<<","<<hBoostRs<<","<<sSvmRs<<","<<sKnnRs<<","<<sBoostRs<<","<<pCount<<","<<nCount<<endl;
		}
		
		if(pCount>nCount)
		{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			cout<<detectionScoresOutput.str()<<endl;
			winsDetected++;
			rectangle(origFrame,roi,CV_RGB(255,0,0));
			ostringstream str;
			str <<imgOutLoc<<frameCount<<"_"<<winCount<<".jpg"<<endl;						
			for(int i=0;i<nCount;i++){
				cout<<"re-training"<<endl;
			}	
		}
		else{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			//cout<<detectionScoresOutput.str()<<endl;
		}
	}
	return winsDetected;
}

void build_initial_hist(feature_class_pair data,bool useSurf, bool useHog)
{
	classify predictor;	
	Mat features = data.hog_features;
	Mat classes = data.hog_classes;
	cout<<"About to build initial history:"<<features.rows<<endl;
	int T =(HISTORY_TH/2);
	if(useHog) {		
		for(int i=0;i<features.rows;i++)
		{		
			if(i==T)	{	break;	}
			int truth = classes.at<int>(i,0);		
			cout<<truth<<":that was the truth value"<<endl;
			int vote = 0;
			Mat feature = features.row(i);
			vote = predictor.predict_svm(hogSvmPtr,feature,0);
			
			if(vote!=truth){	hSvmDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aHogKnn,feature,sKnnTreshold);					
			if(vote!=truth){	hKnnDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(hogBoostPtr,feature,sBoostTreshold);
			if(vote!=truth){	hBoostDetHist.push_back(-1); }
			else {	hBoostDetHist.push_back(1); }
			
			int rowNo = features.rows-(i+1);
			cout <<rowNo<<endl;
			feature = features.row(rowNo);
			truth = classes.at<int>(rowNo,0);	
			cout<<truth<<":that was the second truth value"<<endl;
			vote = predictor.predict_svm(hogSvmPtr,feature,0);
			
			if(vote!=truth){	hSvmDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aHogKnn,feature,sKnnTreshold);					
			if(vote!=truth){	hKnnDetHist.push_back(-1); }
			else {	hSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(hogBoostPtr,feature,sBoostTreshold);
			if(vote!=truth){	hBoostDetHist.push_back(-1); }
			else {	hBoostDetHist.push_back(1); }

		}			
	}	
	features = data.surf_features;
	classes = data.surf_classes;
	cout<<"About to build initial history:"<<features.rows<<endl;
	if(useSurf)
	{
		for(int i=0;i<features.rows;i++)
		{			
			if(i==T)	{	break;	}
			Mat feature = features.row(i);
			int tempBoostThreshold = 1.5;			
			int truth = classes.at<int>(i,0);		
			int vote = 0;
			vote = predictor.predict_svm(surfSvmPtr,feature,0);
			if(vote!=truth){	sSvmDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aSurfKnn,feature,sKnnTreshold);
			if(vote!=truth){	sKnnDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(surfBoostPtr,feature,tempBoostThreshold);
			if(vote!=truth){	sBoostDetHist.push_back(-1); }
			else {	sBoostDetHist.push_back(1); }
			
			int rowNo = features.rows-(i+1);
			cout <<rowNo<<endl;
			feature = features.row(rowNo);
			truth = classes.at<int>(rowNo,0);	
			//cout<<truth<<":that was the second truth value"<<endl;
			
			
			truth = classes.at<int>(i,0);		
			
			vote = predictor.predict_svm(surfSvmPtr,feature,0);
			if(vote!=truth){	sSvmDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }
			
			vote = predictor.predict_knn(aSurfKnn,feature,sKnnTreshold);
			if(vote!=truth){	sKnnDetHist.push_back(-1); }
			else {	sSvmDetHist.push_back(1); }	
			
			vote = predictor.predict_boost(surfBoostPtr,feature,tempBoostThreshold);
			if(vote!=truth){	sBoostDetHist.push_back(-1); }
			else {	sBoostDetHist.push_back(1); }
		}		
	}
	
}
/**
	Params expected:
	param 1:experiment no
	Param 2:the name of the video
	Param 3:type of features it uses i.e. hog or surf-pass the res as well
	Param 4:the pos vec
	Param 5:the neg vec
	Param 6:the directory where the detected imgs would be stored
	Param 7:the location of the test video images
	param 8:the output filename
	param 9 & 10:the win width and height respectively
	param 11:the detector classifier combination to use 0-hog, 1-surf, 2-both
*/

void experiment1(char** argv)
{
	string expNo = argv[1];
	string vidName = argv[2];
	string featuresUsed = argv[3];
	string pVec = argv[4];
	string nVec = argv[5];
	string imgOutputLoc = argv[6];
	string testimgLoc = argv[7];
	string finalLogName = argv[8];
	string swidth =argv[9];
	string sheight =argv[10];
	
	
	cout<<"we are here"<<endl;
	int winwidth = atoi(swidth.c_str());
	int winheight = atoi(sheight.c_str());
	string ext=".jpg";
	string sExpcombo = argv[11];
	int expComboType = atoi(sExpcombo.c_str());
	string DR_USAGE = argv[12];
	int useDR = atoi(DR_USAGE.c_str());
	bool useDecisionRule = false;
	if(useDR==1)
	{
		useDecisionRule = true;
	}
		
	ofstream rLog(finalLogName.c_str());
	string frameLogName =imgOutputLoc+finalLogName+"_frame.txt";
	ofstream frameLog(frameLogName.c_str());
	
	int posAsPos = 0;
	int negAsPos = 0;
	int posAsNeg = 0;
	int negAsNeg = 0;
	
	classify predictor;
	features detector(winwidth,winheight);
	
	//feature_class_pair data = detector.prepare_train_data(pVec.c_str(), nVec.c_str(),7);
	
	feature_class_pair data = detector.get_train_data_from_vec(pVec.c_str(), nVec.c_str(),7,expComboType);
		
	bool trainForHog=false;bool trainforSurf=false;
	
	if((expComboType==0)||(expComboType==2))
	{ 
		trainForHog = true;
		hogSvmPtr = new CvSVM;
		hogBoostPtr = new CvBoost;
		hogSvmPtr->load("classifiers/hogsvm.xml");
		hogBoostPtr->load("classifiers/hogBoost.xml");
	}
	
	if(expComboType==1||expComboType==2)
	{
		trainforSurf =true;
		surfBoostPtr = new CvBoost;
		surfSvmPtr = new CvSVM;
		surfSvmPtr->load("classifiers/surfsvm.xml");
		surfBoostPtr->load("classifiers/surfBoost.xml");
	}
	
	/*train_svm(data,trainForHog,trainforSurf);
	 *train_boost(data,trainForHog,trainforSurf);
	 */
	train_knn(data,trainForHog,trainforSurf);
	
	//once we have the classifiers built, lets populate the intial history
	
	feature_class_pair dataForInitialHist = detector.get_limited_data(pVec.c_str(), nVec.c_str(),7,2,2);
			
	build_initial_hist(dataForInitialHist,trainforSurf,trainForHog);
	
	double svmAccuracy = utils::find_historic_accuracy(hSvmDetHist);
	double knnAccuracy = utils::find_historic_accuracy(hKnnDetHist);
	double boostAccuracy = utils::find_historic_accuracy(hBoostDetHist);
	cout<<svmAccuracy<<":knn:"<<knnAccuracy<<"::boost::"<<boostAccuracy<<endl;
	
	waitKey(10000000000000);
	
	string posdir = testimgLoc;
	posdir+="pos";
	cout<<"posdir:"<<posdir<<endl;
	string negdir  =testimgLoc;
	negdir+="neg";
	cout<<"negdir:"<<negdir<<endl;
	vector<string> posfiles;
	utils::get_files_in_dir(posdir,posfiles);
	vector<string> negfiles;
	utils::get_files_in_dir(negdir,negfiles);
	cout<<"the number of pos files:"<<posfiles.size()<<endl;
	cout<<"the number of neg files:"<<negfiles.size()<<endl;
	frameLog<<"expNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"imgName"<<","<<"Type"<<",totalWins"<<","<<"posAsPos"<<","<<"posAsNeg,"<<"negAsPos,"<<"negAsNeg"<<endl;
	for(unsigned int filecount=0;filecount<posfiles.size();filecount++)
	{	
		Mat newFrame = imread(posdir+"/"+posfiles[filecount]);	
		Mat frame;	
		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		string imgName = posfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
				
		cout<<"detecting on image:"<<imgName.c_str()<<endl;
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		ofstream csvLog(tempLogName.c_str());
		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame,useDecisionRule);
		if(result>0)
			posAsPos++;
		else
			posAsNeg++;
		
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"POS,"<<wins.size()<<","<<posAsPos<<","<<posAsNeg<<",0,0"<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();	
		//so heres the template, experiment name, name,filename,res,total wins, posWins	
	}
	for(unsigned int filecount=0;filecount<negfiles.size();filecount++)
	{
		Mat newFrame = imread(negdir+"/"+negfiles[filecount]);
		//resize(newFrame,newFrame,Size(160,120));
		Mat frame;		
		cvtColor(newFrame,frame,CV_RGB2GRAY);
		GaussianBlur(frame,frame,Size(3,3),1);
		
		string imgName = negfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
		
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		cout<<"detecting on image:"<<imgName.c_str()<<endl;
				
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		ofstream csvLog(tempLogName.c_str());		
		int result = operate_slidewins(frame,wins,imgOutputLoc,imgName,csvLog,detector,predictor,expComboType,newFrame,useDecisionRule);
		
		if(result>0)
			negAsPos++;
		else
			negAsNeg++;
					
		frameLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<imgName<<","<<"NEG,"<<wins.size()<<",0,0,"<<negAsPos<<","<<negAsNeg<<endl;
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),newFrame);
		csvLog.close();		
	}
	rLog<<"ExpNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"FilesTotal"<<","<<"posAsPos"<<","<<"posAsNeg"<<","<<"negAsPos"<<","<<"negAsNeg"<<endl;
	rLog<<expNo<<","<<vidName<<","<<featuresUsed<<","<<(posfiles.size()+negfiles.size())<<","<<posAsPos<<","<<posAsNeg<<","<<negAsPos<<","<<negAsNeg<<endl;
	frameLog.close();
	rLog.close();
	delete surfSvmPtr;
	delete surfBoostPtr;
	delete hogSvmPtr;
	delete hogBoostPtr;
	
}
void train_classifiers()
{
	classify predictor;
	features detector(120,80);
	
	//feature_class_pair data = detector.get_train_data_from_vec("pos_320x240.vec","neg_320x240.vec",7,2);
	feature_class_pair data = detector.get_limited_data("pos_320x240.vec","neg_320x240.vec",7,200,200);
	train_svm(data,true,true);
	train_boost(data,true,true);
	
	aSurfBoost.save("classifiers/surfLTDboost.xml");
	aSurfSvm.save("classifiers/surfLTDsvm.xml");
	aHogSvm.save("classifiers/hogLTDsvm.xml");
	aHogBoost.save("classifiers/hogLTDboost.xml");
	
}
void load_svm(char** argv)
{
	Mat image = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
	
	CvSVM* svmPtr = new CvSVM;
	CvBoost* boostPtr = new CvBoost;
	
	svmPtr->load("classifiers/surfsvm.xml");
	boostPtr->load("classifiers/surfBoost.xml");
	
	classify classifier;
	features detector(120,80);
	Mat desc,classes;
	detector.extract_keypoints(image,1,desc,classes);	
	float sans= classifier.predict_svm(svmPtr,desc,0);
	float bans = classifier.predict_boost(boostPtr, desc,0);
	//classifier.predict_boost(boostPtr,desc);
	cout<<"sans:"<<sans<<endl;
	cout<<"bans:"<<bans<<endl;
}

void print_v(vector<Point> p)
{
	for(unsigned int i=0;i<p.size();i++)
	{
		cout<<"("<<p[i].x<<":"<<p[i].y<<"),";
	}
	cout<<endl;
}
void labelling()
{
	int T = 100;
	int width = 80;int height=80;	
	Point p1(109,94); Point p2(138,102);Point p3(192,115); Point p4(38,148);
	
	Point p5(109+250,p1.y);Point p6(138+250,p1.y);Point p7(p3.x,p1.y+250);
	
	vector<Point> points;
	points.push_back(p1);points.push_back(p2);
	points.push_back(p3);points.push_back(p4);
	points.push_back(p5);points.push_back(p6);
	points.push_back(p7);	

	print_v(points);
	//before anything lets sort the array
	Mat image = imread("frame_203.jpg");
	Mat clone = image.clone();
	Mat newClone = image.clone();
	vector<Rect> rects;
	rects.push_back(Rect(p1.x,p1.y,80,80));rects.push_back(Rect(p2.x,p2.y,80,80));
	rects.push_back(Rect(p3.x,p3.y,80,80));rects.push_back(Rect(p4.x,p4.y,80,80));
	rects.push_back(Rect(p5.x,p5.y,80,80));rects.push_back(Rect(p6.x,p6.y,80,80));
	rects.push_back(Rect(p7.x,p7.y,80,80));
	
	//find_connected_boxes(rects,clone);	
	map<int,vector<Rect>> groups;	
	rectangle(image,Rect(p1.x,p1.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p2.x,p2.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p3.x,p3.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p4.x,p4.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p5.x,p5.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p6.x,p6.y,80,80),CV_RGB(0,255,0),3);
	rectangle(image,Rect(p7.x,p7.y,80,80),CV_RGB(0,255,0),3);

	imshow("upped",image);
	
	utils::bubble_sort(points);	
	print_v(points);	
	
	map<int,vector<Point>> array;
	int groupId = 1;
	vector<Point>* pts = new vector<Point>;	
	bool distanceMatch = false;
	for(unsigned int i=0;i<points.size();i++)
	{		
		Point parent = points[i];
		Point neighabor = points[i+1];
		/*cout<<"parent/Nbor:"<<parent.x<<":"<<parent.y<<":"<<neighabor.x<<":"<<neighabor.y<<endl;
		cout<<"dist:"<<utils::calculate_dist(parent,neighabor) <<endl;*/
		if(utils::calculate_dist(parent,neighabor) < T)
		{		
			distanceMatch = true;	
			//cout<<"about to push back.."<<endl;
			pts->push_back(parent);			
			if(i ==points.size()-2)
			{
				//cout<<"the last thing and i count:"<<i<<endl;
				pts->push_back(neighabor);
			}
		} 
		else //means it did not match
		{
			distanceMatch = false;			
			pts->push_back(parent);
		}
		//we  are still pending the last element entry
		if(!distanceMatch /*&& (pts->size() > 0)*/)
		{			
			array[groupId] = *pts;
			//cout<<"(first non-dist match)vector size:"<<pts->size()<<endl;
			
			groupId++;	
			delete pts;
			pts = new vector<Point>;
			//cout<<"results:"<<groupId<<endl;
		}
		if(i ==(points.size()-2) && distanceMatch)
		{
			//so coming in here the group would already be incremented		
			/*cout<<"(the second last element)i count:"<<i<<endl;
			cout<<"vector size:"<<pts->size()<<endl;*/
			pts->push_back(neighabor);
			array[groupId] = *pts;
			delete pts;			
		}
		distanceMatch = false;
		if(i == (points.size()-2)) { break; }
	}
	
	/*int counter = 1;
	do
	{
		vector<Point> pointArray = array[counter];
		int startX,startY,endX,endY;
		if(pointArray.size()==1)
		{
			//cout<<"we have a 1 size array">
			startX = pointArray[0].x;
			startY = pointArray[0].y;
			endX = startX+width;
			endY = startY+height;
		}
		else{
			startX = utils::get_min(pointArray,0);
			startY = utils::get_min(pointArray,1);
			endX = (utils:: get_max(pointArray,0))+width;
			endY = (utils:: get_max(pointArray,1))+height;
			cout<<"min max:"<<startX<<":"<<startY<<":"<<endX<<":"<<endY<<endl;
		}
		rectangle(newClone,Point(startX,startY),Point(endX,endY),CV_RGB(0,255,0),3);
		//rectangle(clone,Rect(startX,startY,width,height),CV_RGB(0,255,0),3);
		counter++;
	}while(counter < (array.size()+1));*/
	imshow("clones baby",clone);
	imshow("clones baby 2",newClone);
	waitKey(100000000);
}

void trainShogunSVM()
{
	init_shogun();
	features detector(32,32);
	classify cls;
	Mat classes,features;
	Mat image = imread("frame_203.jpg");
	detector.compute_hog_features(image,1,features,classes);
	cout<<"rows:"<<features.rows<<endl;
	Mat f1,c1;
	detector.compute_hog_features(image,-1,f1,c1);
	Mat f2,f3;
	f2.push_back(features.row(1));
	f2.push_back(f1.row(1));
	
	f3.push_back(c1.row(1));
	f3.push_back(classes.row(1));
	
	
	cls.trainShogunSVM(f2,f3);
	exit_shogun();
}
void testGradients()
{
	cout<<"Testing gradients"<<endl;
	Mat image = imread("frame_203.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	//cvSmooth(image,image);
	imshow("sample,",image);
	Mat dst;
	pyrDown(image,dst,Size(160,129));
	imshow("discretised",dst);
	pyrUp(image,dst,Size(500,429));
	//Laplacian(image,dst,CV_64F);
	//Laplacian(image,dst,CV_64F,2,2);
	imshow("upped",dst);
	waitKey(100000000);
	
}
int main(int argc,char** argv)
{
	//testGradients();
	labelling();
	//trainShogunSVM();
	string firstArg = argv[1]; 
	if(firstArg=="help")
	{
		utils::help();
		return 0;
	}
	experiment1(argv);
	if(!utils::validate_params(argv))
	{
		return 0;
	}
	//run_feature_tests(argv);

	cout<<"**************************************files saved**************************************"<<endl;
}

