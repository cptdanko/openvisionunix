#include "vision.h"

const int window_width = 640;
const int window_height = 480;
int x_1 = -window_width/2;
int x_2 = window_width*3/2;
int y_1 = -window_width/2;
int y_2 = window_width*3/2;

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>

using namespace cv;

/// Global variables

int threshold_value = 0;
int threshold_type = 3;;
int const max_value = 255;
int const max_type = 4;
int const max_BINARY_value = 255;

Mat src, src_gray, dst;
char* window_name = "Threshold Demo";

char* trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
char* trackbar_value = "Value";

/// Function headers
void Threshold_Demo( int, void* );

/**
 * @function main
 */
int threshold(char** argv )
{
  /// Load an image
  src = imread( argv[1], 1 );

  /// Convert the image to Gray
  cvtColor( src, src_gray, CV_RGB2GRAY );

  /// Create a window to display results
  namedWindow( window_name, CV_WINDOW_AUTOSIZE );

  /// Create Trackbar to choose type of Threshold
  createTrackbar( trackbar_type,
                  window_name, &threshold_type,
                  max_type, Threshold_Demo );

  createTrackbar( trackbar_value,
                  window_name, &threshold_value,
                  max_value, Threshold_Demo );

  /// Call the function to initialize
  Threshold_Demo( 0, 0 );

  /// Wait until user finishes program
  while(true)
  {
    int c;
    c = waitKey( 20 );
    if( (char)c == 27 )
      { break; }
   }

}
void Threshold_Demo( int, void* )
{
  /* 0: Binary
     1: Binary Inverted
     2: Threshold Truncated
     3: Threshold to Zero
     4: Threshold to Zero Inverted
   */

  threshold( src_gray, dst, threshold_value, max_BINARY_value,threshold_type );

  imshow( window_name, dst );
}

int Drawing_Random_Polylines( Mat image,RNG rng )
{
  int lineType = 8;

  for( int i = 0; i< 100; i++ )
  {
    Point pt[2][3];
    pt[0][0].x = rng.uniform(x_1, x_2);
    pt[0][0].y = rng.uniform(y_1, y_2);
    pt[0][1].x = rng.uniform(x_1, x_2);
    pt[0][1].y = rng.uniform(y_1, y_2);
    pt[0][2].x = rng.uniform(x_1, x_2);
    pt[0][2].y = rng.uniform(y_1, y_2);
    pt[1][0].x = rng.uniform(x_1, x_2);
    pt[1][0].y = rng.uniform(y_1, y_2);
    pt[1][1].x = rng.uniform(x_1, x_2);
    pt[1][1].y = rng.uniform(y_1, y_2);
    pt[1][2].x = rng.uniform(x_1, x_2);
    pt[1][2].y = rng.uniform(y_1, y_2);

    const Point* ppt[2] = {pt[0], pt[1]};
    int npt[] = {3, 3};

    polylines(image, ppt, npt, 2, true, Scalar(0,0,255), rng.uniform(1,10), lineType);

    imshow( "image", image );
    if( waitKey(10000000) >= 0 )
      { return -1; }
  }
  return 0;
}

void drawPolylines(Mat image)
{
	RNG rng( 0xFFFFFFFF );
	Drawing_Random_Polylines(image,rng);
	
}

void doCvWay(char** argv)
{
	
	IplImage* src, *res,*roi;
	
	src = cvLoadImage(argv[1],1);
	Mat matImage = imread(argv[1]);
	res = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,3);
	roi = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,CV_LOAD_IMAGE_GRAYSCALE);
	drawPolylines(matImage);
	cvZero(roi);
	
	cvCircle(roi,cvPoint(160,200),100,CV_RGB(255,255,255),-1,8,0);
	cvAnd(src,src,res,roi);
	cvShowImage("res",res);
	cvShowImage("image",roi);
	
	cvShowImage("src",src);
	
	cvNot(res,res);
	IplImage *roi_c3 = cvCreateImage(cvGetSize(src),8,3);
	
	cvMerge(roi,roi,roi, NULL, roi_c3);	
	cvShowImage("roic3",roi_c3);
	
	cvAnd(res,roi_c3,res,NULL);
	
	cvNot(roi,roi);
	cvAdd(src,res,res,roi);
	
	cvWaitKey(0);
	
	cvDestroyAllWindows();
	cvReleaseImage(&src);
	cvReleaseImage(&res);
	cvReleaseImage(&roi);
}
void blurimage(Mat& image)
{
	Mat blured;
	GaussianBlur(image,blured,Size(5,5),0,0,BORDER_DEFAULT);
	for(int i=0;i<10;i++)
	{	
		GaussianBlur(blured,blured,Size(3,3),0,0,BORDER_DEFAULT);
	}	
	imshow("bured image",blured);
	image = blured;
}
void randomStuff(int width, int height)
{
	Mat image = Mat::zeros(width,height,CV_8UC3);
	RNG rng( 0xFFFFFFFF);
	for(int i=0;i<100;i+=2)
	{
		cout<<rng.uniform(1,100)<<",";
	}
	cout<<endl;
	imshow("zeroes",image);
}
void test_cpp_surf(string imagename)
{
	Mat image = imread(imagename,CV_LOAD_IMAGE_GRAYSCALE);
	blurimage(image);
	SurfFeatureDetector detector(400,4,2,false,false);
	SurfDescriptorExtractor extractor(400,4,2,false,false);
	vector<KeyPoint> keypoints;
	Mat descriptors;
	detector.detect(image,keypoints);
	extractor.compute(image,keypoints,descriptors);
	cout<<"total keypoints:"<<keypoints.size()<<endl;
}
void test_cv_surf()
{
	const char* imagename = "frame_203.jpg";
	IplImage* image = cvLoadImage(imagename,CV_LOAD_IMAGE_GRAYSCALE);
	CvSURFParams params = cvSURFParams(400, 1);
	CvSeq* descriptors = 0;
	CvSeq* keypoints =0;
	CvMemStorage* storage = cvCreateMemStorage(0);
	cvExtractSURF(image,0,&keypoints,&descriptors,storage,params);
	
	printf("total keypoints extracted:%d",keypoints->total);
	//printf<<"total keypoints extracted:"<<keypoints->total<<endl;
}
int main(int argc,char** argv)
{
	randomStuff(640,480);
	
	Mat image;
	if( argc<2)
	{
		cout<<"Enter the file name"<<endl;
		return 0;
	}
	//test_cv_surf();
	test_cpp_surf(argv[1]);	
	image = imread(argv[1]);
	blurimage(image);
	threshold(argv);
	doCvWay(argv);
	
}
