#include "classifiers.h"

int K = 15;
SVM* classify::train_svm(Mat& features, Mat& classes)
{
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::RBF;
	params.degree = 0.5;
	params.gamma = 0.5;
	params.coef0 =1;
	params.C = 1;
	params.nu = 0.5;
	params.p = 0;
	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER,1000,0.01);
	cout<< "***********************SVM training started***********************"<<endl;
	SVM* svm = new SVM;
	svm->train(features,classes,Mat(),Mat(),params);
	cout<< "***********************SVM training finished***********************"<<endl;
	return svm;
}

CvKNearest classify::train_knn(Mat& features, Mat& classes)
{
	CvKNearest knn;
	cout<< "***********************KNN training started***********************"<<endl;
	knn.train(features,classes,Mat(),false,K);
	cout<< "***********************KNN training finished***********************"<<endl;
	return knn;
}

/*
At most this really should have only 1 value
	float predict_svm(SVM& classifier,Mat& feature);
	float predict_knn(CvKNearest& classifier,Mat& feature);
	float predict_dtree(CvDTree& classifier,Mat& feature);
*/
float classify::predict_knn(CvKNearest& classifier,Mat& feature,float treshold)
{
	float prediction;
	int posPrediction = 0;
	int negPrediction = 0;
	Mat nearests,noofsamples;
	Mat test;
	for(int i=0;i< feature.rows;i++){
		Mat sample = feature.row(i);
		prediction = classifier.find_nearest(sample,K,noofsamples,nearests,test);
		//cout<<"knn answer:"<<prediction<<endl;
		if(prediction > treshold)
		//if(prediction>0.6)
			posPrediction++;
		else
			negPrediction++;
	}
	int predictionTreshold = (60*feature.rows)/100;
	
	if(predictionTreshold > 1)//that means there is more than 1 row
	{
		if(posPrediction > predictionTreshold)
		{
			posPrediction = 1;
			negPrediction = -1;
		}
		/* when in doubt classify as negative */
		else 
		{
			posPrediction = -1;
			negPrediction = 1;
		}		
	}
	//cout<<"prediction treshold:"<<predictionTreshold<<endl;
	if(posPrediction > negPrediction)
		prediction = 1;
	else
		prediction = -1;
		
	return prediction;
}
float classify::predict_boost(CvBoost* classifier,Mat& feature,float treshold)
{
	float prediction;
	int posPrediction = 0;
	int negPrediction = 0;
	for(int i=0;i< feature.rows;i++){
		prediction = classifier->predict(feature.row(i),Mat(),Range::all(),false,true);
		//cout<<"boost predict(sum):"<<prediction<<endl;
		if(prediction > treshold)
			posPrediction++;
		else
			negPrediction++;
	}
	int predictionTreshold = (60*feature.rows)/100;
	//cout<<"prediction treshold:"<<predictionTreshold<<endl;
	if(predictionTreshold > 1)//that means there is more than 1 row
	{
		if(posPrediction > predictionTreshold)
		{
			posPrediction = 1;
			negPrediction = -1;
		}
		/* when in doubt classify as negative */
		else 
		{
			posPrediction = -1;
			negPrediction = 1;
		}		
	}
	
	if(posPrediction > negPrediction)
		prediction = 1;
	else
		prediction = -1;

	return prediction;
}
float classify::predict_svm(SVM* svm,Mat& feature,float treshold)
{
	float prediction;
	int posPrediction = 0;
	int negPrediction = 0;	
	for(int i=0;i< feature.rows;i++)
	{
		prediction = svm->predict(feature.row(i),false);
		cout<<"prediction before:"<<prediction;
		if(prediction > treshold)
			posPrediction++;
		else
			negPrediction++;			
	}
	int predictionTreshold = (60*feature.rows)/100;
	if(predictionTreshold > 1)//that means there is more than 1 row
	{
		if(posPrediction > predictionTreshold)
		{
			posPrediction = 1;
			negPrediction = -1;
		}
		/* when in doubt classify as negative */
		else 
		{
			posPrediction = -1;
			negPrediction = 1;
		}		
	}
	//cout<<"prediction treshold:"<<predictionTreshold<<endl;
	if(posPrediction > negPrediction)
		prediction = 1;
	else
		prediction = -1;
	cout<<",prediction after:"<<prediction<<endl;
	return prediction;
}
/**
 * The function to convert the opencv features to shogun features
 * the pointer arrays will come out changed
 */
void convert_local_features(Mat& features,Mat& classes,float64_t* feat, float64_t* lab)
{	

	cout<<"about to convert the, "<<features.rows<< " features..."<<endl;
	for (int32_t i=0; i<features.rows; i++)
	{	

		lab[i]=classes.at<int>(i,0);
		string value;		
		for (int32_t j=0; j<features.cols; j++)
		{
			feat[i*features.cols+j]=features.at<float>(i,j);	
			//cout<<"feature val:"<<feat[i*features.cols+j]<<",";
		}
		
	}
	/*CMath::display_vector(lab,features.rows);
	CMath::display_matrix(feat,features.cols,features.rows);*/
}
void convert_local_features(Mat& features,float64_t* feat)
{		
	for (int32_t i=0; i<features.rows; i++)
	{			
		for (int32_t j=0; j<features.cols; j++)
		{
			feat[i*features.cols+j]=features.at<float>(i,j);	
			//cout<<"feature val:"<<feat[i*features.cols+j]<<",";
		}
	}
	//cout<<" features: "<<features.rows<< " converted..."<<endl;
}
void classify::do_shogun_crossVal(Mat& featureMat,Mat& classesMat)
{
	const int32_t feature_cache=0;
	const int32_t kernel_cache=0;
	const float64_t rbf_width=10;
	const float64_t svm_C=10;
	const float64_t svm_eps=0.001;	
	//SG_MALLOC(float64_t, featureMat.rows);
	//float64_t* feat = SG_MALLOC(float64_t, featureMat.rows*featureMat.cols);
	cout<<"we are in here..."<<featureMat.cols<<endl;
	float64_t* lab =new float64_t[featureMat.rows];
	cout<<"prepared the labels"<<endl;
	int size=featureMat.rows*featureMat.cols;
	float64_t* feat;
	try{
	feat = new float64_t[size];
	}catch(ShogunException &sh)
	{
			cout<<sh.get_exception_string()<<endl;
	}		
	convert_local_features(featureMat,classesMat,feat,lab);	
	cout<<"features converted"<<endl;
	CLabels* labels = new CLabels(SGVector<float64_t>(lab, classesMat.rows));
	SG_REF(labels);
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);
	//cout<<"features converted"<<endl;
	SG_REF(features);
	features->set_feature_matrix(feat, featureMat.cols, featureMat.rows);
	//cout<<"3"<<endl;
	CGaussianKernel* kernel = new CGaussianKernel(kernel_cache, rbf_width);
	SG_REF(kernel);
	kernel->init(features, features);
	//cout<<"4"<<endl;
	
	CLibSVM* svm = new CLibSVM(svm_C, kernel, labels);
	SG_REF(svm);
	svm->set_epsilon(svm_eps);	

	int32_t num_subsets =2;	
	
	CStratifiedCrossValidationSplitting* splitting_strategy = 
		new CStratifiedCrossValidationSplitting(labels, num_subsets);
	
	CContingencyTableEvaluation* recall = 
		new CContingencyTableEvaluation(ACCURACY);
		
	CCrossValidation* c2 = new CCrossValidation(svm, features, labels, 
											splitting_strategy, recall);
	c2->set_num_runs(CROSSVAL_FOLDS);
	
	CrossValidationResult r2 = c2->evaluate();	
	
	kval_results rs;
	rs.accuracy = recall->get_accuracy();
	rs.error = recall->get_error_rate();
	rs.recall = recall->get_recall();
	rs.precision = recall->get_precision();
	rs.wracc = recall->get_WRACC();
	rs.f1 = recall->get_F1();
	rs.print();
	//cout<<results.mean<<endl;
	//cout<<r2.mean<<endl;	
}
double classify::predict_shogun_svm(Mat& featureMat, CLibSVM* svm)
{
	const int feature_cache=0;
	float64_t* feat = new float64_t[featureMat.rows*featureMat.cols];	
	
	int size = featureMat.rows;	
	convert_local_features(featureMat,feat);	
	
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);	
	SG_REF(features);
	features->set_feature_matrix(feat, featureMat.cols, featureMat.rows);
	/* SVM's when applied to new features data, returns classified labels as 
	 * CLables
	 **/		
	CLabels* newLbls = svm->apply(features);	
	SG_REF(newLbls);	
		
	double outcome = 0.0;
	for (int32_t i=0; i<size; i++)
	{
		//cout<<"label value:"<<newLbls->get_label(i)<<endl;
		outcome = newLbls->get_label(i);
	}		

	SG_UNREF(newLbls);	
	SG_UNREF(features);			
	return outcome;
}

double classify::predict_sh_svm_surf(Mat& featureMat,CLibSVM* svm)
{
	//SG_MALLOC(float64_t, featureMat.rows);
	//float64_t* feat = SG_MALLOC(float64_t, featureMat.rows*featureMat.cols);	
	const int feature_cache=0;
	float64_t* feat = new float64_t[featureMat.rows*featureMat.cols];	
	
	int size = featureMat.rows;	
	convert_local_features(featureMat,feat);	
	
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);	
	SG_REF(features);
	features->set_feature_matrix(feat, featureMat.cols, featureMat.rows);
	
	CLabels* newLbls = svm->apply(features);	
	SG_REF(newLbls);	
	
	double outcome = 0.00;
	for (int32_t i=0; i<size; i++){		
		outcome += newLbls->get_label(i);
	}		
	outcome = (outcome/size);
	cout<<"surf svm outcome:"<<outcome<<endl;
	
	SG_UNREF(newLbls);	
	SG_UNREF(features);			
	return outcome;
}

CLibSVM* classify::trainShogunSVM(Mat& featureMat,Mat& classesMat,string svmSaveFileName)
{
	cout<<"get here?..."<<featureMat.cols<<endl;
	const int32_t feature_cache=0;
	const int32_t kernel_cache=0;
	const float64_t rbf_width=10;
	const float64_t svm_C=10;
	const float64_t svm_eps=0.001;	
	//SG_MALLOC(float64_t, featureMat.rows);
	//float64_t* feat = SG_MALLOC(float64_t, featureMat.rows*featureMat.cols);
	cout<<"we are in here..."<<featureMat.cols<<endl;
	float64_t* lab =new float64_t[featureMat.rows];
	cout<<"prepared the labels"<<endl;
	int size=featureMat.rows*featureMat.cols;
	float64_t* feat;
	try{
	feat = new float64_t[size];
	}catch(ShogunException &sh)
	{
			cout<<sh.get_exception_string()<<endl;
	}		
	convert_local_features(featureMat,classesMat,feat,lab);	
	CLabels* labels=new CLabels(SGVector<float64_t>(lab, classesMat.rows));
	SG_REF(labels);
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);
	
	SG_REF(features);
	features->set_feature_matrix(feat, featureMat.cols, featureMat.rows);
	
	CGaussianKernel* kernel = new CGaussianKernel(kernel_cache, rbf_width);
	SG_REF(kernel);
	kernel->init(features, features);
	
	
	CLibSVM* svm = new CLibSVM(svm_C, kernel, labels);
	SG_REF(svm);
	svm->set_epsilon(svm_eps);
	cout<<"about to start training the shogun :"<<endl;	
	svm->train();		
	CSerializableAsciiFile* cs = new CSerializableAsciiFile(svmSaveFileName.c_str(),'w');	
	svm->save_serializable(cs);
	cout<<"svm training finished!!!"<<endl;	
	
	delete feat;
	cout<<"feat floats deleted"<<endl;
	delete lab;
	cout<<"lab floats deleted"<<endl;
	delete labels;
	cout<<"labels deleted"<<endl;
	delete features;
	cout<<"features deleted"<<endl;
	
	return svm;
}
void classify::trainAndSaveShogunSVM(Mat& featureMat,Mat& classesMat,string svmSaveFileName)
{
	const int32_t feature_cache=0;
	const int32_t kernel_cache=0;
	const float64_t rbf_width=10;
	const float64_t svm_C=10;
	const float64_t svm_eps=0.001;	
	//SG_MALLOC(float64_t, featureMat.rows);
	//float64_t* feat = SG_MALLOC(float64_t, featureMat.rows*featureMat.cols);
	cout<<"we are in here..."<<featureMat.cols<<endl;
	float64_t* lab =new float64_t[featureMat.rows];
	cout<<"prepared the labels"<<endl;
	int size=featureMat.rows*featureMat.cols;
	float64_t* feat;
	try{
	feat = new float64_t[size];
	}catch(ShogunException &sh)
	{
		cout<<sh.get_exception_string()<<endl;
	}		
	convert_local_features(featureMat,classesMat,feat,lab);	
	CLabels* labels=new CLabels(SGVector<float64_t>(lab, classesMat.rows));
	SG_REF(labels);
	CSimpleFeatures<float64_t>* features = new CSimpleFeatures<float64_t>(feature_cache);
	
	SG_REF(features);
	features->set_feature_matrix(feat, featureMat.cols, featureMat.rows);
	
	CGaussianKernel* kernel = new CGaussianKernel(kernel_cache, rbf_width);
	SG_REF(kernel);
	kernel->init(features, features);
	
	
	CLibSVM* svm = new CLibSVM(svm_C, kernel, labels);
	SG_REF(svm);
	svm->set_epsilon(svm_eps);
	cout<<"about to start training the shogun :"<<endl;	
	svm->train();		
	CSerializableAsciiFile* cs = new CSerializableAsciiFile(svmSaveFileName.c_str(),'w');	
	svm->save_serializable(cs);
	cout<<"svm training finished!!!"<<endl;	
	
	/* the memory allocated using SG_REF, has to go out via SG_UNREF,
	 * using delete throws the glibc error */ 	
	delete feat;
	cout<<"feat floats deleted"<<endl;
	delete lab;
	cout<<"lab floats deleted"<<endl;
	SG_UNREF(labels);
	//delete labels;
	cout<<"labels deleted"<<endl;
	SG_UNREF(features);
	//delete features;
	cout<<"features deleted"<<endl;
	SG_UNREF(kernel);
	//delete kernel;
	cout<<"kernel deleted"<<endl;
	delete cs;
	cout<<"serialized file deleted"<<endl;	
}
