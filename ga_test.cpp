#include "vision.h"

/* so we need to decide the kernel

1) kernel-> from a choice of 3 kernels
2) c range -> from values of 0.5 to 20
3) gamma range -> from values of 0.5 to 20

 -a chromosome would have those 3
 -a population would have multiple chromosomes
 -generations

 -kernel array: 0 -> RBF
 *1 -> SIGMOID
 *2 -> POLY

*/
typedef struct{
	int accuracy;
	chromosome chromo;
} chromo_performance;

typedef struct{
	int kernel;
	double softmargin;
	double gamma;
	chromosome(int k, double c, double g):
		kernel(k),softmargin(c),gamma(g);
}chromosome;

int kernel_collection[3] = {0,1,2};
void init(int fit_value)
{
	fitness_value = fit_value;
	elitism_no = elitism
}
chromosome gen_chromosome(int kernel_index)
{
	int kernel = rand()%3;
	
	kernel = kernel_collection[int kernel_index];	
	c = gen_rand_value(lowerBound, upperBound);
	gamma  = gen_rand_value(lowerBound, upperBound);
	
	chromosome chrom(kernel,c, gamma);	
	return chrom;
}

chromosome* gen_population(int pop_count)
{
	chromosome* population;
	for(int pop_counter=0;pop_counter < pop_max; pop_counter++)
	{
		population[pop_counter] = gen_chromosome();		
	}
	return population;
}
void sort_and_remove_unfit(chromosome& population)
{
	bubble_sort(population);	
	for(int elite_counter=elitism_no;elite_counter < population_size; elite_counter++)
	{
		population[i] = 0;
	}
}
void crossover(chromosome& population, int membersToFill)
{
	for(int fillCount = 0;i< fillCount; i++)
	{
		fillcount		
	}
	
}
void test_population(int fitness_function,int generations,int pop_count)
{
	stack<chromosome*> generations;
	generations.push_back(gen_population(pop_count));
	for(int gen_counter = 0;gen_counter < generations; gen_counter++)
	{	
		chromosome* population = generations.pop();
		chromo_performance* next_generation = train_and_evaluate_classifier(population[gen_counter]);
		/*it should return the accuracies, mapped to their respective choromosomes*/		
		sort_and_remove_unfit(results);
		/* now we should create the new population, from the old one */		
		crossover(results);
		mutate(results);
		chromosome* next_generation = results;
		generations.push_back(next_generation);
	}
}
