#include "ga.h"

/** so we need to decide the kernel

1) kernel-> from a choice of 3 kernels
2) c range -> from values of 0.5 to 20
3) gamma range -> from values of 0.5 to 20

 -a chromosome would have those 3
 -a population would have multiple chromosomes
 -generations

 -kernel array: 0 -> RBF
 *1 -> SIGMOID
 *2 -> POLY

p.s. other than the entire application, this class really screams 
* for a design pattern.
*/
int kernel_collection[3] = {0,1,2};

void init(int fit_value)
{
	fitness_value = fit_value;
	elitism_no = elitism
}
svm_chromosome GA::gen_svm_chromosome()
{
	int kernel = utils::gen_natural_rand_no(3);
		
	double c = utils::rand_number(1,20);
	double gamma = utils::rand_number(1,20);		
	svm_chromosome chromosome(kernel,c, gamma);		
	return chrom;
}

svm_chromosome* gen_population()
{
	svm_chromosome* population;
	for(int pop_counter=0;pop_counter < pop_max; pop_counter++)
	{
		population[pop_counter] = gen_svm_chromosome();		
	}
	return population;
}
void sort_and_remove_unfit(chromosome& population)
{
	bubble_sort(population);	
	for(int elite_counter=elitism_no;elite_counter < population_size; elite_counter++)
	{
		population[i] = 0;
	}
}
void crossover(chromosome& population, int membersToFill)
{
	for(int fillCount = 0;i< fillCount; i++)
	{
		fillcount		
	}
}
/**
 * Surely there must be a design pattern, where we would
 * ensure that all members with certain interger variables would 
 * have a way of sorting themselves in a collection
 */
void bubble_sort(svm_chromosome* chromosomes)
{
	bool sorted = false;
	while(!sorted)
	{
		sorted = true;
		for(unsigned int i =0;i!=POPULATION_SIZE-1;i++)
		{
			if(chromosomes[i].fitness > chromosomes[i+1].fitness)
			{
				svm_chromosome temp = chromosomes[i];
				chromosomes[i] = chromosomes[i+1];
				chromosomes[i+1]=temp;
				sorted = false;
			}
		}	
	}
}

void sort_and_remove_unfit(svm_chromosome* chromosomes)
{
	bubble_sort(chromosomes);
	/* with the chromosomes sorted, we have the best chromosomes on the top,
	 * so lets discard the unfit ones */
	for(unsigned int unfit_counter =ELITISM_COUNT;unfit_counter<POPULATION_SIZE;unfit_counter++)
	{	
		chromosomes[unfit_counter] = NULL;
	}	
}

void test_population(int fitness_function,int generations,int pop_count)
{
	stack<svm_chromosome*> ch_generations;
	ch_generations.push_back(gen_population(pop_count));
	for(int gen_counter = 0;gen_counter < GENERATIONS; gen_counter++)
	{	
		svm_chromosome* population = ch_generations.top();
		for(int i=0;i<POPULATION_SIZE;i++)
		{
			svm_chromosome ch = population[i];
			/* in the function below, the chromosome will be used to construct an svm
			 * and once finished the accuracy of the resulting svm will be recorded 
			 */
			trainEval_svm_chromo(ch);	
		}
		/* at this stage we are done with the chromosomes for the generation */
		/* next, crossover and mutate them */
		
		/*it should return the accuracies, mapped to their respective choromosomes*/		
		sort_and_remove_unfit(results);
		/* now we should create the new population, from the old one */		
		crossover(results);
		mutate(results);
		chromosome* next_generation = results;
		generations.push_back(next_generation);
	}
}
