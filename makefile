CC= g++
CFLAGS= -Wall -g -std=c++0x -pthread
INCLUDES= -I/usr/local/include/opencv -I/usr/local/include
LD_FLAGS = -L/usr/local/lib 


LIBS = -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann -lopencv_nonfree -lopencv_gpu -lopencv_videostab -lopencv_ts -lshogun

OBJ_FEATURES=features.o
OBJ_CLASSIFY=classifiers.o
OBJ_UTILS=utils.o
EXP_HELPER=ExpHelper.o

MAIN=main
MAIN2=testMain
MAIN3=testMain2
EXP2=exp2
EXP3=exp3
MARKER=marker
PART_EXP=parts
PART_TST=partsTest


$(MARKER):vision.h $(OBJ_UTILS)
	$(CC) $(CFLAGS) -o marker objMarker.cpp $(OBJ_UTILS) $(LIBS) 

$(OBJ_FEATURES):vision.h features.h
	$(CC) -c $(CFLAGS) features.cpp $(LIBS) 

$(OBJ_CLASSIFY):vision.h classifiers.h
	$(CC) -c $(CFLAGS) classifiers.cpp $(LIBS) 

$(OBJ_UTILS):vision.h utils.h
	$(CC) -c $(CFLAGS) utils.cpp $(LIBS) 

$(EXP_HELPER):vision.h ExpHelper.h
	$(CC) -c $(CFLAGS)  ExpHelper.cpp $(LIBS) 

$(MAIN):$(OBJ_FEATURES) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o main main.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(LIBS) 
	
$(EXP2):$(OBJ_FEATURES) vision.h classifiers.h
	$(CC) $(CFLAGS) -o $(EXP2) exp2ensamble.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(LIBS) 

$(EXP3):$(OBJ_FEATURES) vision.h classifiers.h
	$(CC) $(CFLAGS) -o $(EXP3) exp3.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(LIBS) 

all:$(OBJ_FEATURES) $(EXP_HELPER) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o $(PART_EXP) part_exp.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(EXP_HELPER) $(LIBS)
	
$(PART_TST):$(OBJ_FEATURES) $(EXP_HELPER) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o $(PART_TST) part_exp.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(EXP_HELPER) $(LIBS)
	
$(PART_EXP):$(OBJ_FEATURES) $(EXP_HELPER) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o $(PART_EXP) part_exp.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(EXP_HELPER) $(LIBS)

$(MAIN2):$(OBJ_FEATURES) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o $(MAIN2) main.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(LIBS)

$(MAIN3):$(OBJ_FEATURES) $(OBJ_CLASSIFY) $(OBJ_UTILS) vision.h
	$(CC) $(CFLAGS) -o $(MAIN3) main.cpp $(OBJ_FEATURES) $(OBJ_UTILS) $(OBJ_CLASSIFY) $(EXP_HELPER) $(LIBS) 

clean:
	rm -rf *.o $(MAIN) 
