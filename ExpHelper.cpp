#include "ExpHelper.h"
#include "utils.h"
#include "features.h"
#include "classifiers.h"

const string IMAGE_EXTENSION =".jpg";

void bootstrap_face_help(feature_class_pair& fullData,
						bootstrap_params& params,
						features& detector,
						classify& cls,
						cls_ensemble& ensemble,
						double SVM_DET_THRESHOLD)
{
	vector<vec_entry> vec_entries;
	utils::get_vec_entries(params.testvecfile,vec_entries);
	
	for(unsigned int i=0;i < vec_entries.size();i++)
	{
		vec_entry entry = vec_entries[i];
		Mat image = imread(entry.filename,CV_LOAD_IMAGE_GRAYSCALE);	
		if(!image.empty())	
		{
			resize(image,image,Size(32,32));			
			vector<Rect> windows = utils::get_sliding_windows(image,params.samplewidth,params.sampleheight,16,16);
			for(unsigned int i=0;i<windows.size(); i++)
			{
				Rect window = windows[i];
				Mat descriptors;
				Mat classes;
				Mat roi(image,window);			
				detector.compute_hog_features(image,1,descriptors,classes);
				double outcome=0.0;
				if(!ensemble.useShogun)
				{
					//outcome = cls.predict_svm(ensemble.cv_svm,descriptors,0);
					/* in case the image is classified as negative, its an FP */		
					if(outcome < SVM_DET_THRESHOLD)	
					{
						fullData.hog_features.push_back(descriptors);
						fullData.hog_classes.push_back(classes);
					}			
				}
				else
				{			
					outcome = cls.predict_shogun_svm(descriptors,ensemble.shogun_svm);						
					/* in case the image is classified as negative, its an FP */				
					if(outcome < SVM_DET_THRESHOLD)
					{
						cout<<"outcome:"<<outcome<<endl;
						fullData.hog_features.push_back(descriptors);
						fullData.hog_classes.push_back(classes);
					}			
				}		
			}
		}
		//we have the image, now lets get the features from it			
	}
}
void exp_controller::bootstrap_train(bootstrap_params& params)
{
	feature_class_pair data;
	features detector(params.samplewidth,params.sampleheight,params.randwidth,params.randheight);	
	classify cls;	
	//the last param is a rand neg win		
	data = detector.get_hog_data_for_tud(params.posvec,params.negvec,1);		
	CLibSVM* svm = NULL;
	
	for(int i=0;i<params.iterations;i++)
	{
		cout<<"iteration....."<<i<<endl;
		if(params.algorithmType == TYPE_HOG)
		{	
			cls.trainAndSaveShogunSVM(data.hog_features,data.hog_classes,params.savefilename);
			cout<<"About to load the svm..."<<endl;
			CSerializableAsciiFile* cs = new CSerializableAsciiFile(params.savefilename.c_str(),'r');	
			CLibSVM* shogunSvm = new CLibSVM;				
			shogunSvm->load_serializable(cs);			
			cls_ensemble ensemble;
			ensemble.useShogun = true;
			ensemble.shogun_svm =  shogunSvm;
			cout<<"About to seek bootstrap help..."<<endl;
			bootstrap_face_help(data,params,detector,cls,ensemble,SVM_DET_THRESHOLD);
			
			delete shogunSvm;
		}
		else
		{
			//data = det2.get_surf_data(posVec,negVec);
			cls.trainShogunSVM(data.surf_features,data.surf_classes,params.savefilename);
		}
		/* since we persist each trained svm to disk at each iteration, we can keep 
		 * deleting the pointer to the svm in each iteration.	
		 */			
	}
	exit_shogun();	
}
void exp_controller::test_ensemble(ensemble_tst_params params)
{
	init_shogun();

	parts_ensemble ensemble;
	cls_ensemble cls_face;
	cls_ensemble cls_torso;

	CSerializableAsciiFile* faceCS = new CSerializableAsciiFile(params.faceclassifiername.c_str(),'r');
	
	CLibSVM* faceSVM = new CLibSVM;	
	cout<<"svm detection threshold:"<<SVM_DET_THRESHOLD<<endl;
	cout<<"Loading the face SVM....be patient..!"<<endl;
	faceSVM->load_serializable(faceCS);
	
	cout<<"Now onto the torso SVMs..!"<<endl;
	CLibSVM* torsoSVM = new CLibSVM;		
	CSerializableAsciiFile* torsoCS = new CSerializableAsciiFile(params.torsoclassifiername.c_str(),'r');
	cout<<"Loading the torso SVM....be patient..!"<<endl;
	torsoSVM->load_serializable(torsoCS);
	
	ensemble.useShogun = true;	
	ensemble.face_svm = faceSVM;
	ensemble.torso_svm = torsoSVM;	

	cls_face.useShogun = true;		
	cls_torso.useShogun = true;
	
	cls_face.shogun_svm = faceSVM;
	cls_torso.shogun_svm = torsoSVM;
	
	Mat drawnImage;
	vector<string> filesindir;
	string vecfilename ="testset.vec";
	int count =0;
	utils::get_body_part_vecs(vecfilename,filesindir);
	string name="Video Test";
	double outcome = 0.00;
	ofstream detectionLog(params.detectionLogName.c_str());
	for(unsigned int i=0;i<filesindir.size();i++)
	{
		string filename =filesindir[i];
		Mat frame = imread(filename);
		cout<<"processing frame:"<<filename<<endl;
		resize(frame,frame,Size(320,240));
		Mat faceFrame = frame.clone();
		if(params.testtype == TYPE_HOG )
		{
			outcome = detect_object(params.FACE_DET_WIDTH,params.FACE_DET_HEIGHT,filename,cls_face,frame);					
			outcome = detect_object(params.samplewidth,params.sampleheight,filename,cls_torso,frame);					
		
			/* comment out the line below, for now atleast */
			//outcome = detect_obj_for_ensemble(params.samplewidth,params.sampleheight,filename.c_str(),ensemble,frame,detectionLog);					
		}
		imshow(name,frame);		
		ostringstream str;
		str<<params.imageoutputLoc<<"frame_"<<count<<"_"<<outcome<<IMAGE_EXTENSION<<endl;
		//str<<params.imageoutputLoc<<"frame_F"<<count<<"_"<<outcome<<IMAGE_EXTENSION<<endl;
		imwrite(str.str(),frame);
		count++;
		if(waitKey(30) >=0) break;	
		
	}		
	
	detectionLog.close();
	//detect_obj_for_ensemble
}
void exp_controller::test_svm(testing_params params)
{
	init_shogun();
	cout<<params.classifiername.c_str()<<endl;
	CSerializableAsciiFile* cs = new CSerializableAsciiFile(params.classifiername.c_str(),'r');	
	CLibSVM* shogunSvm = new CLibSVM;	
	cout<<"svm detection threshold:"<<SVM_DET_THRESHOLD<<endl;
	cout<<"About to load the SVM be patient...."<<endl;
	shogunSvm->load_serializable(cs);
	cout<<"SVM loaded...."<<endl;
	cls_ensemble ensemble;
	ensemble.useShogun = true;
	ensemble.shogun_svm = shogunSvm;	
	Mat drawnImage;
	VideoCapture cap(params.videoname);
	
	if(!cap.isOpened())
	{
		cout<<"unable to open video...whats happening?..."<<endl;
		return;
	}
	string name="Video Test";
	int count = 0;
	for(;;)
	{
		Mat frame;		
		cap >> frame;	
		if(!frame.empty())
		{		
			cout<<"processing frame"<<count<<endl;
			resize(frame,frame,Size(320,240));
			int outcome = 0;
			if(params.testtype == TYPE_HOG )
			{
				outcome = detect_object(params.samplewidth,params.sampleheight,params.videoname.c_str(),ensemble,frame);					
			}
			else{
				outcome = detect_surf_object(params.samplewidth,params.sampleheight,params.videoname.c_str(),ensemble,frame);					
			}
			
			imshow(name,frame);		
			ostringstream str;
			str<<params.imageoutputLoc<<"frame_"<<count<<"_"<<outcome<<IMAGE_EXTENSION<<endl;
			imwrite(str.str(),frame);
		}
		
		count++;
		if(waitKey(30) >=0) break;
	}
	
	/* great! so the setup is complete now procced to the detection phase */
	exit_shogun();
}
//training_params params
void exp_controller::train_svm_fullbody(training_params params)
{
	init_shogun();
	features det2(params.samplewidth,params.sampleheight,params.randwidth,params.randheight);
	classify cls;
	feature_class_pair data;
	if(params.algorithmType == TYPE_HOG)
	{
		//the last param is a rand neg win
		data = det2.get_hog_data_for_tud(params.posvec,params.negvec,2,1);
		cls.trainShogunSVM(data.hog_features,data.hog_classes,params.savefilename);
	}
	else
	{
		//data = det2.get_surf_data(posVec,negVec);
		cls.trainShogunSVM(data.surf_features,data.surf_classes,params.savefilename);
	}
	exit_shogun();

}
/* for the time being only the shogun version */

void exp_controller::train_svm(training_params params)
{
	
	init_shogun();
	features det2(params.samplewidth,params.sampleheight,params.randwidth,params.randheight);
	classify cls;
	feature_class_pair data;
	if(params.algorithmType  == TYPE_HOG)
	{
		cout<<"About to start extracting HOG features....."<<endl;
		data = det2.extract_from_vec(params.posvec,params.negvec,params.posprefix,params.negprefix,params.imgextensiontoadd);
		cout<<"training svm with HOG features....."<<endl;
		cls.trainShogunSVM(data.hog_features,data.hog_classes,params.savefilename);
		cout<<"training finished and the svm is at:"<< params.savefilename<<endl;
	}
	else
	{
		cout<<"About to start extracting SURF features....."<<endl;
		data = det2.extract_vec_for_surf(params.posvec,params.negvec,params.posprefix,params.negprefix);
		cls.trainShogunSVM(data.surf_features,data.surf_classes,params.savefilename);
	}
	exit_shogun();
}
void exp_controller::train_LMDa_upper_body(training_params params)
{
	init_shogun();
	features det2(params.samplewidth,params.sampleheight,params.randwidth,params.randheight);
	classify cls;	
	if(params.algorithmType == TYPE_HOG)
	{
		cout<<"About to start extracting HOG features....."<<endl;
		vector<Mat> posSamples,negSamples;
		utils::get_LMDa_torso_crops(posSamples);
		utils::get_nicta_neg_peds(negSamples);		
		feature_class_pair trainingData = det2.get_hog_data_for_imgs(posSamples,negSamples);
		cout<<"training svm with HOG features....."<<endl;
		cls.trainShogunSVM(trainingData.hog_features,trainingData.hog_classes,params.savefilename);
		cout<<"training finished and the svm is at:"<< params.savefilename<<endl;
	}
	exit_shogun();
}
void exp_controller::train_face_svm(training_params params)
{
	init_shogun();
	features det2(params.samplewidth,params.sampleheight,params.randwidth,params.randheight);
	classify cls;	
	if(params.algorithmType == TYPE_HOG)
	{
		cout<<"About to start extracting HOG features....."<<endl;
		vector<Mat> posSamples,negSamples;
		utils::get_face_images(posSamples,true);
		utils::get_nicta_neg_peds(negSamples);		
		feature_class_pair trainingData = det2.get_hog_data_for_imgs(posSamples,negSamples,params.samplewidth,params.sampleheight);
		cout<<"training svm with HOG features....."<<endl;
		cls.trainShogunSVM(trainingData.hog_features,trainingData.hog_classes,params.savefilename);
		cout<<"training finished and the svm is at:"<< params.savefilename<<endl;
	}
	exit_shogun();
}
/* a private helper function, which exists because i hate writing the same 
 * loop twice 
 REMEMBER THE WIN HEIGHT AND WIDTH ARE HARD CODED MAKE IT PARAMETERIZED */
void exp_controller::loop_helper(vector<string>& filecollection, int type,cls_ensemble& ensemble,
				conf_matrix& resultMat,string outputLocation,string testImagesLoc)
{
	cout<<"filecollection:"<<filecollection.size()<<endl;
	for(unsigned int i=0;i< filecollection.size();i++)
	{
		/* lets call the function with the method */		
		
		Mat drawnImage;
		/* LOOK AT THE PART BELOW */
		string imagename;
		imagename.append(testImagesLoc);
		if(type == POSITIVE) {	imagename.append("pos/"); }
		else{ imagename.append("neg/"); }
		imagename.append(filecollection[i]);
		int outcome =0;
		outcome = detect_object(120,80,imagename,ensemble,drawnImage);
		
		string imgName = filecollection[i].replace(filecollection[i].find(IMAGE_EXTENSION),sizeof(IMAGE_EXTENSION),"");
		ostringstream str;
		str<<outputLocation<<imgName<<"_"<<outcome<<".jpg";		
		imwrite(str.str(),drawnImage);
		
		if(type == POSITIVE) {	
			
			if(outcome < 1){ resultMat.PAsN++; }else{ resultMat.PAsP++;}
			
		} 
		else { 
			if(outcome < 1){ resultMat.NAsN++; }else{ resultMat.NAsP++;}
		}
	}
}
void exp_controller::analyse_imgs_for_object(string imagefolder,cls_ensemble& ensemble)
{
	/* exp name and no, in case we run the same experiments N no of itmes */
	string expName = "test";
	string expNo = "1";
	string featuresToUse ="HOG";
	/* the format is to have expdata(this stores results) and the experiment
	 * name or number i.e. expdata/exp_1/ */
	string resultsOutput = "expdata/random/";
	string classifierStr = "SVM";
	/* for the number of dirnames that we have, lets run each experiment a number of times */
	for(int i=0;i<1;i++)
	{
		//string dirname = argv[i];//suppose the argv 1 is LAB_1
		string dirname = imagefolder;
		cout<<dirname<<endl;		
		string outLoc;
		
		outLoc.append(resultsOutput);
		/* the dir name is the exp name */
		outLoc.append(dirname);	outLoc.append("/");
		/* to that append the features that are being used */
		outLoc.append(featuresToUse);outLoc.append("/");
		/* to that append the classifiers that are being used*/
		//outLoc.append(classifierStr);outLoc.append("/");
		/* at his point we now have our results directory set up */		
		
		/* assuming the images are stored in the test videos, we now start
		 * building the path to our testdata from the input dir name we have
		 * in this case all the testvideos are stored in testvideos
		 **/		
		string testImagesLoc = "testvideos/";
		/* now join the name of the actual directory */		
		testImagesLoc.append(dirname);testImagesLoc.append("/");
		/* the name of the subfolder where the images are, in this case 
		 * we were playing with a number of different resolutions */
		testImagesLoc.append("small_320x240/");
		cout<<"testImagesLoc:"<<testImagesLoc<<endl;
		/* now at this point we know where the images are, and where we 
		 * need to store the results */
		
		/* now lets use the test path above to get the images */		 
		vector<string> posfiles,negfiles;	
		/* assuming we have divided the test images in pos and neg */
		string posdir = testImagesLoc+"pos";
		cout<<posdir<<endl;		
		string negdir =testImagesLoc+"neg";
		cout<<negdir<<endl;		
		
		vector<string> posFiles,negFiles;
		utils::get_files_in_dir(posdir,posfiles);
		utils::get_files_in_dir(negdir,negfiles);
		/* next we are constructing the log name, which will be stored where
		 * we will store our results, and it will be the name, no and the
		 * dirname for the experiment */
		string frameLogName = resultsOutput+expName+"_"+expNo+"_"+dirname+".txt";
		
		/* now the last thing we need to do is print out all the strings
		 * we have constructed */		 			
		cout<<"output images save loc:"<<outLoc<<endl;
		cout<<"frameLogName:"<<frameLogName<<endl;
		cout<<"the number of pos files:"<<posfiles.size()<<endl;
		cout<<"the number of neg files:"<<negfiles.size()<<endl;
		/* ok now good to go? sure lets start the experiments */
		/* one last thing, we need to get the conf matrix */
		conf_matrix cMat;
		
		ofstream detectionLog(frameLogName.c_str());
		
		/* we need the file collection, */				
		loop_helper(posfiles,POSITIVE,ensemble,cMat,outLoc,testImagesLoc);
		loop_helper(negfiles,NEGATIVE,ensemble,cMat,outLoc,testImagesLoc);
		
		detectionLog<<"expNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"posAsPos"<<","<<"posAsNeg"<<","<<"negAsPos"<<","<<"negAsNeg"<<endl;
		detectionLog<<expNo<<","<<dirname<<","<<featuresToUse<<","<<cMat.PAsP<<","<<cMat.PAsN<<","<<cMat.NAsP<<","<<cMat.NAsN<<endl;
		detectionLog.close();
	}	
}

/**
 * The reason why the name of the image exists is to facilitate the supply 
 * of an empty image during function call. So the imagename param is totally 
 * optional when the drawnImage param contains an actual image.
 * @return the number of patches detected as positive
 **/
int exp_controller::detect_object(int DW_WIDTH,int DW_HEIGHT,string imagename,cls_ensemble& ensemble,Mat& drawnImage)
{	
	features detector(DW_WIDTH,DW_HEIGHT);
	classify cls_meta;
	if(drawnImage.empty())	
	{
		drawnImage = imread(imagename.c_str());	
	}
	//resize(drawnImage,drawnImage,Size(320,240));	
	Mat image;
	cvtColor(drawnImage,image,CV_RGB2GRAY);	
	vector<Rect> wins = utils::get_sliding_windows(image,DW_WIDTH,DW_HEIGHT,16,16);
	
	vector<node> detectedBoxes;
	for(unsigned int rectCounter= 0;rectCounter < wins.size();rectCounter++)
	{
		Rect region = wins[rectCounter];
		Mat roi(image,region);
		Mat h_desc;		
		detector.compute_hog_features(roi,h_desc);		
		double outcome =0.00;
		//now in here we should have a trained classifier or something		
		if(!ensemble.useShogun)
		{
			outcome = cls_meta.predict_svm(ensemble.cv_svm,h_desc,0);
			if(outcome > SVM_DET_THRESHOLD)							
			{
				node nde;
				nde.rect = region;
				nde.groupId = 0;//remeber the group id is always 0 when creating the node
				detectedBoxes.push_back(nde);
			}			
		}
		else
		{			
			outcome = cls_meta.predict_shogun_svm(h_desc,ensemble.shogun_svm);						
			if(outcome > SVM_DET_THRESHOLD)
			{
				//cout<<"output value:"<<outcome<<endl;
				node nde;
				nde.rect = region;
				nde.groupId = 0;//remeber the groupgroup id is always 0 when creating the node	
				detectedBoxes.push_back(nde);
				ostringstream str;
				str <<outcome<<endl;
				//putText(drawnImage,str.str().c_str(),Point(region.x,region.y),FONT_HERSHEY_SIMPLEX,1,CV_RGB(255,0,0));
				rectangle(drawnImage,region,CV_RGB(0,255,0),2);
			}			
		}		
	}
	
	map<int,vector<Rect>> groups;
	cout<<"detected boxes size:"<<detectedBoxes.size()<<endl;
	find_groups(detectedBoxes,drawnImage,groups,75.0);	
	return detectedBoxes.size();
}
int exp_controller::detect_obj_for_ensemble(int DW_WIDTH,int DW_HEIGHT,string imagename,parts_ensemble& ensemble,Mat& drawnImage,ofstream& detlog)
{	
	features detector(DW_WIDTH,DW_HEIGHT);
	classify cls_meta;
	if(drawnImage.empty())	
	{
		drawnImage = imread(imagename.c_str());	
	}
	//resize(drawnImage,drawnImage,Size(320,240));	
	Mat image;
	cvtColor(drawnImage,image,CV_RGB2GRAY);	
	vector<Rect> wins = utils::get_sliding_windows(image,DW_WIDTH,DW_HEIGHT,16,16);
	
	vector<node> detectedBoxes;
	for(unsigned int rectCounter= 0;rectCounter < wins.size();rectCounter++)
	{
		Rect region = wins[rectCounter];
		Mat roi(image,region);
		Mat h_desc;		
		detector.compute_hog_features(roi,h_desc);		
		double faceoutcome =0.00;
		double torsooutcome = 0.00;
		double outcome = 0.00;	
		//now in here we should have a trained classifier or something		
		if(!ensemble.useShogun)
		{
			outcome = cls_meta.predict_svm(ensemble.cv_svm,h_desc,0);
			if(outcome > SVM_DET_THRESHOLD)							
			{
				node nde;
				nde.rect = region;
				nde.groupId = 0;//remeber the group id is always 0 when creating the node
				detectedBoxes.push_back(nde);
			}			
		}
		else
		{			
			faceoutcome = cls_meta.predict_shogun_svm(h_desc,ensemble.face_svm);	
			torsooutcome = cls_meta.predict_shogun_svm(h_desc,ensemble.torso_svm);				

			if(faceoutcome > SVM_DET_THRESHOLD ||torsooutcome > SVM_DET_THRESHOLD )
			{
				//cout<<"output value:"<<outcome<<endl;
				node nde;
				nde.rect = region;
				nde.groupId = 0;//remeber the groupgroup id is always 0 when creating the node	
				detectedBoxes.push_back(nde);
				ostringstream str;
				str <<outcome<<endl;
				if(faceoutcome >torsooutcome )
				{
					cout<<"its a face"<<endl;
					//putText(drawnImage,"face",Point(region.x,region.y),FONT_HERSHEY_SIMPLEX,1,CV_RGB(255,0,0));
				}
				else{
					cout<<"its a torso"<<endl;
					//putText(drawnImage,"torso",Point(region.x,region.y),FONT_HERSHEY_SIMPLEX,1,CV_RGB(255,0,0));
				}

				rectangle(drawnImage,region,CV_RGB(0,255,0),2);
			}			
		}		
	}
	
	map<int,vector<Rect>> groups;
	cout<<"detected boxes size:"<<detectedBoxes.size()<<endl;
	find_groups(detectedBoxes,drawnImage,groups,detlog,imagename,75.0);	
	return detectedBoxes.size();
}
int exp_controller::detect_surf_object(int DW_WIDTH,int DW_HEIGHT,string imagename,cls_ensemble& ensemble,Mat& drawnImage)
{	
	features detector(DW_WIDTH,DW_HEIGHT);
	classify cls_meta;
	if(drawnImage.empty())	
	{
		drawnImage = imread(imagename.c_str());	
	}	
	Mat image;
	cvtColor(drawnImage,image,CV_RGB2GRAY);	
	vector<Rect> wins = utils::get_sliding_windows(image,DW_WIDTH,DW_HEIGHT,16,16);
	
	vector<node> detectedBoxes;
	for(unsigned int rectCounter= 0;rectCounter < wins.size();rectCounter++)
	{
		Rect region = wins[rectCounter];
		Mat roi(image,region);
		Mat h_desc;		
		detector.extract_keypoints(roi,h_desc);				
		//now in here we should have a trained classifier or something
		double outcome = cls_meta.predict_sh_svm_surf(h_desc,ensemble.shogun_svm);
		if(outcome > SVM_DET_THRESHOLD)
		{
			//cout<<"output value:"<<outcome<<endl;
			node nde;
			nde.rect = region;
			nde.groupId = 0;//remeber the groupgroup id is always 0 when creating the node	
			detectedBoxes.push_back(nde);
			ostringstream str;
			str <<outcome<<endl;
			putText(drawnImage,str.str().c_str(),Point(region.x,region.y),FONT_HERSHEY_SIMPLEX,1,CV_RGB(255,0,0));
			rectangle(drawnImage,region,CV_RGB(0,255,0),2);
		}			
		
	}
	map<int,vector<Rect>> groups;
	//cout<<"detected boxes size:"<<detectedBoxes.size()<<endl;
	//find_groups(detectedBoxes,drawnImage,groups,100.0);	
	return detectedBoxes.size();
}
void exp_controller::record_classifier_history(int pCount,int nCount,bool useHog,bool useSurf,
								   vote_collection votes,ensemble_hist& history)
{
	int posLabel = 1;
	int negLabel =-1;
	if(pCount>nCount)
	{
			if(votes.svmVote > 0){					
				utils::update_cls_hist(history.svm,negLabel,posLabel);
			}
			else {	
				utils::update_cls_hist(history.svm,posLabel,negLabel);	
			}			
			if(votes.knnVote >0){	
				utils::update_cls_hist(history.knn,negLabel,posLabel); 
			} 
			else {	
				utils::update_cls_hist(history.knn,posLabel,negLabel);	
			}			
			if(votes.boostVote>0){	
				utils::update_cls_hist(history.boost,negLabel,posLabel);	
			}	 
			else {	
				utils::update_cls_hist(history.boost,posLabel,negLabel);	
			}
	}
	else{		
			if(votes.svmVote <0){ 
				utils::update_cls_hist(history.svm,negLabel,posLabel);
			} 
			else {	
				utils::update_cls_hist(history.svm,posLabel,negLabel); 
			}
			
			if(votes.knnVote<0)
			{	
				utils::update_cls_hist(history.knn,negLabel,posLabel);	
			} 
			else {
				utils::update_cls_hist(history.knn,posLabel,negLabel);
			}
			
			if(votes.boostVote<0){	
				utils::update_cls_hist(history.boost,negLabel,posLabel); 
			} 
			else { 
				utils::update_cls_hist(history.boost,posLabel,negLabel);
			}					
	}
}

df_outcome find_max(double svm, double knn, double boost,int svmVote,int knnVote,int boostVote)
{
	df_outcome outcome;
	if(svm>knn){
		if(svm > boost){	
			outcome.accuracy = svm;
			outcome.vote = svmVote;
		}
		else{	
			outcome.accuracy = boost;
			outcome.vote = boostVote;
		}
	}
	else{
		
		if(knn>boost){	
			outcome.accuracy = knn;
			outcome.vote = knnVote;
		}
		else {	
			outcome.accuracy = boost;
			outcome.vote = boostVote;			
		}
	}
	//if they are all the same then we should return the accuracy of the majority
	if(svm==knn && knn==boost)
	{
		int majority = svmVote+knnVote+boostVote;
		if(svmVote ==majority){ outcome.accuracy = svm;	outcome.vote = svmVote; }
		else if(knnVote ==majority){ outcome.accuracy = knn;outcome.vote = knnVote; }
		else{ outcome.accuracy = boost; outcome.vote = boostVote; }
	}
	return outcome;
}

int exp_controller::apply_decision_function(vote_collection votes,ensemble_hist& history)
{
	df_outcome winner;
	int outcome =0;
	
		
	double svmAccuracy = utils::find_historic_accuracy(history.svm);
	double knnAccuracy = utils::find_historic_accuracy(history.knn);
	double boostAccuracy = utils::find_historic_accuracy(history.boost);
			
	winner= find_max(svmAccuracy,knnAccuracy,boostAccuracy,votes.svmVote,votes.knnVote,votes.boostVote);
	cout<<"hog"<<winner.accuracy<<":and vote:"<<winner.vote<<endl;
	outcome = winner.vote;
		
	return outcome;

}

/*
 * Bubble sort is here so we do not have to specify the definition of
 * node in utils.h
 */
void bubble_sort(vector<node>& boxes)
{
	bool sorted = false;
	if(boxes.size()>1)
	{
		while(!sorted)
		{
			sorted = true;
			for(unsigned int i=0;i!=(boxes.size()-1);i++)
			{
				if(boxes[i].rect.x > boxes[i+1].rect.x)
				{
					node temp = boxes[i];
					boxes[i] = boxes[i+1];
					boxes[i+1]=temp;
					sorted = false;
				}
			}	
		}
	}
}
void exp_controller::find_groups(vector<node>& nodes,Mat& clone,map<int,vector<Rect>>& groups,ofstream& detLog,string imagename,int distThreshold)
{
	bubble_sort(nodes);
	int size = nodes.size();	
	int* adjMatrix = new int[size*size];
	bool canProcessFurther = true;	
	int groupId = 1;
	vector<node> newNodes;
	for(int i=0;i<size;i++)
	{
		node parent = nodes[i];		
		if(parent.groupId == 0)
		{
			parent.groupId = groupId;
			nodes[i] = parent;
			groups[groupId].push_back(parent.rect);
		}						
		for(int j=0;j<size;j++)
		{
			canProcessFurther = true;			
			if(i==j)
			{
				adjMatrix[i*size+j] = 1;
				canProcessFurther = false;				
			}			
			if(adjMatrix[j*size+i] == 1)
			{				
				adjMatrix[i*size+j] = 1;
				canProcessFurther = false;
			}
			if(canProcessFurther)
			{
				node neighabor = nodes[j];						
				if(utils::calculate_dist(parent.rect,neighabor.rect) < distThreshold )
				{
					neighabor.groupId = parent.groupId;
					adjMatrix[i*size+j] = 1;	
					groups[groupId].push_back(neighabor.rect);
				}
				else
				{
					adjMatrix[i*size+j] = 1;						
				}
			}			
		}
		groupId++;		
	}	
	map<int,vector<Rect>>::iterator it;
	for(it = groups.begin();it!=groups.end();it++)
	{
		vector<Rect> rects = (*it).second;		
		int endX,endY;
		Rect rect;
		if(rects.size()==1)		
		{	
			rect = rects[0];
			detLog <<imagename<<" "<<rect.x <<" "<<rect.y<<" "<<rect.width<<" " <<rect.height<<endl;
		}
		else{
			rect.x = utils::get_min(rects,0);			
			rect.y = utils::get_min(rects,1);
			/* the height and width should really be the same across all the rects */
			endX = 	(utils::get_max(rects,0))+rects[0].width;
			endY = (utils::get_max(rects,1))+rects[0].height;
			rect.width = (endX-rect.x);
			rect.height = (endY-rect.y);//(utils::get_max(rects,1)+rects[0].height);
			detLog <<imagename<<" "<<rect.x <<" "<<rect.y<<" "<<rect.width<<" " <<rect.height<<endl;
		}
		rectangle(clone,rect,CV_RGB(0,255,0),3);
	}	
}

void exp_controller::find_groups(vector<node>& nodes,Mat& clone,map<int,vector<Rect>>& groups,int distThreshold)
{
	bubble_sort(nodes);
	int size = nodes.size();	
	int* adjMatrix = new int[size*size];
	bool canProcessFurther = true;	
	int groupId = 1;
	vector<node> newNodes;
	for(int i=0;i<size;i++)
	{
		node parent = nodes[i];		
		if(parent.groupId == 0)
		{
			parent.groupId = groupId;
			nodes[i] = parent;
			groups[groupId].push_back(parent.rect);
		}						
		for(int j=0;j<size;j++)
		{
			canProcessFurther = true;			
			if(i==j)
			{
				adjMatrix[i*size+j] = 1;
				canProcessFurther = false;				
			}			
			if(adjMatrix[j*size+i] == 1)
			{				
				adjMatrix[i*size+j] = 1;
				canProcessFurther = false;
			}
			if(canProcessFurther)
			{
				node neighabor = nodes[j];						
				if(utils::calculate_dist(parent.rect,neighabor.rect) < distThreshold )
				{
					neighabor.groupId = parent.groupId;
					adjMatrix[i*size+j] = 1;	
					groups[groupId].push_back(neighabor.rect);
				}
				else
				{
					adjMatrix[i*size+j] = 1;						
				}
			}			
		}
		groupId++;		
	}
	map<int,vector<Rect>>::iterator it;
	for(it = groups.begin();it!=groups.end();it++)
	{
		vector<Rect> rects = (*it).second;		
		int endX,endY;
		Rect rect;
		if(rects.size()==1)//if there is only 1 elem in the array
		{	
			rect = rects[0];
			
		}
		else{
			rect.x = utils::get_min(rects,0);			
			rect.y = utils::get_min(rects,1);
			/* the height and width should really be the same across all the rects */
			endX = 	(utils::get_max(rects,0))+rects[0].width;
			endY = (utils::get_max(rects,1))+rects[0].height;
			rect.width = (endX-rect.x);
			rect.height = (endY-rect.y);//(utils::get_max(rects,1)+rects[0].height);			
		}
		rectangle(clone,rect,CV_RGB(0,255,0),3);
	
	}
}


