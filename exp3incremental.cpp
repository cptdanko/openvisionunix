#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"

mutex m;

int updateTreshold = 50;

CvKNearest aSurfKnn,surfKnnCopy,aHogKnn,hogKnnCopy;

bool knnRetrainingActive = false;
bool knnTrainingDone = false;

feature_class_pair knnActive,knnPassive;

void train_knn(feature_class_pair& data,bool isHTrain,bool isSTrain)
{	
	if(isHTrain){
		aHogKnn.train(data.hog_features,data.hog_classes,Mat(),true,KTH);
	}
	if(isSTrain){
		aSurfKnn.train(data.surf_features,data.surf_classes,Mat(),true,KTH);
	}
}
void updateKnn(bool isHTrain,bool isSTrain)
{
	m.lock();
	knnRetrainingActive = true;
	m.unlock();		
	if(isHTrain)
	{
		aHogKnn.train(knnActive.hog_features,knnActive.hog_classes,Mat(),true,KTH,32,true);
	}
	if(isSTrain)
	{
		aSurfKnn.train(knnActive.surf_features,knnActive.surf_classes,Mat(),true,KTH,32,true);
	}
	lock_guard<std::mutex> lk(m);
	knnTrainingDone = true;	
}
int main()
{ 
   bool v1 = false;bool v2=false;
   future<bool>done = async(std::launch::async,countToTen,1);
   future<bool>doIt = async(std::launch::async,countToTen,0);
   for(int counter=0;counter<1000000;counter++)
   {
      /*do something meaningfull....
        In my case i was looping through frames of a video
      */
        cout<<"in the counter:"<<endl;
      if(t1){
          value+=counter;
          cout<<"ok so we have finished the function"<<endl;
          cout<<firstVal<<":"<<secondVal<<endl;
          cout<<endl;
         //break;//ok we are done so lets get out of here
      }
      if(t2){cout<<"second finito"<<endl;}
      if(t1 && t2){cout<<"val:"<<secondVal<<":"<<firstVal<<endl;break;}
   }
}

