#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/legacy/legacy.hpp>
/***************C includes**********************/
#include <opencv/ml.h>
#include <opencv/cv.h>
/***************Cpp includes*******************/
#include <iostream>

using namespace cv;
using namespace std;

void test_cpp_surf(string imagename)
{
	Mat image = imread(imagename,CV_LOAD_IMAGE_GRAYSCALE);	
	SurfFeatureDetector detector(400,4,2,false,false);
	SurfDescriptorExtractor extractor(400,4,2,false,false);
	vector<KeyPoint> keypoints;
	Mat descriptors;
	detector.detect(image,keypoints);
	extractor.compute(image,keypoints,descriptors);
	cout<<"total keypoints:"<<keypoints.size()<<endl;
}
void test_cv_surf()
{
	const char* imagename = "frame_203.jpg";
	IplImage* image = cvLoadImage(imagename,CV_LOAD_IMAGE_GRAYSCALE);
	CvSURFParams params = cvSURFParams(400, 1);
	CvSeq* descriptors = 0;
	CvSeq* keypoints =0;
	CvMemStorage* storage = cvCreateMemStorage(0);
	
	cvExtractSURF(image,0,&keypoints,&descriptors,storage,params);
		
	printf("total keypoints extracted:%d",keypoints->total);	
	
	cvReleaseImage(&image);
}
void testCV(const char* imagename)
{
	Mat image = imread(imagename);
	cout<<"Total rows:"<<image.rows<<endl;
}
int main(int argc,char** argv)
{
	string imagename="frame_203.jpg";
	testCV(imagename.c_str());
	
	return 0;
}
