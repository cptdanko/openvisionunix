#include "features.h"
#include "classifiers.h"
#include "vision.h"
#include "utils.h"

#define HOG 1
#define SURF 2

#define SVM 1
#define KNN 2
#define BOOST 3


CvKNearest aHogKnn;
CvKNearest aSurfKnn;
CvNormalBayesClassifier aHogBayes,aSurfBayes;

int KTH=24;
string posVec,negVec;

CvSVM* hogSvmPtr;CvSVM* surfSvmPtr;
CvBoost* hogBoostPtr; CvBoost* surfBoostPtr;

CvStatModel *model;

bool useSURF = false; bool useHOG = false;
bool useSvm = false;bool useKnn=false; bool useBoost = false;

float sKnnTreshold = 0.4;	
float sBoostTreshold = 3;
float sSvmTreshold = 0.3;


void train_knn(feature_class_pair& data,bool isHTrain,bool isSTrain)
{	

	cout<< "***********************KNN training started***********************"<<endl;
	if(isHTrain){
		aHogKnn.train(data.hog_features,data.hog_classes,Mat(),true,KTH);
		//iHogKnn.train(data.hog_features,data.hog_classes,Mat(),false,KTH);
	}
	if(isSTrain){
		//iSurfKnn.train(data.surf_features,data.surf_classes,Mat(),false,KTH);
		aSurfKnn.train(data.surf_features,data.surf_classes,Mat(),true,KTH);
	}
	cout<< "***********************KNN training finished***********************"<<endl;
}
			
int operate_slidewins(Mat& origFrame,vector<Rect> wins,Mat& frame,string imgOutLoc,
			string frameCount,ofstream &csvlog,features& detector,
			classify& predictor)
{	
	
	int winsDetected = 0;
	csvlog<<"HogSVMVote,HogKnnVote,HogBoostVote,SurfSVMVote,SurfKnnVote,SurfBoostVote,PosDets,NegDets"<<endl;
	for(unsigned int winCount=0; winCount< wins.size();winCount++)
	{
		Rect r = wins[winCount];
		Mat image(frame,r);
		
		float hgSvmRs=0;float hKnnRs=0;float hBoostRs=0; 
		float sSvmRs=0;float sKnnRs=0;float sBoostRs=0;
		int pCount =0;int nCount = 0;
		Mat desc,classes;
		
		if(useHOG){
			detector.compute_hog_features(image,1,desc,classes);
			if(useSvm)
			{	
				hgSvmRs = predictor.predict_svm(hogSvmPtr,desc,0);
				((hgSvmRs>0)?pCount++:nCount++);
			}
			if(useKnn){ 
				
				hKnnRs = predictor.predict_knn(aHogKnn,desc,sKnnTreshold);
				((hKnnRs>0)?pCount++:nCount++);
			}
			if(useBoost){ 
				hBoostRs = predictor.predict_boost(hogBoostPtr,desc,sBoostTreshold);
				((hBoostRs>0)?pCount++:nCount++);			
			}
		}
		else{		
							
			detector.extract_keypoints(image,1,desc,classes);
			if(useSvm){	
				sSvmRs = predictor.predict_svm(surfSvmPtr,desc,0);
				((sSvmRs>0)?pCount++:nCount++);
			}
			if(useKnn){ 
				sKnnRs = predictor.predict_knn(aSurfKnn,desc,sKnnTreshold);
				((sKnnRs>0)?pCount++:nCount++);	
			}
			if(useBoost){ 
				sBoostRs = predictor.predict_boost(surfBoostPtr,desc,sBoostTreshold); 
				((sBoostRs>0)?pCount++:nCount++);	
			}
		}
		//log the votes for each of these classifier results

		ostringstream detWinProps;
		detWinProps<<"win("<<r.x<<"X"<<r.y<<")";
		csvlog<<detWinProps.str().c_str()<<",";
		
		csvlog<<hgSvmRs<<","<<hKnnRs<<","<<hBoostRs<<","<<sSvmRs<<","<<sKnnRs<<","<<sBoostRs<<","<<pCount<<","<<nCount<<endl;
		
		if(pCount>nCount)
		{	
			ostringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			cout<<detectionScoresOutput.str()<<endl;
			winsDetected++;
			rectangle(origFrame,r,CV_RGB(255,0,0));
			ostringstream str;
			str <<imgOutLoc<<frameCount<<"_"<<winCount<<".jpg"<<endl;
		}
		else{	
			stringstream detectionScoresOutput;
			detectionScoresOutput<<"hsvm:"<<hgSvmRs<<":"<<"hKnnRs:"<<hKnnRs<<":hBoostRs:"<<hBoostRs<<",pcount:"<<pCount<<":ncount"<<nCount<<endl;
			cout<<detectionScoresOutput.str()<<endl;
		}
	}
	csvlog.close();
	return winsDetected;
}

void run_exp(string frameLogName,string imgOutputLoc,int winwidth,int winheight,vector<string> posfiles,vector<string>negfiles
	,classify& predictor,features& detector,string vidName,string posdir,string negdir)
{
	int posAsPos = 0;
	int negAsPos = 0;
	int posAsNeg = 0;
	int negAsNeg = 0;
	string ext=".jpg";
	
	ofstream frameLog(frameLogName.c_str());
	frameLog<<"expNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"imgName"<<","<<"Type"<<",totalWins"<<","<<"posAsPos"<<","<<"posAsNeg"<<endl;
	for(unsigned int filecount=0;filecount<posfiles.size();filecount++)
	{	
		//cout<<"posfile:"<<posdir+"/"+posfiles[filecount]<<endl;
		Mat frame = imread(posdir+"/"+posfiles[filecount]);
		Mat newFrame;
		cvtColor(frame,newFrame,CV_RGB2GRAY);		
		
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		
		string imgName = posfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
		
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		ofstream csvLog(tempLogName.c_str());
		
		int result = operate_slidewins(frame,wins,newFrame,imgOutputLoc,imgName,csvLog,detector,predictor);
		if(result>0)
			posAsPos++;
		else
			posAsNeg++;

		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),frame);
		csvLog.close();		
		//frameLog<<expNo<<","<<vidName<<","<<"ensemble"<<","<<imgName<<",POS,"<<wins.size()<<","<<posAsPos<<","<<posAsNeg<<endl;
		//so heres the template, experiment name, name,filename,res,total wins, posWins
	
	}
	for(unsigned int filecount=0;filecount<negfiles.size();filecount++)
	{
		//cout<<"negfile:"<<negdir+"/"+negfiles[filecount]<<endl;
		Mat frame = imread(negdir+"/"+negfiles[filecount]);
		Mat newFrame;
		cvtColor(frame,newFrame,CV_RGB2GRAY);
		
		string imgName = negfiles[filecount];
		imgName= imgName.replace(imgName.find(ext),sizeof(ext),"");
		
		string tempLogName = imgOutputLoc+"logs/"+imgName+".txt";
		ofstream csvLog(tempLogName.c_str());		
		vector<Rect> wins = utils::get_sliding_windows(frame,winwidth,winheight,30,30);
		cout<<"wins count:"<<wins.size()<<endl;		
		int result = operate_slidewins(frame,wins,newFrame,imgOutputLoc,imgName,csvLog,detector,predictor);
		if(result>0)
			negAsPos++;
		else
			negAsNeg++;
		
		ostringstream str;
		str <<imgOutputLoc<<imgName<<"_"<<result<<".jpg"<<endl;
		imwrite(str.str(),frame);
		csvLog.close();		
		//operate_slidewins(frame,wins,&image,imgOutLoc,negfiles[filecount],expJSONlog,detector,predictor);
		//frameLog<<expNo<<","<<vidName<<","<<"ensemble"<<","<<imgName<<",NEG,"<<wins.size()<<","<<negAsPos<<","<<negAsNeg<<endl;
	}
	//the first three things should uniquely identify the exp record
	//then there should be frame count
	/*rLog<<"ExpNo"<<","<<"vidName"<<","<<"featuresUsed"<<","<<"FilesTotal"<<","<<"posAsPos"<<","<<"posAsNeg"<<","<<"negAsPos"<<","<<"negAsNeg"<<endl;
	rLog<<expNo<<","<<vidName<<","<<"ensemble"<<","<<(posfiles.size()+negfiles.size())<<","<<posAsPos<<","<<posAsNeg<<","<<negAsPos<<","<<negAsNeg<<endl;
	frameLog.close();
	rLog.close();*/
}


void testStuff()
{
	//feature_class_pair data = detector.get_train_data_from_vec("pos_320x240.vec","neg_320x240.vec",7,2);
	vector<vec_entry> entries;
	utils::get_vec_entries("pos_320x240.vec",entries);
	for(int i=0;i<entries.size();i++)
	{
		vec_entry entry = entries[i];
		cout<<entry.filename<<endl;
		for(int j=0;j<entry.objects.size();j++)
		{
			vec_object object = entry.objects[j];
			cout<<object.x<<":"<<object.y<<":"<<object.width<<" "<<object.height<<endl;
		}
	}
}
/**
	Params expected:
	Param 1: feature type
		1=SURF
		2=HOG
	Param 2: classifier type
		1=SVM
		2=k-NN
		3=AdaBoost
	Params 3=onwards, all the directories we wish to use
*/
int main(int argc,char** argv)
{
	testStuff();
	classify predictor;	
	
	string expNo = "2";
	string expName="HSensemble";
	string resultsOutput = "expdata/exp_5/";
	int winWidth = 120;
	int winHeight =80;	
	features detector(winWidth,winHeight);
	string param1 = argv[1];
	string param2= argv[2];
	int featureType = atoi(param1.c_str());
	int classifierType = atoi(param2.c_str());
	string classifierStr;
	string featuresToUse;
	if(featureType == HOG)
	{
		cout<<"using hog"<<endl;
		useHOG = true;	
		hogSvmPtr = new CvSVM;
		hogBoostPtr = new CvBoost;
		featuresToUse = "HOG";
		hogSvmPtr->load("classifiers/hogsvm.xml");
		hogBoostPtr->load("classifiers/hogboost.xml");
	}
	else
	{
		cout<<"using SURF"<<endl;
		featuresToUse = "SURF";
		useSURF = true;
		surfBoostPtr = new CvBoost;
		surfSvmPtr = new CvSVM;
		surfSvmPtr->load("classifiers/surfsvm.xml");
		surfBoostPtr->load("classifiers/surfboost.xml");
	}
	if(classifierType == SVM)
	{
		cout<<"using SVM"<<endl;
		classifierStr ="SVM";
		useSvm = true;
	}
	else if(classifierType == KNN)
	{
		cout<<"using KNN"<<endl;
		classifierStr ="KNN";
		useKnn = true;			
	}
	else
	{
		classifierStr ="BOOST";
		cout<<"using Boost"<<endl;
		useBoost = true;
	}
	
	//string frameLogName = resultsOutput+expName+"_"+expNo+".txt";
	
	feature_class_pair data = detector.get_train_data_from_vec("pos_320x240.vec","neg_320x240.vec",7,2);
	train_knn(data,useHOG,useSURF);


	for(int i=3;i<argc;i++)
	{
		string dirname = argv[i];
		cout<<dirname<<endl;		
		string outLoc;
		
		outLoc.append(resultsOutput);
		outLoc.append(dirname);	outLoc.append("/");
		outLoc.append(featuresToUse);outLoc.append("/");
		outLoc.append(classifierStr);outLoc.append("/");
		
		
		string testImagesLoc = "testvideos/";
		testImagesLoc.append(dirname);
		testImagesLoc.append("/");
		testImagesLoc.append("small_320x240/");
		cout<<"testImagesLoc:"<<testImagesLoc<<endl;
		vector<string> posfiles,negfiles;
		
		string posdir = testImagesLoc;
		posdir+="pos";
		cout<<posdir<<endl;		
		string negdir =testImagesLoc;
		negdir+="neg";
		cout<<negdir<<endl;
		utils::get_files_in_dir(posdir,posfiles);
		utils::get_files_in_dir(negdir,negfiles);
		string frameLogName = resultsOutput+expName+"_"+expNo+"_"+argv[i]+".txt";
		//run_exp(string frameLogName,string imgOutputLoc,int winwidth,int winheight,vector<string> posfiles,vector<string>negfiles)
		//so now that we have reached here we have all the files
		//so lets make the call to the function to run the experiment.
		string resultsSaveLoc = resultsOutput+argv[i]+"/";		
		cout<<"output images save loc:"<<outLoc<<endl;
		cout<<"frameLogName:"<<frameLogName<<endl;
		cout<<"the number of pos files:"<<posfiles.size()<<endl;
		cout<<"the number of neg files:"<<negfiles.size()<<endl;
		run_exp(frameLogName,outLoc,winWidth,winHeight,posfiles,negfiles,predictor,detector,argv[i],posdir,negdir);		
	}
}
