#include "test.h"
#include "classifiers.h"
#include "utils.h"

typedef struct{	
	double uarm_x1;
	double uarm_y1;
	double uarm_x2;
	double uarm_y2;		
	double larm_x1;
	double larm_y1;
	double larm_x2;
	double larm_y2;	
}lamda_arms;
/*
images/good_will_hunting_1/00003450.jpg luarm 244.9 175.5 254.2 220.3
images/good_will_hunting_1/00003450.jpg ruarm 302.3 157.8 331.8 193.3
images/good_will_hunting_1/00003450.jpg rlarm 331.8 193.3 304.8 211.0
* 
images/good_will_hunting_1/00004050.jpg luarm 228.9 193.3 217.9 235.4
images/good_will_hunting_1/00004050.jpg llarm 217.9 235.4 225.5 265.8
images/good_will_hunting_1/00004050.jpg ruarm 287.1 186.5 309.0 226.2
*/

void vec_test()
{
	vector<string> entries;
	string vecfilename ="testset.vec";
	utils::get_body_part_vecs(vecfilename,entries);
	cout<<entries.size()<<endl;
}
void showBBBox()
{
	double ruarm_x1 = 217.9;
	double luarm_x2 = 217.9;
	double luarm_x1 = 228.9;	
	double ruarm_x2 = 225.5;	
	
	
	double luarm_y1 = 193.3;
	double luarm_y2 = 235.4;	
	double ruarm_y1 = 235.4;
	double ruarm_y2 = 265.8;
	
	double rlarm_x1 =  287.1;
	double rlarm_y1 = 186.5;
	double rlarm_x2 = 309.0;
	double rlarm_y2 = 226.2;
	
	
	Mat image = imread("testvideos/LAMDa/images/good_will_hunting_1/00004050.jpg");
	rectangle(image,Point(ruarm_x1,luarm_y1),Point(luarm_x1 ,ruarm_y2  ),CV_RGB(0,255,0),2);
	rectangle(image,Point(rlarm_x1,rlarm_y1),Point(rlarm_x2,rlarm_y2 ),CV_RGB(0,255,0),2);	
	imshow("sample",image);
	if(waitKey(300000) >= 0) cout<<"waiting"<<endl;
}
void test_image_function()
{
	vector<Mat> images;
	utils::get_LMDa_torso_crops(images);
	
	for(unsigned int i=0;i<images.size();i++)
	{		
		IplImage image = images[i];
		
		imshow("images",images[i]);		
		
		IplImage* rotate45 = utils::rotateImage(&image,-45);
		Mat rotatedImage = rotate45;
		imshow("45",rotatedImage);
		IplImage* rotate90 = utils::rotateImage(&image,-90);
		rotatedImage = rotate90;
		imshow("90",rotatedImage);
		//cvShowImage("90",rotate90);
		rotate45 = utils::rotateImage(&image,-135);
		cvShowImage("145",rotate45);
		
		//cvShowImage("images",utils::rotateImage(&image,90));
		cvReleaseImage(&rotate90);
		cvReleaseImage(&rotate45);
		if(waitKey(300) >= 0) break;
	}
	cout<<"images size:"<<images.size()<<endl;
	vector<Mat> neg;
	utils::get_nicta_neg_peds(neg);
	for(unsigned int i=0;i<neg.size();i++)
	{
		Mat image = neg[i];
		resize(image,image,Size(80,80));
		imshow("cropped images",image);
		if(waitKey(35) >= 0) break;
	}
	cout<<"neg size:"<<neg.size()<<endl;
}
void unit_test::shogun_svm()
{
	
}
void unit_test::test_vec_process()
{
	string filename = "vecfiles/TUDuprightpeople.vec";
	vector<vec_entry> entries;
	utils::get_vec_entries(filename,entries);	
	cout<<"size of the file"<<entries.size()<<endl;
	cout<<"great success in test..."<<entries.size()<<endl;
}
void merge_images()
{
	double alpha = 0.5; double beta; double input;
	
	Mat src1,src2, dst;
	/// Ask the user enter alpha
	std::cout<<" Simple Linear Blender "<<std::endl;
	std::cout<<"-----------------------"<<std::endl;
	std::cout<<"* Enter alpha [0-1]: ";


	/// We use the alpha provided by the user iff it is between 0 and 1
	if( alpha >= 0 && alpha <= 1 )
	{ alpha = input; }

	/// Read image ( same size, same type )
	src1 = imread("5.jpg");
	src2 = imread("4.jpg");

	/// Create Windows
	namedWindow("Linear Blend", 1);

	beta = ( 1.0 - alpha );
	addWeighted( src1, alpha, src2, beta, 0.0, dst);

	imshow( "Linear Blend", dst );

	waitKey(0);
	
}
void test_get_face_images()
{
	cout<<"trying to get the face images"<<endl;
	vector<Mat> images;
	utils::get_face_images(images,true);
	cout<<images.size()<<endl;
}
